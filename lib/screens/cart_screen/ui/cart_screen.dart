import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/screens/dashboard_screen/ui/dashboard_screen.dart';
import 'package:kiosk/screens/payment_options_screen/ui/payment_options_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/utils/snack_bar.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/bottom_sheet_keyboard.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:kiosk/widgets/reset_cart_dialog.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  List<OrderItems> cartList = [];
  double total = 0.00;
  int totalItems = 0;
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    cartList = Provider.of<CartViewModel>(context, listen: false).cartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 1.7,
                child: Container(
                  width: ScreenSize.width(context),
                  height: ScreenSize.height(context),
                  margin: EdgeInsets.fromLTRB(0, 40, 20, 40),
                  child: Column(
                    children: [
                      Expanded(
                          child: Container(
                        margin: EdgeInsets.fromLTRB(10, 40, 10, 20),
                        child: Container(
                          margin: EdgeInsets.only(top: 40),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.all(5),
                                child: Card(
                                  elevation: 3,
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                  child: InkWell(
                                    onTap: () {
                                      showSnackBar(context, "Coming Soon..");
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                            child: CircleAvatar(
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.all(8),
                                                  child: Image.asset("assets/images/remember_order.png"),
                                                ),
                                              ),
                                              backgroundColor: def_color_red,
                                            ),
                                          ),
                                          Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                rememberStuff.toUpperCase(),
                                                maxLines: 1,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 24,
                                                    color: def_color_red,
                                                    letterSpacing: 0.63,
                                                    fontFamily: "Swis721",
                                                    fontWeight: FontWeight.w700),
                                              ),
                                              Text(
                                                saveItems,
                                                maxLines: 1,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                    fontSize: 16,
                                                    color: def_color_red,
                                                    letterSpacing: 0.93,
                                                    fontFamily: "Swis721",
                                                    fontWeight: FontWeight.w300),
                                              )
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                child: Card(
                                  elevation: 3,
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => DashboardScreen()));
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                            child: CircleAvatar(
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.all(5),
                                                  child: Image.asset("assets/images/new_item_white.png"),
                                                ),
                                              ),
                                              backgroundColor: def_color_red,
                                            ),
                                          ),
                                          Text(
                                            addItem.toUpperCase(),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24,
                                                color: def_color_red,
                                                letterSpacing: 0.63,
                                                fontFamily: "Swis721",
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                child: Card(
                                  elevation: 3,
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                  child: InkWell(
                                    onTap: () {
                                      if (cartList.isNotEmpty) {
                                        sessionManager.getCustomerInfo().then((value) {
                                          if (value == "") {
                                            showCustomerDetailsBottomSheet();
                                          } else {
                                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => PaymentOptionsScreen()));
                                          }
                                        });
                                      } else {
                                        showSnackBar(context, "Add items to cart to place order!");
                                      }
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                            child: CircleAvatar(
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.all(5),
                                                  child: Image.asset("assets/images/complete_order.png"),
                                                ),
                                              ),
                                              backgroundColor: def_color_red,
                                            ),
                                          ),
                                          Text(
                                            completeOrder.toUpperCase(),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24,
                                                color: def_color_red,
                                                letterSpacing: 0.63,
                                                fontFamily: "Swis721",
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )),
                      Container(
                          margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
                          width: 220,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            color: Colors.white,
                          ),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(50),
                              child: TextButton(
                                onPressed: () {
                                  clearCartDialog(context);
                                },
                                child: Padding(
                                  padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                                  child: Text(
                                    cancelOrder.toUpperCase(),
                                    style: TextStyle(
                                        fontSize: 24, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.87, fontWeight: FontWeight.w700),
                                  ),
                                ),
                              ))),
                    ],
                  ),
                ),
              ),
              top: 40,
              left: 30,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 2.8,
                decoration:
                    BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                child: Column(
                  children: [
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                          gradient: button_red_gradient,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                      child: Center(
                        child: Text(
                          yourItems.toUpperCase(),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, color: Colors.white, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    Expanded(
                        child: Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: ListView.builder(
                          itemCount: cartList.length,
                          itemBuilder: (context, index) {
                            return Container(
                              width: ScreenSize.width(context),
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Column(children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        child: Text(
                                          cartList[index].itemName.toUpperCase(),
                                          maxLines: 2,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                    Center(
                                      child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                              child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    cartList[index].quantity = cartList[index].quantity - 1;
                                                    if (cartList[index].quantity == 0) {
                                                      Provider.of<CartViewModel>(context, listen: false).removeItem(index);
                                                    } else {
                                                      Provider.of<CartViewModel>(context, listen: false).updateCartItem(index, cartList[index]);
                                                    }
                                                  });
                                                },
                                                child: Container(
                                                  decoration:
                                                      BoxDecoration(borderRadius: BorderRadius.circular(50), border: Border.all(color: Colors.black)),
                                                  child: Icon(
                                                    /*cartList[index].quantity == 1 ? Icons.delete : */
                                                    Icons.remove,
                                                    color: def_color_red,
                                                    size: 20,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                              child: Text(
                                                cartList[index].quantity.toString(),
                                                maxLines: 2,
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    fontSize: 13,
                                                    color: def_color_red,
                                                    letterSpacing: 0.3,
                                                    fontFamily: "Swis721-Blk",
                                                    fontWeight: FontWeight.w500),
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                              child: InkWell(
                                                onTap: () {
                                                  setState(() {
                                                    cartList[index].quantity = cartList[index].quantity + 1;
                                                    Provider.of<CartViewModel>(context, listen: false).updateCartItem(index, cartList[index]);
                                                  });
                                                },
                                                child: Container(
                                                    decoration: BoxDecoration(
                                                        borderRadius: BorderRadius.circular(50), border: Border.all(color: Colors.black)),
                                                    child: Icon(
                                                      Icons.add,
                                                      color: def_color_red,
                                                      size: 20,
                                                    )),
                                              ),
                                            )
                                          ]),
                                    ),
                                    Expanded(
                                        child: Padding(
                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "\u0024",
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Text(
                                            (cartList[index].unitPrice * cartList[index].quantity).toStringAsFixed(2),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          )
                                        ],
                                      ),
                                    )),
                                  ],
                                ),
                                cartList[index].modifiersList.isEmpty
                                    ? SizedBox()
                                    : ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: cartList[index].modifiersList.length,
                                        itemBuilder: (context, i) {
                                          return Container(
                                              width: ScreenSize.width(context),
                                              padding: EdgeInsets.fromLTRB(0, 1, 0, 1),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                      child: Text(
                                                        cartList[index].modifiersList[i].optionName.toUpperCase(),
                                                        maxLines: 2,
                                                        textAlign: TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.black38,
                                                            letterSpacing: 0.3,
                                                            fontFamily: "Swis721-Blk",
                                                            fontWeight: FontWeight.w500),
                                                      ),
                                                    ),
                                                    flex: 2,
                                                  ),
                                                  Expanded(
                                                      child: Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Text(
                                                          "\u0024",
                                                          maxLines: 1,
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        ),
                                                        Text(
                                                          cartList[index].modifiersList[i].price.toString(),
                                                          maxLines: 1,
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        )
                                                      ],
                                                    ),
                                                  )),
                                                ],
                                              ));
                                        })
                              ]),
                            );
                          }),
                    )),
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                        gradient: button_red_gradient,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Text(
                                itemTotal.toUpperCase(),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                              ),
                            ),
                            flex: 2,
                          ),
                          Expanded(
                              child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "\u0024",
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  total.toStringAsFixed(2),
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              right: 20,
              top: 40,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {},
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }

  Widget _nameTextForm() {
    return Container(
      padding: EdgeInsets.all(10),
      child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
        Text(
          enterName,
          style: TextStyle(
            fontFamily: 'Swis721-Blk',
            fontSize: 16,
            color: def_color_red,
          ),
        ),
        Container(
          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
          margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), border: Border.all(color: Colors.black)),
          child: TextFormField(
            onTap: () {
              keyboard(context, nameController, 'text');
            },
            controller: nameController,
            cursorColor: def_color_red,
            decoration: InputDecoration(
                border: InputBorder.none, fillColor: Colors.white, hintText: enterName, hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
          ),
        )
      ]),
    );
  }

  Widget _emailTextForm() {
    return Container(
        padding: EdgeInsets.all(10),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            enterEmail,
            style: TextStyle(
              fontFamily: 'Swis721-Blk',
              fontSize: 16,
              color: def_color_red,
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), border: Border.all(color: Colors.black)),
            child: TextFormField(
              onTap: () {
                keyboard(context, emailController, 'text');
              },
              controller: emailController,
              cursorColor: def_color_red,
              decoration: InputDecoration(
                  border: InputBorder.none, fillColor: Colors.white, hintText: enterEmail, hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
            ),
          )
        ]));
  }

  Widget _phoneTextForm() {
    return Container(
        padding: EdgeInsets.all(10),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            phoneNumber,
            style: TextStyle(
              fontFamily: 'Swis721-Blk',
              fontSize: 16,
              color: def_color_red,
            ),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
            margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              border: Border.all(
                color: Colors.black,
              ),
            ),
            child: TextFormField(
              onTap: () {
                keyboard(context, phoneController, 'num');
              },
              controller: phoneController,
              cursorColor: def_color_red,
              decoration: InputDecoration(
                  border: InputBorder.none, fillColor: Colors.white, hintText: phoneNumber, hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
            ),
          )
        ]));
  }

  Widget _submitButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: def_color_red,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                CustomerInfo customerInfo = CustomerInfo(
                    name: nameController.text.toString(), email: emailController.text.toString(), phoneNumber: phoneController.text.toString());
                sessionManager.saveCustomerInfo(jsonEncode(customerInfo));
                Navigator.of(context).pop();
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PaymentOptionsScreen()));
              },
              child: Padding(
                padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                child: Text(
                  submit.toUpperCase(),
                  style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                ),
              ),
            )));
  }

  Widget _cancelButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
          border: Border.all(
            color: Colors.black,
          ),
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                phoneController.clear();
                nameController.clear();
                emailController.clear();
                Navigator.of(context).pop();
              },
              child: Padding(
                padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                child: Text(
                  cancel.toUpperCase(),
                  style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                ),
              ),
            )));
  }

  void showCustomerDetailsBottomSheet() async {
    await showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        isDismissible: false,
        enableDrag: false,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        builder: (context) {
          return FractionallySizedBox(
              heightFactor: 0.65,
              child: Container(
                width: ScreenSize.width(context) / 3,
                padding: EdgeInsets.all(25),
                margin: EdgeInsets.all(25),
                child: Column(mainAxisAlignment: MainAxisAlignment.start, crossAxisAlignment: CrossAxisAlignment.center, children: [
                  _nameTextForm(),
                  _emailTextForm(),
                  _phoneTextForm(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [_submitButton(), _cancelButton()],
                  )
                ]),
              ));
        });
  }
}
