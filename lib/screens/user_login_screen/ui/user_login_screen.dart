import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

class UserLoginScreen extends StatefulWidget {
  const UserLoginScreen({Key? key}) : super(key: key);

  @override
  _UserLoginScreenState createState() => _UserLoginScreenState();
}

class _UserLoginScreenState extends State<UserLoginScreen> {
  String currentText = "";
  bool _otpSent = false, _nameISEmpty = false, _phoneIsEmpty = false, enableButton = true;
  TextEditingController enterNameController = TextEditingController();
  TextEditingController enterPhoneNumberController = TextEditingController();

  @override
  void initState() {
    super.initState();
    enterNameController.text = '';
    enterNameController.addListener(() {
      setState(() {});
    });
    enterPhoneNumberController.text = '';
    enterNameController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: dashboardTilesWidget(),
              top: 20,
              left: 30,
              right: 30,
            ),
            Positioned(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                height: ScreenSize.height(context),
                width: ScreenSize.width(context),
                child: _loginUIWidget(),
              ),
              top: 130,
              bottom: 30,
              left: 30,
              right: 30,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardTilesWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        InkWell(
          onTap: () {},
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(15, 10, 20, 10),
              decoration: BoxDecoration(gradient: button_red_gradient),
              child: Row(
                children: [
                  Icon(
                    Icons.more_vert,
                    color: Colors.white,
                  ),
                  Text(
                    moreInfo.toUpperCase(),
                    style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: "Swis721", fontWeight: FontWeight.w500),
                  )
                ],
              ),
            ),
          ),
        ),
        Spacer(),
        InkWell(
          onTap: () {},
          child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Container(
              height: 50,
              padding: EdgeInsets.fromLTRB(5, 0, 20, 0),
              decoration: BoxDecoration(gradient: button_red_gradient),
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: 20),
                    child: CircleAvatar(
                      child: Padding(
                        padding: EdgeInsets.all(5),
                        child: Image.asset(
                          "assets/images/default_user.png",
                          height: 30,
                          width: 30,
                        ),
                      ),
                      backgroundColor: Colors.white,
                    ),
                  ),
                  Text(
                    signIn.toUpperCase(),
                    style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: "Swis721", fontWeight: FontWeight.w500),
                  )
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(left: 20),
          child: Image.asset(
            "assets/images/default_qr.png",
            height: 75,
            width: 75,
          ),
        )
      ],
    );
  }

  Widget _loginUIWidget() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(60)),
      child: Container(
        height: ScreenSize.height(context),
        width: ScreenSize.width(context),
        margin: EdgeInsets.only(top: 30),
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(60)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _emailAndPasswordWidget(),
            _otpSent ? _enterOtpTextWidget() : SizedBox(),
            Spacer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [_bottomLeftImage(), _submitButton(), _bottomRightImage()],
            ),
          ],
        ),
      ),
    );
  }

  Widget _emailAndPasswordWidget() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          enterName,
          style: TextStyle(fontSize: 24, fontFamily: "Swis721", fontWeight: FontWeight.w700, color: def_color_red, letterSpacing: 0.72),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          padding: EdgeInsets.all(3),
          width: ScreenSize.width(context) / 2,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.black, width: 1.5),
            borderRadius: BorderRadius.all(Radius.circular(60.0)),
          ),
          child: TextFormField(
            controller: enterNameController,
            cursorColor: Colors.black,
            cursorHeight: 16,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              hintText: enterNameHint,
              hintStyle: TextStyle(fontFamily: "Swis721", fontSize: 16, color: Colors.grey, letterSpacing: 0.9),
              border: InputBorder.none,
            ),
          ),
        ),
        _nameISEmpty
            ? Align(
                alignment: Alignment.center,
                child: Text(
                  'please enter name',
                  style: TextStyle(fontFamily: "Swis721", fontWeight: FontWeight.w700, fontSize: 16, color: def_color_red),
                ),
              )
            : SizedBox(),
        SizedBox(
          height: 30,
        ),
        Text(
          phoneNumber,
          style: TextStyle(fontSize: 24, fontFamily: "Swis721", fontWeight: FontWeight.w700, color: def_color_red, letterSpacing: 1.72),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 20,
          ),
          padding: EdgeInsets.all(3),
          width: ScreenSize.width(context) / 2,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.black, width: 1.5),
            borderRadius: BorderRadius.all(Radius.circular(60.0)),
          ),
          child: TextFormField(
            cursorColor: Colors.black,
            controller: enterPhoneNumberController,
            cursorHeight: 16,
            keyboardType: TextInputType.number,
            textAlign: TextAlign.center,
            decoration: InputDecoration(
              hintText: passwordHint,
              hintStyle: TextStyle(fontFamily: "Swiz721", fontWeight: FontWeight.w300, fontSize: 16, color: Colors.grey, letterSpacing: 0.9),
              border: InputBorder.none,
            ),
          ),
        ),
        _phoneIsEmpty
            ? Align(
                alignment: Alignment.center,
                child: Text(
                  'please enter phone number',
                  style: TextStyle(fontFamily: "Swis721", fontWeight: FontWeight.w700, fontSize: 16, color: def_color_red),
                ),
              )
            : SizedBox()
      ],
    );
  }

  Widget _submitButton() {
    return InkWell(
      onTap: () {
        if (enterNameController.text.isEmpty) {
          setState(() {
            _nameISEmpty = true;
            _otpSent = false;
          });
        } else if (enterPhoneNumberController.text.isEmpty) {
          setState(() {
            _phoneIsEmpty = true;
            _otpSent = false;
          });
        } else if (enterNameController.text.isNotEmpty &&
            enterPhoneNumberController.text.isNotEmpty &&
            enterPhoneNumberController.text.length != 10) {
          setState(() {
            _otpSent = false;
          });
        } else {
          if (_otpSent) {
            Navigator.of(context).pop();
          }
          setState(() {
            _nameISEmpty = false;
            _phoneIsEmpty = false;
            _otpSent = true;
          });
        }
      },
      child: Container(
        padding: EdgeInsets.all(10),
        width: ScreenSize.width(context) / 6,
        decoration: enableButton
            ? BoxDecoration(
                gradient: button_red_gradient,
                borderRadius: BorderRadius.circular(30),
              )
            : BoxDecoration(
                color: button_color_disabled,
                borderRadius: BorderRadius.circular(30),
              ),
        child: Center(
          child: Text(
            submit,
            style: TextStyle(fontSize: 21, fontFamily: "Swis721", fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: 1.72),
          ),
        ),
      ),
    );
  }

  Widget _enterOtpTextWidget() {
    return Column(
      children: [
        Text(
          otpSent,
          style: TextStyle(fontSize: 21, fontFamily: "Swis721", fontWeight: FontWeight.w300, color: Colors.black, letterSpacing: 0.72),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          enterOtp,
          style: TextStyle(fontSize: 24, fontFamily: "Swis721", fontWeight: FontWeight.w700, color: def_color_red, letterSpacing: 0.72),
        ),
        SizedBox(
          height: 20,
        ),
        _otpWidget(),
      ],
    );
  }

  Widget _otpWidget() {
    return Container(
      width: ScreenSize.width(context) / 4,
      height: 50,
      child: Center(
        child: PinCodeTextField(
          appContext: context,
          pastedTextStyle: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          length: 4,
          obscureText: false,
          pinTheme: PinTheme(
              fieldHeight: 50,
              fieldWidth: 60,
              selectedColor: Colors.black,
              activeColor: Colors.black,
              inactiveColor: Colors.grey,
              selectedFillColor: Colors.white,
              activeFillColor: Colors.white,
              inactiveFillColor: Colors.grey,
              shape: PinCodeFieldShape.circle),
          cursorColor: Colors.black,
          backgroundColor: Colors.white,
          keyboardType: TextInputType.number,
          onChanged: (otp) {},
        ),
      ),
    );
  }

  Widget _bottomLeftImage() {
    return Container(
      width: 173,
      child: Image.asset(
        "assets/images/default_left_bottom_corner.png",
        fit: BoxFit.fill,
      ),
    );
  }

  Widget _bottomRightImage() {
    return Container(
      width: 173,
      child: Image.asset(
        "assets/images/default_right_bottom_corner.png",
        fit: BoxFit.fill,
      ),
    );
  }
}
