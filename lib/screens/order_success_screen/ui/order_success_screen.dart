import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/screens/dashboard_screen/viewModel/dashboard_view_model.dart';
import 'package:kiosk/screens/landing_banner_screen/ui/landing_banners_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:provider/provider.dart';

class OrderSuccessScreen extends StatefulWidget {
  final String orderUniqueId, orderId;

  const OrderSuccessScreen({Key? key, required this.orderId, required this.orderUniqueId}) : super(key: key);

  @override
  _OrderSuccessScreenState createState() => _OrderSuccessScreenState();
}

class _OrderSuccessScreenState extends State<OrderSuccessScreen> {
  List<OrderItems> cartList = [];
  double total = 0.00, taxAmount = 0.00;
  int totalItems = 0;
  String mOrderUniqueId = "", mOrderId = "", restaurantId = "", restaurantLogo = "", restaurantName = "";
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    mOrderUniqueId = widget.orderUniqueId;
    mOrderId = widget.orderId;
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantId = value.restaurantId;
        restaurantLogo = value.restaurantLogo;
        restaurantName = value.restaurantName;
      });
    });
    print("orderId $mOrderId $mOrderUniqueId $restaurantId");
    cartList = Provider.of<CartViewModel>(context, listen: false).cartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 1.7,
                child: Container(
                  width: ScreenSize.width(context),
                  height: ScreenSize.height(context),
                  margin: EdgeInsets.fromLTRB(0, 40, 40, 40),
                  child: Container(
                    height: ScreenSize.height(context),
                    width: ScreenSize.width(context),
                    margin: EdgeInsets.only(top: 40),
                    child: Card(
                      elevation: 3,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                      child: Container(
                        padding: EdgeInsets.all(15),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                              height: ScreenSize.height(context) / 4,
                              child: Image.asset(
                                "assets/images/payment_success.png",
                                fit: BoxFit.fill,
                              ),
                            ),
                            Column(
                              children: [
                                Text(
                                  orderPlaced,
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 24, color: def_color_red, letterSpacing: 3, fontFamily: "Poppins", fontWeight: FontWeight.w600),
                                ),
                                RichText(
                                  text: TextSpan(
                                    text: '',
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: totalAmount,
                                        style: TextStyle(
                                            fontSize: 24, color: Colors.black, letterSpacing: 3, fontFamily: "Poppins", fontWeight: FontWeight.w500),
                                      ),
                                      TextSpan(
                                        text: "\u0024 ${total.toStringAsFixed(2)}",
                                        style: TextStyle(
                                            fontSize: 24, color: Colors.black, letterSpacing: 3, fontFamily: "Poppins", fontWeight: FontWeight.w700),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Container(
                                  margin: EdgeInsets.all(5),
                                  child: InkWell(
                                    onTap: () {
                                      printReceiptDialog();
                                    },
                                    child: Card(
                                      elevation: 4,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                      child: Container(
                                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10),
                                          color: def_color_yellow,
                                        ),
                                        child: Text(
                                          print_str.toUpperCase(),
                                          maxLines: 1,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.black,
                                              letterSpacing: 0.66,
                                              fontFamily: "Poppins",
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.all(5),
                                  child: InkWell(
                                    onTap: () {
                                      Provider.of<CartViewModel>(context, listen: false).clearCart();
                                      Provider.of<DashBoardViewModel>(context, listen: false).setSelected(-1);
                                      sessionManager.clearCartItems();
                                      sessionManager.clearTempCartItems();
                                      sessionManager.clearCustomerInfo();
                                      sessionManager.clearDineOptions();
                                      Navigator.pushAndRemoveUntil(
                                          context, MaterialPageRoute(builder: (context) => LandingBannerScreen()), (Route<dynamic> route) => false);
                                    },
                                    child: Card(
                                      elevation: 4,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                      child: Container(
                                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(10),
                                          color: def_color_red,
                                        ),
                                        child: Text(
                                          home.toUpperCase(),
                                          maxLines: 1,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.white,
                                              letterSpacing: 0.66,
                                              fontFamily: "Poppins",
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              "Order No: #$mOrderUniqueId",
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black.withOpacity(0.8),
                                  letterSpacing: 2.8,
                                  fontFamily: "Poppins",
                                  fontWeight: FontWeight.w400),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              top: 40,
              left: 30,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 2.8,
                decoration:
                    BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                child: Column(
                  children: [
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                          gradient: button_red_gradient,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                      child: Center(
                        child: Text(
                          yourItems.toUpperCase(),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, color: Colors.white, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    Expanded(
                        child: Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: ListView.builder(
                          itemCount: cartList.length,
                          itemBuilder: (context, index) {
                            return Container(
                              width: ScreenSize.width(context),
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Column(children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        child: Text(
                                          cartList[index].itemName.toUpperCase(),
                                          maxLines: 2,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                    Center(
                                      child: Container(
                                        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                        child: Text(
                                          cartList[index].quantity.toString(),
                                          maxLines: 2,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                        child: Padding(
                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "\u0024",
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Text(
                                            (cartList[index].unitPrice * cartList[index].quantity).toStringAsFixed(2),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          )
                                        ],
                                      ),
                                    )),
                                  ],
                                ),
                                cartList[index].modifiersList.isEmpty
                                    ? SizedBox()
                                    : ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: cartList[index].modifiersList.length,
                                        itemBuilder: (context, i) {
                                          return Container(
                                              width: ScreenSize.width(context),
                                              padding: EdgeInsets.fromLTRB(0, 1, 0, 1),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                      child: Text(
                                                        cartList[index].modifiersList[i].optionName.toUpperCase(),
                                                        maxLines: 2,
                                                        textAlign: TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.black38,
                                                            letterSpacing: 0.3,
                                                            fontFamily: "Swis721-Blk",
                                                            fontWeight: FontWeight.w500),
                                                      ),
                                                    ),
                                                    flex: 2,
                                                  ),
                                                  Expanded(
                                                      child: Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Text(
                                                          "\u0024",
                                                          maxLines: 1,
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        ),
                                                        Text(
                                                          cartList[index].modifiersList[i].price.toString(),
                                                          maxLines: 1,
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        )
                                                      ],
                                                    ),
                                                  )),
                                                ],
                                              ));
                                        })
                              ]),
                            );
                          }),
                    )),
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                        gradient: button_red_gradient,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Text(
                                itemTotal.toUpperCase(),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                              ),
                            ),
                            flex: 2,
                          ),
                          Expanded(
                              child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "\u0024",
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  total.toStringAsFixed(2),
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              right: 20,
              top: 40,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {},
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }

  void printReceiptDialog() {
    showDialog<void>(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => true,
              child: AlertDialog(
                contentPadding: EdgeInsets.zero,
                backgroundColor: Colors.white,
                content: Container(
                  height: ScreenSize.height(context),
                  width: ScreenSize.width(context) / 3,
                  child: Column(
                    children: [
                      Container(
                        width: ScreenSize.width(context),
                        color: Colors.grey.shade100,
                        child: Row(
                          children: [
                            Container(
                              height: ScreenSize.height(context) / 10,
                              width: ScreenSize.width(context) / 11,
                              padding: EdgeInsets.all(5),
                              child: Image.network(
                                restaurantLogo,
                                fit: BoxFit.fill,
                              ),
                            ),
                            Expanded(
                              child: Container(
                                padding: EdgeInsets.all(5),
                                child: Column(
                                  children: [
                                    Text(
                                      restaurantName,
                                      maxLines: 1,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 20, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                    ),
                                    Text(
                                      "Order No: #$mOrderUniqueId",
                                      maxLines: 1,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                    ),
                                    Text(
                                      "11/16 12:42 PM",
                                      maxLines: 1,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.black45,
                                          letterSpacing: 0.6,
                                          fontFamily: "Swis721",
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ],
                                ),
                              ),
                              flex: 2,
                            )
                          ],
                        ),
                      ),
                      Expanded(
                          child: Container(
                        width: ScreenSize.width(context),
                        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                        child: ListView.builder(
                            primary: false,
                            itemCount: cartList.length,
                            itemBuilder: (context, index) {
                              return Container(
                                width: ScreenSize.width(context),
                                padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                                child: Column(children: [
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Padding(
                                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                          child: Text(
                                            cartList[index].itemName.toUpperCase(),
                                            maxLines: 2,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        flex: 2,
                                      ),
                                      Center(
                                        child: Container(
                                          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                          child: Text(
                                            cartList[index].quantity.toString(),
                                            maxLines: 2,
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                                fontSize: 13,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                          child: Padding(
                                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: [
                                            Text(
                                              "\u0024",
                                              maxLines: 1,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: def_color_red,
                                                  letterSpacing: 0.3,
                                                  fontFamily: "Swis721-Blk",
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            Text(
                                              (cartList[index].unitPrice * cartList[index].quantity).toStringAsFixed(2),
                                              maxLines: 1,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  color: def_color_red,
                                                  letterSpacing: 0.3,
                                                  fontFamily: "Swis721-Blk",
                                                  fontWeight: FontWeight.w500),
                                            )
                                          ],
                                        ),
                                      )),
                                    ],
                                  ),
                                  cartList[index].modifiersList.isEmpty
                                      ? SizedBox()
                                      : ListView.builder(
                                          shrinkWrap: true,
                                          itemCount: cartList[index].modifiersList.length,
                                          itemBuilder: (context, i) {
                                            return Container(
                                                width: ScreenSize.width(context),
                                                padding: EdgeInsets.fromLTRB(0, 1, 0, 1),
                                                child: Row(
                                                  children: [
                                                    Expanded(
                                                      child: Padding(
                                                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                        child: Text(
                                                          cartList[index].modifiersList[i].optionName.toUpperCase(),
                                                          maxLines: 2,
                                                          textAlign: TextAlign.start,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        ),
                                                      ),
                                                      flex: 2,
                                                    ),
                                                    Expanded(
                                                        child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                      child: Row(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        children: [
                                                          Text(
                                                            "\u0024",
                                                            maxLines: 1,
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                color: Colors.black38,
                                                                letterSpacing: 0.3,
                                                                fontFamily: "Swis721-Blk",
                                                                fontWeight: FontWeight.w500),
                                                          ),
                                                          Text(
                                                            cartList[index].modifiersList[i].price.toString(),
                                                            maxLines: 1,
                                                            textAlign: TextAlign.center,
                                                            style: TextStyle(
                                                                fontSize: 14,
                                                                color: Colors.black38,
                                                                letterSpacing: 0.3,
                                                                fontFamily: "Swis721-Blk",
                                                                fontWeight: FontWeight.w500),
                                                          )
                                                        ],
                                                      ),
                                                    )),
                                                  ],
                                                ));
                                          })
                                ]),
                              );
                            }),
                      )),
                      Container(
                        width: ScreenSize.width(context),
                        color: Colors.grey.shade100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Padding(
                              padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                    child: Text(
                                      "sub total".toUpperCase(),
                                      maxLines: 1,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                    ),
                                  ),
                                  Text(
                                    ":",
                                    maxLines: 1,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  ),
                                  Expanded(
                                    child: Text(
                                      "\u0024" + total.toStringAsFixed(2),
                                      maxLines: 1,
                                      textAlign: TextAlign.end,
                                      style: TextStyle(
                                          fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                      child: Text(
                                    "tax".toUpperCase(),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  )),
                                  Text(
                                    ":",
                                    maxLines: 1,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  ),
                                  Expanded(
                                      child: Text(
                                    "\u0024" + taxAmount.toStringAsFixed(2),
                                    maxLines: 1,
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  )),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                      child: Text(
                                    "total".toUpperCase(),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  )),
                                  Text(
                                    ":",
                                    maxLines: 1,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  ),
                                  Expanded(
                                      child: Text(
                                    "\u0024" + (total + taxAmount).toStringAsFixed(2),
                                    maxLines: 1,
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  )),
                                ],
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(5, 3, 5, 3),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Expanded(
                                      child: Text(
                                    "mode".toUpperCase(),
                                    maxLines: 1,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  )),
                                  Text(
                                    ":",
                                    maxLines: 1,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  ),
                                  Expanded(
                                      child: Text(
                                    "",
                                    maxLines: 1,
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                        fontSize: 16, color: Colors.black, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                  )),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: ScreenSize.width(context),
                        color: Colors.grey.shade100,
                        child: Text(
                          "footer",
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style:
                              TextStyle(fontSize: 14, color: Colors.black26, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                        ),
                      ),
                    ],
                  ),
                ),
              ));
        });
  }
}
