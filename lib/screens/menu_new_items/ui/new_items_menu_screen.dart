import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/screens/menu_new_items/viewModel/newly_added_view_model.dart';
import 'package:kiosk/screens/modifiers_menu_screen/ui/modifiers_menu_screen.dart';
import 'package:kiosk/screens/sides_menu_screen/ui/sides_menu_screen.dart';
import 'package:kiosk/screens/size_based_screen/ui/size_based_items_screen.dart';
import 'package:kiosk/screens/suggestions_screen/ui/suggestions_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/math_calculations.dart';
import 'package:kiosk/utils/progress_loading_white.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:provider/provider.dart';

class NewItemsMenuScreen extends StatefulWidget {
  const NewItemsMenuScreen({Key? key}) : super(key: key);

  @override
  _NewItemsMenuScreenState createState() => _NewItemsMenuScreenState();
}

class _NewItemsMenuScreenState extends State<NewItemsMenuScreen> {
  String restaurantId = "", restaurantLogo = "", currentDay = "", currentTime = "";
  late DateTime time;
  List<ItemsList> mItemsList = [];
  List<OrderItems> cartList = [], tempCartList = [];
  double total = 0.00;
  int totalItems = 0;
  late NewlyAddedItemsViewModel viewModel;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    viewModel = Provider.of<NewlyAddedItemsViewModel>(context, listen: false);
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantId = value.restaurantId;
        restaurantLogo = value.restaurantLogo;
      });
    });
    DateTime today = DateTime.now();
    setState(() {
      currentDay = DateFormat('EEEE').format(today);
      currentTime = DateFormat('HH:mm').format(today);
    });
    print("date---" + DateFormat('EEEE').format(today) + "----" + DateFormat('HH:mm').format(today));
    cartList = Provider.of<CartViewModel>(context, listen: false).getCartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                child: Text(
                  newItems,
                  maxLines: 2,
                  textScaleFactor: 1.5,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 32, color: Colors.white, letterSpacing: 2.79, fontFamily: "Palace", fontWeight: FontWeight.w500),
                ),
              ),
              top: 20,
              left: 20,
              right: 20,
            ),
            Positioned(child: _items(), top: 80, right: 20, bottom: ScreenSize.height(context) / 8, left: 20),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget _items() {
    return FutureBuilder(
        future: viewModel.newlyAddedItemsApi(restaurantId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print("connectionState ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loaderWhite());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 25, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as MenuItemsModel;
              if (data.responseStatus == 1) {
                if (data.itemsList!.isNotEmpty) {
                  mItemsList = data.itemsList!;
                  return GridView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: mItemsList.length,
                      gridDelegate:
                          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4, childAspectRatio: 1, mainAxisSpacing: 8, crossAxisSpacing: 8),
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            double price = 0.00;
                            if (mItemsList[index].pricingStrategy == 4) {
                              bool timeBased = false;
                              mItemsList[index].timePriceList.forEach((timePriceElement) {
                                timePriceElement.days.forEach((daysElement) {
                                  if (currentDay == daysElement) {
                                    time = DateFormat('HH:mm').parse(currentTime);
                                    print(time);
                                    var startTime = DateFormat('HH:mm').parse(timePriceElement.timeFrom);
                                    var endTime = DateFormat('HH:mm').parse(timePriceElement.timeTo);
                                    if (time.isAfter(startTime) && time.isBefore(endTime)) {
                                      price = timePriceElement.price;
                                      timeBased = true;
                                    }
                                  }
                                });
                              });
                              if (!timeBased) {
                                price = mItemsList[index].basePrice;
                              }
                            } else {
                              price = mItemsList[index].basePrice;
                            }
                            print("asdasdad== $price ${mItemsList[index].basePrice}");
                            if (mItemsList[index].sizeList.isNotEmpty) {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SizeBasedItemsScreen(
                                        item: mItemsList[index],
                                      )));
                            } else if (mItemsList[index].suggestibleItems.isNotEmpty) {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SidesMenuScreen(
                                        item: mItemsList[index],
                                      )));
                            } else if (mItemsList[index].modifiersList.isNotEmpty) {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => ModifiersMenuScreen(
                                        item: mItemsList[index],
                                      )));
                            } else {
                              BotToast.showText(
                                text: "${mItemsList[index].name} $addedSuccessfully",
                                align: Alignment(0, 0),
                              );
                              double itemTax = 0.0;
                              if (mItemsList[index].taxIncludeOption) {
                                if (mItemsList[index].taxrates.isNotEmpty) {
                                  itemTax = getTaxes(mItemsList[index].taxrates, mItemsList[index].basePrice, 1);
                                } else {
                                  itemTax = 0.00;
                                }
                              }
                              print("tax---$itemTax");
                              OrderItems item = OrderItems(
                                  menuId: mItemsList[index].menuId,
                                  menuGroupId: mItemsList[index].menuGroupId,
                                  itemId: mItemsList[index].id,
                                  quantity: 1,
                                  itemName: mItemsList[index].name,
                                  itemDescription: "",
                                  itemImage: mItemsList[index].itemImage,
                                  unitPrice: mItemsList[index].basePrice,
                                  totalPrice: mItemsList[index].basePrice,
                                  itemTax: 0,
                                  discountAmount: 0,
                                  taxIncludeOption: mItemsList[index].taxIncludeOption,
                                  taxrates: mItemsList[index].taxrates,
                                  modifiersList: []);
                              Provider.of<CartViewModel>(context, listen: false).addItemToCart(item);
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => SuggestionsScreen(
                                        item: mItemsList[index],
                                      )));
                            }
                          },
                          child: Container(
                            height: ScreenSize.height(context),
                            width: ScreenSize.width(context),
                            child: Card(
                              elevation: 3,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                              color: Colors.white,
                              child: menuItem(mItemsList[index]),
                            ),
                          ),
                        );
                      });
                } else {
                  return Center(
                    child: Text(
                      noDataAvailable,
                      style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                    ),
                  );
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                  ),
                );
              }
            } else {
              return Center(
                child: Text(
                  pleaseTryAgain,
                  style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            }
          } else {
            return Center(child: loaderWhite());
          }
        });
  }

  Widget menuItem(ItemsList listItem) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
          child: Container(
            padding: EdgeInsets.all(0),
            height: ScreenSize.height(context),
            width: ScreenSize.width(context),
            child: Center(
              child: Image.asset("assets/images/item_bg.png"),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(25), bottomLeft: Radius.circular(25)),
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              width: ScreenSize.width(context),
              child: Center(
                child: Text(
                  listItem.name,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16, color: def_color_red, letterSpacing: 0, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 40,
          left: 0,
          right: 0,
          top: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
            child: Container(
              padding: EdgeInsets.all(20),
              width: ScreenSize.width(context),
              child: Center(
                child: Container(
                  child: listItem.itemImage == "" ? Image.network(restaurantLogo) : Image.network(listItem.itemImage),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }
}
