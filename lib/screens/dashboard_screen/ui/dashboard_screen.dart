import 'dart:convert';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:group_button/group_button.dart';
import 'package:kiosk/models/dinning_options_model.dart';
import 'package:kiosk/models/menu_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/screens/dashboard_screen/viewModel/dashboard_view_model.dart';
import 'package:kiosk/screens/main_menu_screen/ui/main_menu_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/employee_logout_button.dart';
import 'package:provider/provider.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({Key? key}) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  String restaurantLogo = "", restaurantName = "", restaurantId = "";
  late DineInList mDineOption;
  List<DineInList> mDineInList = [];
  int isDineOptionSelectedPos = -1;
  late DashBoardViewModel viewModel;
  bool cartEmpty = false, isEmployeeLogged = false;
  double total = 0.00;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    viewModel = Provider.of<DashBoardViewModel>(context, listen: false);
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantId = value.restaurantId;
        restaurantLogo = value.restaurantLogo;
        restaurantName = value.restaurantName;
      });
    });
    sessionManager.getDineOptions().then((value) {
      print("value $value");
      if (value == "") {
        // showDineOptionsDialog();
        showDineOptionsBottomSheet();
      } else {
        mDineOption = DineInList.fromJson(jsonDecode(value));
      }
    });
    sessionManager.isEmployeeLoggedIn().then((value) {
      setState(() {
        isEmployeeLogged = value!;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<CartViewModel>(context, listen: true).getCartList.isEmpty ? cartEmpty = true : cartEmpty = false;
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: dashboardTopWidget(),
              top: 20,
              left: 30,
              right: 30,
            ),
            Positioned(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                height: ScreenSize.height(context),
                width: ScreenSize.width(context),
                child: Card(
                  color: Colors.white,
                  elevation: 3,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                  child: Stack(
                    children: [
                      Positioned(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 70),
                              child: Image.asset(
                                "assets/images/default_left_bottom_corner.png",
                                width: 100,
                                height: 100,
                              ),
                            ),
                            Flexible(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Center(
                                    child: Image.asset(
                                      "assets/images/default_chicken_center_red.png",
                                      width: 100,
                                      height: 100,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                                    child: Text(
                                      restaurantName,
                                      style: TextStyle(fontSize: 18, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w500),
                                      overflow: TextOverflow.clip,
                                      maxLines: 3,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 70),
                              child: Image.asset(
                                "assets/images/default_right_bottom_corner.png",
                                width: 100,
                                height: 100,
                              ),
                            )
                          ],
                        ),
                        bottom: 0,
                        left: 0,
                        right: 0,
                      ),
                      Positioned(
                        child: Container(
                          height: ScreenSize.height(context),
                          width: ScreenSize.width(context),
                          child: menuList(),
                        ),
                        top: 30,
                        right: 30,
                        left: 30,
                        bottom: ScreenSize.height(context) / 8,
                      ),
                    ],
                  ),
                ),
              ),
              top: ScreenSize.height(context) / 7,
              bottom: 30,
              left: 30,
              right: 30,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardTopWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        moreInfoButton(),
        Spacer(),
        cartEmpty ? SizedBox() : cartCountButton(),
        /*InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => UserLoginScreen()));
          },
          child: userSignInButton(),
        ),*/
        isEmployeeLogged ? employeeLogoutButton(context) : SizedBox(),
        Container(
          margin: EdgeInsets.only(left: 20),
          child: Image.asset(
            "assets/images/default_qr.png",
            height: 75,
            width: 75,
          ),
        )
      ],
    );
  }

  Widget menuItemWidget(MenusList item) {
    return Container(
      margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
      width: ScreenSize.width(context) / 2.4,
      child: InkWell(
          onTap: () {
            /*Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => OffersBannerScreen()));*/
            Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => MainMenuScreen(
                      menuItem: item,
                    )));
          },
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      height: ScreenSize.height(context) / 3,
                      width: ScreenSize.width(context) / 2.3,
                      child: item.menuImage == ""
                          ? Image.network(
                              restaurantLogo,
                              fit: BoxFit.fill,
                            )
                          : Image.network(
                              item.menuImage,
                              fit: BoxFit.fill,
                            ),
                    ),
                  ),
                ),
                flex: 2,
              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                      width: ScreenSize.width(context) / 2.3,
                      padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      decoration: BoxDecoration(gradient: button_red_gradient),
                      child: Center(
                        child: Text(
                          item.menuName == "" ? defItemName.toUpperCase() : item.menuName.toUpperCase(),
                          maxLines: 1,
                          style: TextStyle(fontSize: 24, color: Colors.white, fontFamily: "Swis721", fontWeight: FontWeight.w800),
                        ),
                      )),
                ),
              ),
              //)
            ],
          )),
    );
  }

  Widget menuList() {
    return FutureBuilder(
        future: viewModel.getMenuApi(restaurantId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print("connectionState ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loader());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as MenuModel;
              if (data.responseStatus == 1) {
                if (data.menusList!.isNotEmpty) {
                  return ListView.separated(
                      separatorBuilder: (BuildContext context, int index) {
                        return VerticalDivider(
                          width: 25,
                          color: Colors.transparent,
                        );
                      },
                      controller: PageController(viewportFraction: 1 / 2),
                      physics: PageScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: data.menusList!.length,
                      itemBuilder: (BuildContext context, int index) {
                        return menuItemWidget(data.menusList![index]);
                      });
                } else {
                  return Center(
                    child: Text(
                      noDataAvailable,
                      style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                    ),
                  );
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                  ),
                );
              }
            } else {
              return Center(
                child: Text(
                  pleaseTryAgain,
                  style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            }
          } else {
            return Center(child: loader());
          }
        });
  }

  void showDineOptionsBottomSheet() async {
    await showModalBottomSheet(
        context: context,
        isScrollControlled: false,
        isDismissible: false,
        enableDrag: false,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
        builder: (context) {
          return Container(
              width: ScreenSize.width(context) / 3,
              padding: EdgeInsets.all(25),
              margin: EdgeInsets.all(20),
              child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, crossAxisAlignment: CrossAxisAlignment.center, children: [
                Text(
                  selectDineOptions,
                  style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
                Expanded(
                  child: FutureBuilder(
                    future: viewModel.dineOptionsApi(restaurantId),
                    builder: (context, AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(child: loader());
                      } else if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                          return Center(
                            child: Text(
                              '${snapshot.error} occurred',
                              style: TextStyle(
                                  fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                            ),
                          );
                        } else if (snapshot.hasData) {
                          var data = snapshot.data as DinningOptionsModel;
                          if (data.responseStatus == 1) {
                            if (data.dineInList!.isNotEmpty) {
                              List<String> optionNameList = [];
                              data.dineInList!.forEach((element) {
                                optionNameList.add(element.optionName);
                              });
                              return ListView(
                                shrinkWrap: true,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: GroupButton(
                                      buttonWidth: ScreenSize.width(context),
                                      elevation: 1,
                                      groupingType: GroupingType.column,
                                      selectedTextStyle:
                                          TextStyle(fontSize: 25, fontFamily: "Swis721", fontWeight: FontWeight.w300, letterSpacing: 0.63),
                                      unselectedTextStyle: TextStyle(
                                          fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                                      selectedColor: def_color_red,
                                      unselectedColor: Colors.white,
                                      textPadding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                      borderRadius: BorderRadius.circular(5.0),
                                      buttons: optionNameList,
                                      onSelected: (index, isSelected) {
                                        print('$index button is selected -- ${data.dineInList![index].optionName}');
                                        Provider.of<DashBoardViewModel>(context, listen: false).setSelected(index);
                                        Provider.of<DashBoardViewModel>(context, listen: false).setDineInList(data.dineInList![index]);
                                      },
                                    ),
                                  )
                                ],
                              );
                            } else {
                              return Center(
                                child: Text(
                                  noDataAvailable,
                                  style: TextStyle(
                                      fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                                ),
                              );
                            }
                          } else {
                            return Center(
                              child: Text(
                                data.result,
                                style: TextStyle(
                                    fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                              ),
                            );
                          }
                        } else {
                          return Center(
                            child: Text(
                              pleaseTryAgain,
                              style: TextStyle(
                                  fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                            ),
                          );
                        }
                      } else {
                        return Center(child: loader());
                      }
                    },
                  ),
                ),
                Container(
                    margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                    width: 150,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), gradient: button_red_gradient),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(25),
                        child: TextButton(
                          onPressed: () {
                            if (Provider.of<DashBoardViewModel>(context, listen: false).getSelectedIndex == -1) {
                              BotToast.showText(
                                text: "Please select any Dine Option to proceed.",
                                align: Alignment(0, 0),
                              );
                            } else {
                              SessionManager().saveDineOptions(jsonEncode(Provider.of<DashBoardViewModel>(context, listen: false).getDineInList));
                              Navigator.of(context).pop();
                            }
                          },
                          child: Padding(
                            padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                            child: Text(
                              submit,
                              style: TextStyle(
                                  fontSize: 22, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                            ),
                          ),
                        ))),
              ]));
        });
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: button_red_gradient,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 20),
                      child: CircleAvatar(
                        child: Padding(
                          padding: EdgeInsets.all(8),
                          child: Image.asset(
                            "assets/images/cart.png",
                            height: 30,
                            width: 30,
                          ),
                        ),
                        backgroundColor: Colors.white,
                      ),
                    ),
                    Expanded(
                        child: Center(
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "\u0024",
                            style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                          ),
                          Text(
                            total.toStringAsFixed(2),
                            style: TextStyle(
                                fontSize: 20, color: Colors.white, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                          )
                        ],
                      ),
                    ))
                  ],
                ),
              ),
            )));
  }

  Widget moreInfoButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: button_red_gradient,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {},
              child: Padding(
                padding: EdgeInsets.fromLTRB(0, 13, 10, 13),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.more_vert,
                      color: Colors.white,
                    ),
                    Text(
                      moreInfo.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: "Swis721", fontWeight: FontWeight.w500),
                    )
                  ],
                ),
              ),
            )));
  }
}
