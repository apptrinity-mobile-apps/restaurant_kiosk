import 'package:flutter/material.dart';
import 'package:kiosk/models/dinning_options_model.dart';
import 'package:kiosk/models/menu_model.dart';
import 'package:kiosk/services/repositories.dart';

class DashBoardViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  DinningOptionsModel? _dinningOptionsModel;
  DineInList? _dineInList;
  MenuModel? _menuModel;
  int selected = -1;

  DinningOptionsModel? get dinningOptionsModel {
    return _dinningOptionsModel;
  }

  Future<DinningOptionsModel?> dineOptionsApi(String restaurantId) async {
    _isFetching = true;
    _isHavingData = false;
    _dinningOptionsModel = DinningOptionsModel(result: '', responseStatus: 0, dineInList: []);
    notifyListeners();
    try {
      dynamic response = await Repository().dinningOptions(restaurantId);
      if (response != null) {
        _dinningOptionsModel = DinningOptionsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("dinningOptionsModel $_isFetching $_isHavingData $_dinningOptionsModel");
    notifyListeners();
    return _dinningOptionsModel;
  }

  Future<MenuModel?> getMenuApi(String restaurantId) async {
    _isFetching = true;
    _isHavingData = false;
    _menuModel = MenuModel(result: '', responseStatus: 0, menusList: []);
    notifyListeners();
    try {
      dynamic response = await Repository().menus(restaurantId);
      if (response != null) {
        _menuModel = MenuModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_menuModel $_isFetching $_isHavingData $_menuModel");
    notifyListeners();
    return _menuModel;
  }

  int get getSelectedIndex {
    return selected;
  }

  void setSelected(int position) {
    selected = position;
    notifyListeners();
  }

  void setDineInList(DineInList option) {
    _dineInList = option;
    notifyListeners();
  }

  DineInList? get getDineInList {
    return _dineInList;
  }
}
