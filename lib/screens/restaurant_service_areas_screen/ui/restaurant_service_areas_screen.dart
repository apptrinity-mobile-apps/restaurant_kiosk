import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:group_button/group_button.dart';
import 'package:kiosk/models/add_kiosk_model.dart';
import 'package:kiosk/models/restaurant_login_model.dart';
import 'package:kiosk/models/service_areas_model.dart';
import 'package:kiosk/screens/landing_banner_screen/ui/landing_banners_screen.dart';
import 'package:kiosk/screens/restaurant_login_screen/viewModel/restaurant_login_view_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/utils/snack_bar.dart';
import 'package:kiosk/viewModel/device_setup_view_model.dart';
import 'package:provider/provider.dart';

class ServiceAreasScreen extends StatefulWidget {
  const ServiceAreasScreen({Key? key}) : super(key: key);

  @override
  _ServiceAreasScreenState createState() => _ServiceAreasScreenState();
}

class _ServiceAreasScreenState extends State<ServiceAreasScreen> {
  late RestaurantLoginViewModel restaurantLoginViewModel;
  late DeviceSetupViewModel deviceSetupViewModel;
  late RestaurantLoginModel restaurantLoginModel;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    restaurantLoginViewModel = Provider.of<RestaurantLoginViewModel>(context, listen: false);
    deviceSetupViewModel = Provider.of<DeviceSetupViewModel>(context, listen: false);
    restaurantLoginModel = restaurantLoginViewModel.restaurantLoginModel!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.black12,
      child: Stack(children: [
        Container(
          width: ScreenSize.width(context),
          height: ScreenSize.height(context),
          child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
        ),
        Positioned(
          child: Container(
            width: ScreenSize.width(context),
            height: ScreenSize.height(context),
            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                selectServiceArea,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 24,
                  fontFamily: "Swis721",
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(5, 5, 5, 5),
                  width: ScreenSize.width(context),
                  height: 200,
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
                  child: FutureBuilder(
                    future: deviceSetupViewModel.getServiceAreasApi(restaurantLoginModel.restaurantId),
                    builder: (context, AsyncSnapshot<dynamic> snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Center(child: loader());
                      } else if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.hasError) {
                          return Center(
                            child: Text(
                              '${snapshot.error} occurred',
                              style: TextStyle(
                                  fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                            ),
                          );
                        } else if (snapshot.hasData) {
                          var data = snapshot.data as ServiceAreasModel;
                          if (data.responseStatus == 1) {
                            if (data.serviceAreasList!.isNotEmpty) {
                              List<String> optionNameList = [];
                              data.serviceAreasList!.forEach((element) {
                                optionNameList.add(element.serviceName!);
                              });
                              return ListView(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                    child: GroupButton(
                                      buttonWidth: ScreenSize.width(context),
                                      mainGroupAlignment: MainGroupAlignment.start,
                                      crossGroupAlignment: CrossGroupAlignment.start,
                                      elevation: 1,
                                      groupingType: GroupingType.column,
                                      spacing: 7,
                                      runSpacing: 10,
                                      alignment: Alignment.centerLeft,
                                      selectedTextStyle:
                                          TextStyle(fontSize: 25, fontFamily: "Swis721", fontWeight: FontWeight.w300, letterSpacing: 0.63),
                                      unselectedTextStyle: TextStyle(
                                          fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                                      selectedColor: def_color_red,
                                      unselectedColor: Colors.white,
                                      textPadding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                                      borderRadius: BorderRadius.circular(5.0),
                                      buttons: optionNameList,
                                      onSelected: (index, isSelected) {
                                        print('$index button is selected -- ${data.serviceAreasList![index].serviceName}');
                                        deviceSetupViewModel.saveServiceArea(data.serviceAreasList![index].id!);
                                      },
                                    ),
                                  )
                                ],
                                shrinkWrap: true,
                              );
                            } else {
                              return Center(
                                child: Text(
                                  noDataAvailable,
                                  style: TextStyle(
                                      fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                                ),
                              );
                            }
                          } else {
                            return Center(
                              child: Text(
                                data.result!,
                                style: TextStyle(
                                    fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                              ),
                            );
                          }
                        } else {
                          return Center(
                            child: Text(
                              pleaseTryAgain,
                              style: TextStyle(
                                  fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                            ),
                          );
                        }
                      } else {
                        return Center(child: loader());
                      }
                    },
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: 120,
                      margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.white),
                      child: TextButton(
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: Text(
                            back.toUpperCase(),
                            style: TextStyle(
                              fontSize: 21,
                              fontFamily: "Swis721",
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.53,
                              color: def_color_red,
                            ),
                          ),
                        ),
                      )),
                  Container(
                      width: 120,
                      margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.white),
                      child: TextButton(
                        onPressed: () async {
                          if (deviceSetupViewModel.serviceAreaId == "") {
                            showSnackBar(context, "Please select service area");
                          } else {
                            showDialog(
                                context: context,
                                barrierDismissible: false,
                                builder: (BuildContext context) {
                                  return loader();
                                });
                            await deviceSetupViewModel
                                .addKioskDevice(
                              restaurantLoginModel.restaurantId,
                              deviceSetupViewModel.deviceName,
                              "",
                              deviceSetupViewModel.serviceAreaId,
                            )
                                .then((value) {
                              Navigator.of(context).pop();
                              AddActiveKioskModel? addActiveKioskModel;
                              if (deviceSetupViewModel.activeKioskModel != null) {
                                setState(() {
                                  addActiveKioskModel = deviceSetupViewModel.activeKioskModel;
                                });
                              }
                              if (addActiveKioskModel!.responseStatus == 1) {
                                sessionManager.login(true);
                                sessionManager.saveRestaurantDetails(jsonEncode(restaurantLoginModel));
                                sessionManager.saveDeviceId(addActiveKioskModel!.deviceId!);
                                sessionManager.saveServiceAreaId(deviceSetupViewModel.serviceAreaId);
                                Navigator.pushAndRemoveUntil(
                                    context, MaterialPageRoute(builder: (context) => LandingBannerScreen()), (route) => false);
                              } else {
                                showSnackBar(context, addActiveKioskModel!.result!);
                              }
                            });
                          }
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: Text(
                            submit.toUpperCase(),
                            style: TextStyle(
                              fontSize: 21,
                              fontFamily: "Swis721",
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.53,
                              color: def_color_red,
                            ),
                          ),
                        ),
                      )),
                ],
              )
            ]),
          ),
          top: ScreenSize.height(context) / 6,
          bottom: ScreenSize.height(context) / 6,
          right: ScreenSize.width(context) / 5,
          left: ScreenSize.width(context) / 5,
        )
      ]),
    ));
  }
}
