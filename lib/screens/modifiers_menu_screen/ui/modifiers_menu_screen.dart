import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/screens/suggestions_screen/ui/suggestions_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/math_calculations.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:provider/provider.dart';

class ModifiersMenuScreen extends StatefulWidget {
  final ItemsList item;

  const ModifiersMenuScreen({Key? key, required this.item}) : super(key: key);

  @override
  _ModifiersMenuScreenState createState() => _ModifiersMenuScreenState();
}

class _ModifiersMenuScreenState extends State<ModifiersMenuScreen> {
  late ItemsList mMenuItem;
  String restaurantLogo = "", modifierGroupName = "";
  int modifiersSize = -1, currentPage = 0, selectedPos = -1;
  bool isSelected = false;
  List<ModifierList> modifiersList = [], tempModifiersList = [];
  List<OrderItems> cartList = [], tempCartList = [];
  double total = 0.00;
  int totalItems = 0;
  List<String> mImagesList = [];
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    mMenuItem = widget.item;
    modifiersSize = mMenuItem.modifiersList.length;
    modifierGroupName = mMenuItem.modifiersList[currentPage].name;
    mImagesList.add("assets/images/mod_1.png");
    mImagesList.add("assets/images/mod_2.png");
    mImagesList.add("assets/images/mod_3.png");
    mImagesList.add("assets/images/mod_4.png");
    mImagesList.add("assets/images/mod_5.png");
    mImagesList.add("assets/images/mod_6.png");
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantLogo = value.restaurantLogo;
      });
    });
    cartList = Provider.of<CartViewModel>(context, listen: false).getCartList;
    tempCartList = [];
    tempCartList = cartList;
    OrderItems item = OrderItems(
        menuId: mMenuItem.menuId,
        menuGroupId: mMenuItem.menuGroupId,
        itemId: mMenuItem.id,
        quantity: 1,
        itemName: mMenuItem.name,
        itemDescription: "",
        itemImage: mMenuItem.itemImage,
        unitPrice: mMenuItem.basePrice,
        totalPrice: mMenuItem.basePrice,
        itemTax: 0,
        discountAmount: 0,
        taxIncludeOption: mMenuItem.taxIncludeOption,
        taxrates: mMenuItem.taxrates,
        modifiersList: []);
    tempCartList.add(item);
    print("mod length ${tempCartList.length}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 1.7,
                child: Container(
                  width: ScreenSize.width(context),
                  height: ScreenSize.height(context),
                  margin: EdgeInsets.only(right: 0),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        width: ScreenSize.width(context),
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child: RichText(
                          textScaleFactor: 1.5,
                          text: TextSpan(
                            text: '',
                            children: <TextSpan>[
                              TextSpan(
                                text: chooseYour,
                                style: TextStyle(
                                    fontSize: 28, color: Colors.white, fontFamily: "Palace", letterSpacing: 2.25, fontWeight: FontWeight.w500),
                              ),
                              TextSpan(
                                text: modifierGroupName.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 28, color: Colors.white, letterSpacing: 1.89, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          width: ScreenSize.width(context),
                          child: ListView(
                            children: [
                              GridView.builder(
                                  itemCount: mMenuItem.modifiersList[currentPage].optionsList.length,
                                  shrinkWrap: true,
                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    crossAxisSpacing: 8,
                                    mainAxisSpacing: 8,
                                    childAspectRatio: (1),
                                  ),
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {
                                        ModifierList modifier = ModifierList(
                                            id: mMenuItem.modifiersList[currentPage].optionsList[index].id,
                                            optionName: mMenuItem.modifiersList[currentPage].optionsList[index].optionName,
                                            price: mMenuItem.modifiersList[currentPage].optionsList[index].price);
                                        setState(() {
                                          if (mMenuItem.modifiersList[currentPage].multipleSelect) {
                                            if (modifiersList.isNotEmpty) {
                                              var _removed = false;
                                              for (var item in modifiersList) {
                                                if (item.id == modifier.id) {
                                                  selectedPos = -1;
                                                  modifiersList.remove(item);
                                                  _removed = true;
                                                  break;
                                                }
                                              }
                                              if (!_removed) {
                                                selectedPos = index;
                                                modifiersList.add(modifier);
                                              }
                                            } else {
                                              selectedPos = index;
                                              modifiersList.add(modifier);
                                            }
                                          } else {
                                            if (selectedPos == -1) {
                                              if (tempModifiersList.isEmpty) {
                                                tempModifiersList.add(modifier);
                                                selectedPos = index;
                                                modifiersList.add(modifier);
                                              }
                                            } else if (selectedPos == index) {
                                              if (tempModifiersList.isNotEmpty) {
                                                for (var item in tempModifiersList) {
                                                  if (item.id == modifier.id) {
                                                    selectedPos = -1;
                                                    tempModifiersList.remove(item);
                                                    modifiersList.remove(item);
                                                    break;
                                                  }
                                                }
                                              }
                                            } else {
                                              // do not add extra modifiers
                                              BotToast.showText(
                                                text: "You can add only one item from ${mMenuItem.modifiersList[currentPage].name}!",
                                                align: Alignment(0, 0),
                                              );
                                            }
                                          }
                                          print("ssss ${modifiersList.length}");
                                          double itemTax = 0.0;
                                          if (mMenuItem.taxIncludeOption) {
                                            if (mMenuItem.taxrates.isNotEmpty) {
                                              itemTax = getTaxes(mMenuItem.taxrates, mMenuItem.basePrice, 1);
                                            } else {
                                              itemTax = 0.00;
                                            }
                                          }
                                          print("tax---$itemTax");
                                          OrderItems item = OrderItems(
                                              menuId: mMenuItem.menuId,
                                              menuGroupId: mMenuItem.menuGroupId,
                                              itemId: mMenuItem.id,
                                              quantity: 1,
                                              itemName: mMenuItem.name,
                                              itemDescription: "",
                                              itemImage: mMenuItem.itemImage,
                                              unitPrice: mMenuItem.basePrice,
                                              totalPrice: mMenuItem.basePrice,
                                              itemTax: 0,
                                              discountAmount: 0,
                                              taxIncludeOption: mMenuItem.taxIncludeOption,
                                              taxrates: mMenuItem.taxrates,
                                              modifiersList: modifiersList);
                                          Provider.of<CartViewModel>(context, listen: false).updateCartItem(tempCartList.length - 1, item);
                                        });
                                      },
                                      child: Container(
                                        height: ScreenSize.height(context),
                                        width: ScreenSize.width(context),
                                        child: Card(
                                          elevation: 3,
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                          color: Colors.white,
                                          child: menuItem(mMenuItem.modifiersList[currentPage].optionsList[index], index, selectedPos),
                                        ),
                                      ),
                                    );
                                  }),
                            ],
                          ),
                        ),
                        flex: 1,
                      ),
                      Container(
                        width: ScreenSize.width(context),
                        margin: EdgeInsets.fromLTRB(0, 0, 10, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            mMenuItem.modifiersList[currentPage].isRequired != 1
                                ? Container(
                                    margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(25),
                                        child: TextButton(
                                            onPressed: () {
                                              tempModifiersList = [];
                                              double itemTax = 0.0;
                                              if (mMenuItem.taxIncludeOption) {
                                                if (mMenuItem.taxrates.isNotEmpty) {
                                                  itemTax = getTaxes(mMenuItem.taxrates, mMenuItem.basePrice, 1);
                                                } else {
                                                  itemTax = 0.00;
                                                }
                                              }
                                              print("tax---$itemTax");
                                              OrderItems item = OrderItems(
                                                  menuId: mMenuItem.menuId,
                                                  menuGroupId: mMenuItem.menuGroupId,
                                                  itemId: mMenuItem.id,
                                                  quantity: 1,
                                                  itemName: mMenuItem.name,
                                                  itemDescription: "",
                                                  itemImage: mMenuItem.itemImage,
                                                  unitPrice: mMenuItem.basePrice,
                                                  totalPrice: mMenuItem.basePrice,
                                                  itemTax: 0,
                                                  discountAmount: 0,
                                                  taxIncludeOption: mMenuItem.taxIncludeOption,
                                                  taxrates: mMenuItem.taxrates,
                                                  modifiersList: []);
                                              Provider.of<CartViewModel>(context, listen: false).updateCartItem(tempCartList.length - 1, item);
                                              if (modifiersSize - 1 > currentPage) {
                                                setState(() {
                                                  currentPage++;
                                                  selectedPos = -1;
                                                  modifierGroupName = mMenuItem.modifiersList[currentPage].name;
                                                });
                                              } else {
                                                Navigator.of(context).push(MaterialPageRoute(
                                                    builder: (context) => SuggestionsScreen(
                                                          item: mMenuItem,
                                                        )));
                                              }
                                            },
                                            child: Padding(
                                              padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                                              child: Text(
                                                noThanks.toUpperCase(),
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.white,
                                                    letterSpacing: 0.9,
                                                    fontFamily: "Swis721-Blk",
                                                    fontWeight: FontWeight.w500),
                                              ),
                                            ))))
                                : SizedBox(),
                            Container(
                              height: 100,
                              child: Image.asset(
                                "assets/images/default_chicken_right_white.png",
                                fit: BoxFit.fill,
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  gradient: button_red_gradient,
                                ),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(25),
                                    child: TextButton(
                                      onPressed: () {
                                        bool hasItemAdded = false;
                                        int currentPageModifiers = 0;
                                        modifiersList.forEach((element) {
                                          mMenuItem.modifiersList[currentPage].optionsList.forEach((modifier) {
                                            if (element.id == modifier.id) {
                                              setState(() {
                                                hasItemAdded = true;
                                                currentPageModifiers++;
                                              });
                                            }
                                          });
                                        });
                                        if (mMenuItem.modifiersList[currentPage].isRequired == 1) {
                                          if (hasItemAdded) {
                                            if (mMenuItem.modifiersList[currentPage].multipleSelect) {
                                              multipleSelectConditions(currentPageModifiers);
                                            } else {
                                              continueButtonAddModifiers();
                                            }
                                          } else {
                                            BotToast.showText(
                                              text: "Add an item from ${mMenuItem.modifiersList[currentPage].name} to proceed.",
                                              align: Alignment(0, 0),
                                            );
                                          }
                                        } else {
                                          if (mMenuItem.modifiersList[currentPage].multipleSelect) {
                                            multipleSelectConditions(currentPageModifiers);
                                          } else {
                                            continueButtonAddModifiers();
                                          }
                                        }
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                                        child: Text(
                                          continue_str.toUpperCase(),
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.white,
                                              letterSpacing: 0.9,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    )))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              top: 20,
              left: 30,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 2.8,
                decoration:
                    BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                child: Column(
                  children: [
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                          gradient: button_red_gradient,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                      child: Center(
                        child: Text(
                          yourItems.toUpperCase(),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, color: Colors.white, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    Expanded(
                        child: Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: ListView.builder(
                          itemCount: tempCartList.length,
                          itemBuilder: (context, index) {
                            return Container(
                              width: ScreenSize.width(context),
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Column(children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        child: Text(
                                          tempCartList[index].itemName.toUpperCase(),
                                          maxLines: 2,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                    Center(
                                      child: Container(
                                        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                        child: Text(
                                          tempCartList[index].quantity.toString(),
                                          maxLines: 2,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                        child: Padding(
                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "\u0024",
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Text(
                                            (tempCartList[index].unitPrice * tempCartList[index].quantity).toStringAsFixed(2),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          )
                                        ],
                                      ),
                                    )),
                                  ],
                                ),
                                tempCartList[index].modifiersList.isEmpty
                                    ? SizedBox()
                                    : ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: tempCartList[index].modifiersList.length,
                                        itemBuilder: (context, i) {
                                          return Container(
                                              width: ScreenSize.width(context),
                                              padding: EdgeInsets.fromLTRB(0, 1, 0, 1),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                      child: Text(
                                                        tempCartList[index].modifiersList[i].optionName.toUpperCase(),
                                                        maxLines: 2,
                                                        textAlign: TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.black38,
                                                            letterSpacing: 0.3,
                                                            fontFamily: "Swis721-Blk",
                                                            fontWeight: FontWeight.w500),
                                                      ),
                                                    ),
                                                    flex: 2,
                                                  ),
                                                  Expanded(
                                                      child: Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Text(
                                                          "\u0024",
                                                          maxLines: 1,
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        ),
                                                        Text(
                                                          tempCartList[index].modifiersList[i].price.toString(),
                                                          maxLines: 1,
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        )
                                                      ],
                                                    ),
                                                  )),
                                                ],
                                              ));
                                        })
                              ]),
                            );
                          }),
                    )),
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                        gradient: button_red_gradient,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Text(
                                itemTotal.toUpperCase(),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                              ),
                            ),
                            flex: 2,
                          ),
                          Expanded(
                              child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "\u0024",
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  total.toStringAsFixed(2),
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              right: 20,
              top: 20,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    if (currentPage != 0) {
                      setState(() {
                        currentPage--;
                        selectedPos = -1;
                        modifierGroupName = mMenuItem.modifiersList[currentPage].name;
                      });
                    } else {
                      OrderItems item = OrderItems(
                          menuId: mMenuItem.menuId,
                          menuGroupId: mMenuItem.menuGroupId,
                          itemId: mMenuItem.id,
                          quantity: 1,
                          itemName: mMenuItem.name,
                          itemDescription: "",
                          itemImage: mMenuItem.itemImage,
                          unitPrice: mMenuItem.basePrice,
                          totalPrice: mMenuItem.basePrice,
                          itemTax: 0,
                          discountAmount: 0,
                          taxIncludeOption: mMenuItem.taxIncludeOption,
                          taxrates: mMenuItem.taxrates,
                          modifiersList: []);
                      Provider.of<CartViewModel>(context, listen: false).updateCartItem(tempCartList.length - 1, item);
                      Navigator.of(context).pop();
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget menuItem(OptionsList listItem, int position, int selectedPosition) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
          child: Container(
            padding: EdgeInsets.all(0),
            height: ScreenSize.height(context),
            width: ScreenSize.width(context),
            child: Center(
              child: Image.asset("assets/images/item_bg.png"),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(25), bottomLeft: Radius.circular(25)),
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              width: ScreenSize.width(context),
              child: Center(
                child: Text(
                  listItem.optionName,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16, color: def_color_red, letterSpacing: 0, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 40,
          left: 0,
          right: 0,
          top: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
            child: Container(
              padding: EdgeInsets.all(20),
              width: ScreenSize.width(context),
              child: Center(
                child: Container(
                  child: listItem.modifierImage == "" ? Image.network(restaurantLogo) : Image.network(listItem.modifierImage),
                ),
              ),
            ),
          ),
        ),
        position == selectedPosition
            ? Positioned(
                top: 0,
                right: 0,
                child: Image.asset(
                  "assets/images/item_selected_tick.png",
                  fit: BoxFit.fill,
                  height: 40,
                  width: 40,
                ),
              )
            : SizedBox()
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }

  void multipleSelectConditions(int modifiersInCurrentPage) {
    int min = 0, max = 0;
    setState(() {
      min = mMenuItem.modifiersList[currentPage].minSelections;
      max = mMenuItem.modifiersList[currentPage].maxSelections;
    });
    print("sizes--- $min $max $modifiersInCurrentPage");
    if (min != -1 && modifiersInCurrentPage < min) {
      BotToast.showText(
        text: "You need to add minimum of $min item(s) from ${mMenuItem.modifiersList[currentPage].name}!",
        align: Alignment(0, 0),
      );
    } else if (max != -1 && modifiersInCurrentPage != max) {
      BotToast.showText(
        text: "You must add maximum of $max item(s) from ${mMenuItem.modifiersList[currentPage].name}!",
        align: Alignment(0, 0),
      );
    } else if (max != -1 && modifiersInCurrentPage > max) {
      BotToast.showText(
        text: "You can add maximum of $max item(s) from ${mMenuItem.modifiersList[currentPage].name}!",
        align: Alignment(0, 0),
      );
    } else {
      continueButtonAddModifiers();
    }
  }

  void continueButtonAddModifiers() {
    tempModifiersList = [];
    if (modifiersSize - 1 > currentPage) {
      setState(() {
        currentPage++;
        selectedPos = -1;
        modifierGroupName = mMenuItem.modifiersList[currentPage].name;
      });
    } else {
      BotToast.showText(
        text: "${mMenuItem.name} $addedSuccessfully",
        align: Alignment(0, 0),
      );
      Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => SuggestionsScreen(
                item: mMenuItem,
              )));
    }
  }
}
