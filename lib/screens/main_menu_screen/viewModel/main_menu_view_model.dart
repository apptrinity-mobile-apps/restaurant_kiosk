import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/services/repositories.dart';

class MainMenuViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  MenuItemsModel? _itemsViewModel;
  double selectedPage = 0.0;

  Future<MenuItemsModel?> getPopularSideMenuItemsApi(String restaurantId, String menuId) async {
    _isFetching = true;
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response = await Repository().getPopularMenuItems(restaurantId, menuId);
      if (response != null) {
        _itemsViewModel = MenuItemsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("getPopularSideMenuItems $_isFetching $_isHavingData $_itemsViewModel");
    notifyListeners();
    return _itemsViewModel;
  }

  void pageSelected(double page) {
    selectedPage = page;
    notifyListeners();
  }

  double get page {
    return selectedPage;
  }
}
