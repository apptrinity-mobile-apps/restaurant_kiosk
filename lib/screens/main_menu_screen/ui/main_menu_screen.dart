import 'package:bot_toast/bot_toast.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/models/menu_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/screens/main_menu_screen/viewModel/main_menu_view_model.dart';
import 'package:kiosk/screens/menu_full/ui/full_menu_screen.dart';
import 'package:kiosk/screens/menu_new_items/ui/new_items_menu_screen.dart';
import 'package:kiosk/screens/menu_top_picks/ui/top_picks_menu_screen.dart';
import 'package:kiosk/screens/menu_valuez/ui/valuez_menu_screen.dart';
import 'package:kiosk/screens/modifiers_menu_screen/ui/modifiers_menu_screen.dart';
import 'package:kiosk/screens/sides_menu_screen/ui/sides_menu_screen.dart';
import 'package:kiosk/screens/size_based_screen/ui/size_based_items_screen.dart';
import 'package:kiosk/screens/suggestions_screen/ui/suggestions_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/math_calculations.dart';
import 'package:kiosk/utils/progress_loading_white.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:provider/provider.dart';

class MainMenuScreen extends StatefulWidget {
  final MenusList menuItem;

  const MainMenuScreen({Key? key, required this.menuItem}) : super(key: key);

  @override
  _MainMenuScreenState createState() => _MainMenuScreenState();
}

class _MainMenuScreenState extends State<MainMenuScreen> {
  String mMenuId = "", restaurantLogo = "", restaurantName = "", restaurantId = "", currentDay = "", currentTime = "";
  late DateTime time;
  List<OrderItems> cartList = [];
  double total = 0.00;
  int totalItems = 0;
  late MainMenuViewModel viewModel;
  double currentIndexPage = 0.0;
  PageController controller = PageController(initialPage: 0);
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    mMenuId = widget.menuItem.id;
    Provider.of<MainMenuViewModel>(context, listen: false).pageSelected(0);
    controller.addListener(() {
      Provider.of<MainMenuViewModel>(context, listen: false).pageSelected(controller.page!);
    });
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantName = value.restaurantName;
        restaurantId = value.restaurantId;
        restaurantLogo = value.restaurantLogo;
        viewModel = Provider.of<MainMenuViewModel>(context, listen: false);
      });
    });
    DateTime today = DateTime.now();
    setState(() {
      currentDay = DateFormat('EEEE').format(today);
      currentTime = DateFormat('HH:mm').format(today);
    });
    print("date---" + DateFormat('EEEE').format(today) + "----" + DateFormat('HH:mm').format(today));
    cartList = Provider.of<CartViewModel>(context, listen: false).getCartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                alignment: Alignment.topLeft,
                width: ScreenSize.width(context) / 7,
                height: ScreenSize.height(context) / 3,
                child: Image.asset(
                  "assets/images/default_menu_left_top.png",
                ),
              ),
              top: 0,
              left: 0,
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 2.8,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.7),
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                child: Column(
                  children: [
                    Expanded(
                        child: Container(
                      margin: EdgeInsets.fromLTRB(15, 15, 15, 5),
                      height: ScreenSize.height(context),
                      width: ScreenSize.width(context),
                      child: Card(
                        elevation: 3,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                        color: def_color_red,
                        child: Column(
                          children: [
                            // TODO uncomment
                            /*Expanded(
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
                                      child: Container(
                                        padding: EdgeInsets.all(15),
                                        child: Center(
                                          child: Text(
                                            restaurantName.toUpperCase(),
                                            maxLines: 2,
                                            style: TextStyle(
                                                fontSize: 26,
                                                color: Colors.white,
                                                letterSpacing: 0.6,
                                                fontFamily: "Swis721",
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ),
                                      ),
                                    )),
                                Expanded(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25), bottomRight: Radius.circular(25)),
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Image.network(
                                        restaurantLogo,
                                        fit: BoxFit.fill,
                                      ),
                                    ),
                                  ),
                                  flex: 2,
                                ),*/
                            ClipRRect(
                              borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
                              child: Container(
                                padding: EdgeInsets.all(15),
                                child: Column(
                                  children: [
                                    Center(
                                      child: Text(
                                        "feed the team!",
                                        maxLines: 2,
                                        style: TextStyle(
                                            fontSize: 30,
                                            color: Colors.white,
                                            letterSpacing: 0.6,
                                            fontFamily: "Swis721",
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ),
                                    Center(
                                      child: Text(
                                        "ask us about family deals",
                                        maxLines: 2,
                                        style: TextStyle(
                                            fontSize: 26,
                                            color: Colors.white,
                                            letterSpacing: 0.6,
                                            fontFamily: "Swis721",
                                            fontWeight: FontWeight.w700),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25), bottomRight: Radius.circular(25)),
                                child: Container(
                                  width: ScreenSize.width(context),
                                  padding: EdgeInsets.all(0),
                                  child: Image.asset(
                                    "assets/images/feed_team.png",
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                              flex: 2,
                            ),
                          ],
                        ),
                      ),
                    )),
                    Expanded(
                        child: Container(
                      margin: EdgeInsets.fromLTRB(15, 10, 15, 15),
                      height: ScreenSize.height(context),
                      width: ScreenSize.width(context),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), gradient: button_red_gradient),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(25),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => TopPicksMenuScreen()));
                                    },
                                    child: Padding(
                                        padding: EdgeInsets.all(5),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(left: 15, right: 10),
                                              child: Text(
                                                topPicks.toUpperCase(),
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.white,
                                                    letterSpacing: 0.6,
                                                    fontFamily: "Swis721",
                                                    fontWeight: FontWeight.w700),
                                              ),
                                            )),
                                            Container(
                                              child: CircleAvatar(
                                                child: Center(
                                                  child: Padding(
                                                    padding: EdgeInsets.all(8),
                                                    child: Image.asset("assets/images/top_picks.png"),
                                                  ),
                                                ),
                                                backgroundColor: Colors.white,
                                              ),
                                            ),
                                          ],
                                        )),
                                  ))),
                          Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), gradient: button_red_gradient),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(25),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => ValuezItemsMenuScreen()));
                                    },
                                    child: Padding(
                                        padding: EdgeInsets.all(5),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(left: 15, right: 10),
                                              child: Text(
                                                valuez.toUpperCase(),
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.white,
                                                    letterSpacing: 0.6,
                                                    fontFamily: "Swis721",
                                                    fontWeight: FontWeight.w700),
                                              ),
                                            )),
                                            Container(
                                              child: CircleAvatar(
                                                child: Center(
                                                  child: Padding(
                                                    padding: EdgeInsets.all(8),
                                                    child: Image.asset("assets/images/valuez.png"),
                                                  ),
                                                ),
                                                backgroundColor: Colors.white,
                                              ),
                                            ),
                                          ],
                                        )),
                                  ))),
                          Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), gradient: button_red_gradient),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(25),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => NewItemsMenuScreen()));
                                    },
                                    child: Padding(
                                        padding: EdgeInsets.all(5),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(left: 15, right: 10),
                                              child: Text(
                                                newItems.toUpperCase(),
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.white,
                                                    letterSpacing: 0.6,
                                                    fontFamily: "Swis721",
                                                    fontWeight: FontWeight.w700),
                                              ),
                                            )),
                                            Container(
                                              child: CircleAvatar(
                                                child: Center(
                                                  child: Padding(
                                                    padding: EdgeInsets.all(8),
                                                    child: Image.asset("assets/images/new_item_red.png"),
                                                  ),
                                                ),
                                                backgroundColor: Colors.white,
                                              ),
                                            ),
                                          ],
                                        )),
                                  ))),
                          Container(
                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), gradient: button_red_gradient),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(25),
                                  child: TextButton(
                                    onPressed: () {
                                      Navigator.of(context).push(MaterialPageRoute(
                                          builder: (context) => FullMenuScreen(
                                                menuId: mMenuId,
                                              )));
                                    },
                                    child: Padding(
                                        padding: EdgeInsets.all(5),
                                        child: Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Expanded(
                                                child: Container(
                                              margin: EdgeInsets.only(left: 15, right: 10),
                                              child: Text(
                                                fullMenu.toUpperCase(),
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    color: Colors.white,
                                                    letterSpacing: 0.6,
                                                    fontFamily: "Swis721",
                                                    fontWeight: FontWeight.w700),
                                              ),
                                            )),
                                            Container(
                                              child: CircleAvatar(
                                                child: Center(
                                                  child: Padding(
                                                    padding: EdgeInsets.all(8),
                                                    child: Image.asset("assets/images/full_menu.png"),
                                                  ),
                                                ),
                                                backgroundColor: Colors.white,
                                              ),
                                            ),
                                          ],
                                        )),
                                  ))),
                        ],
                      ),
                    ))
                  ],
                ),
              ),
              right: 20,
              top: 40,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: Container(
                width: ScreenSize.width(context) / 1.9,
                height: ScreenSize.height(context),
                child: menuIdBasedPopularSideMenu(),
              ),
              top: 70,
              left: 30,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }

  Widget menuIdBasedPopularSideMenu() {
    print("menuid $mMenuId");
    return FutureBuilder(
        future: viewModel.getPopularSideMenuItemsApi(restaurantId, mMenuId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print("connectionState ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loaderWhite());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 25, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as MenuItemsModel;
              if (data.responseStatus == 1) {
                if (data.itemsList!.isNotEmpty) {
                  return Column(
                    children: [
                      Expanded(
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            controller: controller,
                            physics: PageScrollPhysics(),
                            itemCount: data.itemsList!.length,
                            itemBuilder: (BuildContext context, int i) {
                              return menuBasedSuggestedItem(data.itemsList![i]);
                            }),
                      ),
                      DotsIndicator(
                        dotsCount: data.itemsList!.length,
                        position: Provider.of<MainMenuViewModel>(context).page,
                        decorator: DotsDecorator(
                          color: Colors.white,
                          activeColor: def_color_red,
                          size: Size.square(9.0),
                          activeSize: Size(18.0, 9.0),
                          activeShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                        ),
                        onTap: (position) {
                          controller.animateToPage(
                            position.floor(),
                            curve: Curves.easeIn,
                            duration: Duration(seconds: 1),
                          );
                        },
                      )
                    ],
                  );
                } else {
                  return SizedBox();
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                  ),
                );
              }
            } else {
              return Center(
                child:
                    /*Text(
                  pleaseTryAgain,
                  style: TextStyle(fontSize: 35,
                      fontFamily: "Swis721",
                      color: Colors.black,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0.63),
                )*/
                    SizedBox(),
              );
            }
          } else {
            return Center(child: loaderWhite());
          }
        });
  }

  Widget menuBasedSuggestedItem(ItemsList menuItem) {
    return Container(
      width: ScreenSize.width(context) / 1.9,
      height: ScreenSize.height(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              width: ScreenSize.width(context) / 4,
              height: ScreenSize.height(context) / 4.5,
              margin: EdgeInsets.all(20),
              child: AspectRatio(
                child: Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Container(
                      color: Colors.white,
                      child: menuItem.itemImage == "" ? Image.network(restaurantLogo) : Image.network(menuItem.itemImage),
                    ),
                  ),
                ),
                aspectRatio: 1 / 1,
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: ScreenSize.height(context),
              width: ScreenSize.width(context),
              child: Center(
                child: Column(
                  children: [
                    Container(
                      child: Text(
                        menuItem.name,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 46, color: Colors.white, letterSpacing: 3.06, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                      ),
                      margin: EdgeInsets.fromLTRB(5, 5, 5, 10),
                    ),
                    Container(
                        margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: TextButton(
                              onPressed: () {
                                double price = 0.00;
                                if (menuItem.pricingStrategy == 4) {
                                  bool timeBased = false;
                                  menuItem.timePriceList.forEach((timePriceElement) {
                                    timePriceElement.days.forEach((daysElement) {
                                      if (currentDay == daysElement) {
                                        time = DateFormat('HH:mm').parse(currentTime);
                                        print(time);
                                        var startTime = DateFormat('HH:mm').parse(timePriceElement.timeFrom);
                                        var endTime = DateFormat('HH:mm').parse(timePriceElement.timeTo);
                                        if (time.isAfter(startTime) && time.isBefore(endTime)) {
                                          price = timePriceElement.price;
                                          timeBased = true;
                                        }
                                      }
                                    });
                                  });
                                  if (!timeBased) {
                                    price = menuItem.basePrice;
                                  }
                                } else {
                                  price = menuItem.basePrice;
                                }
                                print("asdasdad== $price ${menuItem.basePrice}");
                                if (menuItem.sizeList.isNotEmpty) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => SizeBasedItemsScreen(
                                            item: menuItem,
                                          )));
                                } else if (menuItem.suggestibleItems.isNotEmpty) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => SidesMenuScreen(
                                            item: menuItem,
                                          )));
                                } else if (menuItem.modifiersList.isNotEmpty) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => ModifiersMenuScreen(
                                            item: menuItem,
                                          )));
                                } else {
                                  double itemTax = 0.0;
                                  if (menuItem.taxIncludeOption) {
                                    if (menuItem.taxrates.isNotEmpty) {
                                      itemTax = getTaxes(menuItem.taxrates, menuItem.basePrice, 1);
                                    } else {
                                      itemTax = 0.00;
                                    }
                                  }
                                  print("tax---$itemTax");
                                  BotToast.showText(
                                    text: "${menuItem.name} $addedSuccessfully",
                                    align: Alignment(0, 0),
                                  );
                                  OrderItems item = OrderItems(
                                      menuId: menuItem.menuId,
                                      menuGroupId: menuItem.menuGroupId,
                                      itemId: menuItem.id,
                                      quantity: 1,
                                      itemName: menuItem.name,
                                      itemDescription: "",
                                      itemImage: menuItem.itemImage,
                                      unitPrice: menuItem.basePrice,
                                      totalPrice: menuItem.basePrice,
                                      itemTax: 0,
                                      discountAmount: 0,
                                      taxIncludeOption: menuItem.taxIncludeOption,
                                      taxrates: menuItem.taxrates,
                                      modifiersList: []);
                                  Provider.of<CartViewModel>(context, listen: false).addItemToCart(item);
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => SuggestionsScreen(
                                            item: menuItem,
                                          )));
                                }
                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                                child: Text(
                                  cancelOrder.toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 24, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.87, fontWeight: FontWeight.w700),
                                ),
                              ),
                            )))
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
