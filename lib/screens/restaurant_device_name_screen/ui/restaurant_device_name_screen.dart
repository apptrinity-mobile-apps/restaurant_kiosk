import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/basic_response.dart';
import 'package:kiosk/models/restaurant_login_model.dart';
import 'package:kiosk/screens/restaurant_login_screen/viewModel/restaurant_login_view_model.dart';
import 'package:kiosk/screens/restaurant_service_areas_screen/ui/restaurant_service_areas_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/snack_bar.dart';
import 'package:kiosk/viewModel/device_setup_view_model.dart';
import 'package:kiosk/widgets/bottom_sheet_keyboard.dart';
import 'package:provider/provider.dart';

class RestaurantDeviceScreen extends StatefulWidget {
  const RestaurantDeviceScreen({Key? key}) : super(key: key);

  @override
  _RestaurantDeviceScreenState createState() => _RestaurantDeviceScreenState();
}

class _RestaurantDeviceScreenState extends State<RestaurantDeviceScreen> {
  RestaurantLoginModel? _restaurantLoginModel;
  TextEditingController deviceNameTextController = TextEditingController();
  bool isUniqueName = false;
  late RestaurantLoginViewModel viewModel;

  @override
  void initState() {
    viewModel = Provider.of<RestaurantLoginViewModel>(context, listen: false);
    _restaurantLoginModel = viewModel.restaurantLoginModel;
    print("ppppp == ${viewModel.restaurantLoginModel!.restaurantId}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.black12,
      child: Stack(children: [
        Container(
          width: ScreenSize.width(context),
          height: ScreenSize.height(context),
          child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
        ),
        Positioned(
          child: Container(
            width: ScreenSize.width(context),
            height: ScreenSize.height(context),
            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                enterDeviceName,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 24,
                  fontFamily: "Swis721",
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
                  child: TextFormField(
                    onTap: () {
                      setState(() {
                        keyboard(context, deviceNameTextController, 'text');
                      });
                    },
                    controller: deviceNameTextController,
                    cursorColor: def_color_red,
                    decoration: InputDecoration(border: InputBorder.none, hintText: enterDeviceName, hintStyle: TextStyle(color: Colors.grey)),
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: 120,
                      margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.white),
                      child: TextButton(
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: Text(
                            back.toUpperCase(),
                            style: TextStyle(
                              fontSize: 21,
                              fontFamily: "Swis721",
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.53,
                              color: def_color_red,
                            ),
                          ),
                        ),
                      )),
                  Container(
                      width: 120,
                      margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.white),
                      child: TextButton(
                        onPressed: () async {
                          if (!isUniqueName) {
                            if (deviceNameTextController.text.trim() == "") {
                              showSnackBar(context, "Please enter device name.");
                            } else {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) {
                                    return loader();
                                  });
                              await Provider.of<DeviceSetupViewModel>(context, listen: false)
                                  .checkDeviceNameApi(_restaurantLoginModel!.restaurantId, deviceNameTextController.text.trim())
                                  .then((value) {
                                BasicResponse? _deviceNameResponse;
                                Navigator.of(context).pop();
                                if (Provider.of<DeviceSetupViewModel>(context, listen: false).deviceNameResponse != null) {
                                  setState(() {
                                    _deviceNameResponse = Provider.of<DeviceSetupViewModel>(context, listen: false).deviceNameResponse;
                                  });
                                }
                                if (_deviceNameResponse!.responseStatus == 1) {
                                  setState(() {
                                    isUniqueName = true;
                                  });
                                  showSnackBar(context, nameAvailable);
                                } else {
                                  showSnackBar(context, nameAlreadyUsed);
                                }
                              });
                            }
                          } else {
                            Provider.of<DeviceSetupViewModel>(context, listen: false).saveDeviceName(deviceNameTextController.text.trim());
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ServiceAreasScreen()));
                          }
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: Text(
                            isUniqueName ? next.toUpperCase() : submit.toUpperCase(),
                            style: TextStyle(
                              fontSize: 21,
                              fontFamily: "Swis721",
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.53,
                              color: def_color_red,
                            ),
                          ),
                        ),
                      )),
                ],
              )
            ]),
          ),
          top: ScreenSize.height(context) / 5,
          bottom: ScreenSize.height(context) / 5,
          right: ScreenSize.width(context) / 5,
          left: ScreenSize.width(context) / 5,
        )
      ]),
    ));
  }
}
