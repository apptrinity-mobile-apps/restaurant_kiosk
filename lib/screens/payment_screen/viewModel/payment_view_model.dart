import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/gift_card_model.dart';
import 'package:kiosk/services/repositories.dart';

class PaymentViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  GiftCardModel? _giftCardModel;

  GiftCardModel? get giftCardModel {
    return _giftCardModel;
  }

  Future<bool?> getGiftCardBalanceApi(String restaurantId, String cardNo) async {
    _isFetching = true;
    _isHavingData = false;
    _giftCardModel = GiftCardModel(result: '', responseStatus: 0, giftCard: GiftCard(amount: "", cardType: "", createdOn: "", email: "", name: ""));
    notifyListeners();
    try {
      dynamic response = await Repository().getGiftCardBalance(restaurantId, cardNo);
      print("response $response");
      if (response != null) {
        _giftCardModel = GiftCardModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_giftCardModel $_isFetching $_isHavingData $_giftCardModel");
    notifyListeners();
    return _isHavingData;
  }
}
