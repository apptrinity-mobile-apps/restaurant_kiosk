import 'package:bot_toast/bot_toast.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/menu_groups_model.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/screens/menu_item_detailed_screen/viewModel/menu_items_detailed_view_model.dart';
import 'package:kiosk/screens/modifiers_menu_screen/ui/modifiers_menu_screen.dart';
import 'package:kiosk/screens/sides_menu_screen/ui/sides_menu_screen.dart';
import 'package:kiosk/screens/size_based_screen/ui/size_based_items_screen.dart';
import 'package:kiosk/screens/suggestions_screen/ui/suggestions_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/math_calculations.dart';
import 'package:kiosk/utils/progress_loading_white.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:provider/provider.dart';

class MenuItemDetailedScreen extends StatefulWidget {
  final MenuGroupsList menuGroup;

  const MenuItemDetailedScreen({Key? key, required this.menuGroup}) : super(key: key);

  @override
  _MenuItemDetailedScreenState createState() => _MenuItemDetailedScreenState();
}

class _MenuItemDetailedScreenState extends State<MenuItemDetailedScreen> {
  late MenuGroupsList mMenuGroup;
  String restaurantLogo = "", restaurantId = "", menuGroupName = "", menuGroupImage = "", menuGroupId = "", currentDay = "", currentTime = "";
  late DateTime time;
  late MenuGroupItemsViewModel viewModel;
  List<OrderItems> cartList = [];
  double total = 0.00;
  int totalItems = 0;
  double currentIndexPage = 0.0;
  PageController controller = PageController(initialPage: 0);
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    mMenuGroup = widget.menuGroup;
    controller.addListener(() {
      Provider.of<MenuGroupItemsViewModel>(context, listen: false).pageSelected(controller.page!);
    });
    viewModel = Provider.of<MenuGroupItemsViewModel>(context, listen: false);
    setState(() {
      menuGroupName = mMenuGroup.name;
      menuGroupId = mMenuGroup.id;
      menuGroupImage = mMenuGroup.groupImage;
    });
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantLogo = value.restaurantLogo;
        restaurantId = value.restaurantId;
      });
    });
    DateTime today = DateTime.now();
    setState(() {
      currentDay = DateFormat('EEEE').format(today);
      currentTime = DateFormat('HH:mm').format(today);
    });
    cartList = Provider.of<CartViewModel>(context, listen: false).getCartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                alignment: Alignment.topLeft,
                width: ScreenSize.width(context) / 7,
                height: ScreenSize.height(context) / 3,
                child: Image.asset(
                  "assets/images/default_menu_left_top.png",
                ),
              ),
              top: 0,
              left: 0,
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 2.8,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.7),
                    borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                child: Column(
                  children: [
                    Expanded(
                        child: Container(
                      margin: EdgeInsets.fromLTRB(15, 15, 15, 5),
                      height: ScreenSize.height(context),
                      width: ScreenSize.width(context),
                      child: Card(
                        elevation: 3,
                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                        color: Colors.white,
                        child: menuItem(),
                      ),
                    )),
                    Expanded(
                        child: Container(
                            margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                            height: ScreenSize.height(context),
                            width: ScreenSize.width(context),
                            child: menuGroupsList(viewModel)))
                  ],
                ),
              ),
              right: 20,
              top: 40,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: Container(
                width: ScreenSize.width(context) / 1.9,
                height: ScreenSize.height(context),
                child: popularSideMenu(),
              ),
              top: 70,
              left: 30,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget menuItem() {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
          child: Container(
            padding: EdgeInsets.all(0),
            height: ScreenSize.height(context),
            width: ScreenSize.width(context),
            child: Center(
              child: Image.asset("assets/images/item_bg.png"),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(25), bottomLeft: Radius.circular(25)),
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              width: ScreenSize.width(context),
              child: Center(
                child: Text(
                  menuGroupName,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16, color: def_color_red, letterSpacing: 0, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 40,
          left: 0,
          right: 0,
          top: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
            child: Container(
              padding: EdgeInsets.all(20),
              width: ScreenSize.width(context),
              child: Center(
                child: Container(
                  child: menuGroupImage == "" ? Image.network(restaurantLogo) : Image.network(menuGroupImage),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget menuGroupsList(MenuGroupItemsViewModel viewModel) {
    print("menuGroupId $menuGroupId ===== $restaurantId");
    return FutureBuilder(
        future: viewModel.menuItemsBasedONGroupsApi(menuGroupId, restaurantId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print("connectionState ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loaderWhite());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as MenuItemsModel;
              if (data.responseStatus == 1) {
                if (data.itemsList!.isNotEmpty) {
                  return ListView.separated(
                    primary: true,
                    physics: BouncingScrollPhysics(),
                    itemCount: data.itemsList!.length,
                    itemBuilder: (context, i) {
                      return Container(
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), gradient: button_red_gradient),
                          child: ClipRRect(
                              borderRadius: BorderRadius.circular(25),
                              child: TextButton(
                                onPressed: () {
                                  double price = 0.00;
                                  if (data.itemsList![i].pricingStrategy == 4) {
                                    bool timeBased = false;
                                    data.itemsList![i].timePriceList.forEach((timePriceElement) {
                                      timePriceElement.days.forEach((daysElement) {
                                        if (currentDay == daysElement) {
                                          time = DateFormat('HH:mm').parse(currentTime);
                                          print(time);
                                          var startTime = DateFormat('HH:mm').parse(timePriceElement.timeFrom);
                                          var endTime = DateFormat('HH:mm').parse(timePriceElement.timeTo);
                                          if (time.isAfter(startTime) && time.isBefore(endTime)) {
                                            price = timePriceElement.price;
                                            timeBased = true;
                                          }
                                        }
                                      });
                                    });
                                    if (!timeBased) {
                                      price = data.itemsList![i].basePrice;
                                    }
                                  } else {
                                    price = data.itemsList![i].basePrice;
                                  }
                                  print("asdasdad== $price ${data.itemsList![i].basePrice}");
                                  data.itemsList![i].basePrice = price;
                                  if (data.itemsList![i].sizeList.isNotEmpty) {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => SizeBasedItemsScreen(
                                              item: data.itemsList![i],
                                            )));
                                  } else if (data.itemsList![i].suggestibleItems.isNotEmpty) {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => SidesMenuScreen(
                                              item: data.itemsList![i],
                                            )));
                                  } else if (data.itemsList![i].modifiersList.isNotEmpty) {
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => ModifiersMenuScreen(
                                              item: data.itemsList![i],
                                            )));
                                  } else {
                                    BotToast.showText(
                                      text: "${data.itemsList![i].name} $addedSuccessfully",
                                      align: Alignment(0, 0),
                                    );
                                    double itemTax = 0.0;
                                    if (data.itemsList![i].taxIncludeOption) {
                                      if (data.itemsList![i].taxrates.isNotEmpty) {
                                        itemTax = getTaxes(data.itemsList![i].taxrates, data.itemsList![i].basePrice, 1);
                                      } else {
                                        itemTax = 0.00;
                                      }
                                    }
                                    print("tax---$itemTax");
                                    OrderItems item = OrderItems(
                                        menuId: data.itemsList![i].menuId,
                                        menuGroupId: data.itemsList![i].menuGroupId,
                                        itemId: data.itemsList![i].id,
                                        quantity: 1,
                                        itemName: data.itemsList![i].name,
                                        itemDescription: "",
                                        itemImage: data.itemsList![i].itemImage,
                                        unitPrice: data.itemsList![i].basePrice,
                                        totalPrice: data.itemsList![i].basePrice,
                                        itemTax: 0,
                                        discountAmount: 0,
                                        taxIncludeOption: data.itemsList![i].taxIncludeOption,
                                        taxrates: data.itemsList![i].taxrates,
                                        modifiersList: []);
                                    Provider.of<CartViewModel>(context, listen: false).addItemToCart(item);
                                    Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => SuggestionsScreen(
                                              item: data.itemsList![i],
                                            )));
                                  }
                                },
                                child: Padding(
                                    padding: EdgeInsets.fromLTRB(5, 10, 5, 10),
                                    child: Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Expanded(
                                            child: Container(
                                          margin: EdgeInsets.only(left: 15, right: 10),
                                          child: Text(
                                            data.itemsList![i].name.toUpperCase(),
                                            style: TextStyle(
                                                fontSize: 20,
                                                color: Colors.white,
                                                letterSpacing: 0.6,
                                                fontFamily: "Swis721",
                                                fontWeight: FontWeight.w700),
                                          ),
                                        )),
                                      ],
                                    )),
                              )));
                    },
                    separatorBuilder: (context, i) {
                      return Divider(
                        color: Colors.transparent,
                        height: 30,
                      );
                    },
                  );
                } else {
                  return Center(
                    child: Text(
                      noDataAvailable,
                      style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                    ),
                  );
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                  ),
                );
              }
            } else {
              return Center(
                child: Text(
                  pleaseTryAgain,
                  style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            }
          } else {
            return Center(child: loaderWhite());
          }
        });
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }

  Widget popularSideMenu() {
    return FutureBuilder(
        future: viewModel.getPopularMenuItemsFromMenuGroupApi(restaurantId, menuGroupId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print("connectionState ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loaderWhite());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as MenuItemsModel;
              if (data.responseStatus == 1) {
                if (data.itemsList!.isNotEmpty) {
                  return Column(
                    children: [
                      Expanded(
                        child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            controller: controller,
                            physics: PageScrollPhysics(),
                            itemCount: data.itemsList!.length,
                            itemBuilder: (BuildContext context, int i) {
                              return suggestItem(data.itemsList![i]);
                            }),
                      ),
                      DotsIndicator(
                        dotsCount: data.itemsList!.length,
                        position: Provider.of<MenuGroupItemsViewModel>(context).page,
                        decorator: DotsDecorator(
                          color: Colors.white,
                          activeColor: def_color_red,
                          size: Size.square(9.0),
                          activeSize: Size(18.0, 9.0),
                          activeShape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                        ),
                        onTap: (position) {
                          controller.animateToPage(
                            position.floor(),
                            curve: Curves.easeIn,
                            duration: Duration(seconds: 1),
                          );
                        },
                      )
                    ],
                  );
                } else {
                  return SizedBox();
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                  ),
                );
              }
            } else {
              return Center(
                child:
                    /*Text(
                  pleaseTryAgain,
                  style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                )*/
                    SizedBox(),
              );
            }
          } else {
            return Center(child: loaderWhite());
          }
        });
  }

  Widget suggestItem(ItemsList menuItem) {
    return Container(
      width: ScreenSize.width(context) / 1.9,
      height: ScreenSize.height(context),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              width: ScreenSize.width(context) / 4,
              height: ScreenSize.height(context) / 4.5,
              margin: EdgeInsets.all(20),
              child: AspectRatio(
                child: Card(
                  elevation: 10,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(25),
                    child: Container(
                      color: Colors.white,
                      child: menuItem.itemImage == "" ? Image.network(restaurantLogo) : Image.network(menuItem.itemImage),
                    ),
                  ),
                ),
                aspectRatio: 1 / 1,
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: ScreenSize.height(context),
              width: ScreenSize.width(context),
              child: Center(
                child: Column(
                  children: [
                    Container(
                      child: Text(
                        menuItem.name,
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                        softWrap: true,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 46, color: Colors.white, letterSpacing: 3.06, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                      ),
                      margin: EdgeInsets.fromLTRB(5, 5, 5, 10),
                    ),
                    Container(
                        margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: TextButton(
                              onPressed: () {
                                double price = 0.00;
                                if (menuItem.pricingStrategy == 4) {
                                  bool timeBased = false;
                                  menuItem.timePriceList.forEach((timePriceElement) {
                                    timePriceElement.days.forEach((daysElement) {
                                      if (currentDay == daysElement) {
                                        time = DateFormat('HH:mm').parse(currentTime);
                                        print(time);
                                        var startTime = DateFormat('HH:mm').parse(timePriceElement.timeFrom);
                                        var endTime = DateFormat('HH:mm').parse(timePriceElement.timeTo);
                                        if (time.isAfter(startTime) && time.isBefore(endTime)) {
                                          price = timePriceElement.price;
                                          timeBased = true;
                                        }
                                      }
                                    });
                                  });
                                  if (!timeBased) {
                                    price = menuItem.basePrice;
                                  }
                                } else {
                                  price = menuItem.basePrice;
                                }
                                print("asdasdad== $price ${menuItem.basePrice}");
                                if (menuItem.sizeList.isNotEmpty) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => SizeBasedItemsScreen(
                                            item: menuItem,
                                          )));
                                } else if (menuItem.suggestibleItems.isNotEmpty) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => SidesMenuScreen(
                                            item: menuItem,
                                          )));
                                } else if (menuItem.modifiersList.isNotEmpty) {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => ModifiersMenuScreen(
                                            item: menuItem,
                                          )));
                                } else {
                                  BotToast.showText(
                                    text: "${menuItem.name} $addedSuccessfully",
                                    align: Alignment(0, 0),
                                  );
                                  double itemTax = 0.0;
                                  if (menuItem.taxIncludeOption) {
                                    if (menuItem.taxrates.isNotEmpty) {
                                      itemTax = getTaxes(menuItem.taxrates, menuItem.basePrice, 1);
                                    } else {
                                      itemTax = 0.00;
                                    }
                                  }
                                  print("tax---$itemTax");
                                  OrderItems item = OrderItems(
                                      menuId: menuItem.menuId,
                                      menuGroupId: menuItem.menuGroupId,
                                      itemId: menuItem.id,
                                      quantity: 1,
                                      itemName: menuItem.name,
                                      itemDescription: "",
                                      itemImage: menuItem.itemImage,
                                      unitPrice: menuItem.basePrice,
                                      totalPrice: menuItem.basePrice,
                                      itemTax: 0,
                                      discountAmount: 0,
                                      taxIncludeOption: menuItem.taxIncludeOption,
                                      taxrates: menuItem.taxrates,
                                      modifiersList: []);
                                  Provider.of<CartViewModel>(context, listen: false).addItemToCart(item);
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => SuggestionsScreen(
                                            item: menuItem,
                                          )));
                                }
                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
                                child: Text(
                                  cancelOrder.toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 24, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.87, fontWeight: FontWeight.w700),
                                ),
                              ),
                            )))
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
