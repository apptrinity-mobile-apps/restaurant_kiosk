import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/services/repositories.dart';

class MenuGroupItemsViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  MenuItemsModel? _menuItemsModel, _popularItemsViewModel;
  double selectedPage = 0.0;

  void pageSelected(double page) {
    selectedPage = page;
    notifyListeners();
  }

  double get page {
    return selectedPage;
  }

  Future<MenuItemsModel?> menuItemsBasedONGroupsApi(String menuGroupId, String restaurantId) async {
    _isFetching = true;
    _isHavingData = false;
    _menuItemsModel = MenuItemsModel(result: '', responseStatus: 0, itemsList: []);
    notifyListeners();
    try {
      dynamic response = await Repository().menuItemsBasedOnGroups(menuGroupId, restaurantId);
      if (response != null) {
        _menuItemsModel = MenuItemsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_menuItemsModel $_isFetching $_isHavingData $_menuItemsModel");
    notifyListeners();
    return _menuItemsModel;
  }

  Future<MenuItemsModel?> getPopularMenuItemsFromMenuGroupApi(String restaurantId, String menuGroupId) async {
    _isFetching = true;
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response = await Repository().getPopularMenuItemsFromMenuGroup(restaurantId, menuGroupId);
      if (response != null) {
        _popularItemsViewModel = MenuItemsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("getPopularSideMenuItems $_isFetching $_isHavingData $_popularItemsViewModel");
    notifyListeners();
    return _popularItemsViewModel;
  }
}
