import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/models/suggestion_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/screens/modifiers_menu_screen/ui/modifiers_menu_screen.dart';
import 'package:kiosk/screens/sides_menu_screen/ui/sides_menu_screen.dart';
import 'package:kiosk/screens/size_based_screen/ui/size_based_items_screen.dart';
import 'package:kiosk/screens/suggestions_screen/viewModel/suggestions_view_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/math_calculations.dart';
import 'package:kiosk/utils/progress_loading_white.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:kiosk/widgets/reset_cart_dialog.dart';
import 'package:provider/provider.dart';

class SuggestionsScreen extends StatefulWidget {
  final ItemsList item;

  const SuggestionsScreen({Key? key, required this.item}) : super(key: key);

  @override
  _SuggestionsScreenState createState() => _SuggestionsScreenState();
}

class _SuggestionsScreenState extends State<SuggestionsScreen> {
  List<OrderItems> cartList = [];
  double total = 0.00;
  int totalItems = 0;
  late SuggestionsViewModel viewModel;
  List<OrderItems> itemsOfSameGroup = [];
  String mMenuGroupId = "", restaurantId = "", restaurantLogo = "", currentDay = "", currentTime = "";
  late DateTime time;
  SuggestionModel suggestionModel = SuggestionModel(restaurantId: "", menuGroupId: "", itemIdsList: []);
  List<String> itemIdsList = [];
  late ItemsList mItem;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    mItem = widget.item;
    mMenuGroupId = mItem.menuGroupId;
    viewModel = Provider.of<SuggestionsViewModel>(context, listen: false);
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantId = value.restaurantId;
        restaurantLogo = value.restaurantLogo;
      });
    });
    DateTime today = DateTime.now();
    setState(() {
      currentDay = DateFormat('EEEE').format(today);
      currentTime = DateFormat('HH:mm').format(today);
    });
    print("date---" + DateFormat('EEEE').format(today) + "----" + DateFormat('HH:mm').format(today));
    cartList = Provider.of<CartViewModel>(context, listen: false).getCartList;
    cartList.forEach((element) {
      if (mMenuGroupId == element.menuGroupId) {
        itemsOfSameGroup.add(element);
      }
    });
    itemsOfSameGroup.forEach((element) {
      itemIdsList.add(element.itemId);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                        child: Container(
                      child: suggestedItems(viewModel),
                    )),
                    _yourOrderText()
                  ],
                ),
              ),
              right: 20,
              left: 20,
              top: 20,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget suggestionImageBottom(ItemsList item) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      width: ScreenSize.width(context) / 3.1,
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(25),
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context),
                child: Image.asset(
                  "assets/images/suggestions_bg.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Container(
                    height: ScreenSize.height(context) / 8.5,
                    width: ScreenSize.width(context),
                    child: Image.asset(
                      "assets/images/default_bottom.png",
                      fit: BoxFit.fill,
                    ),
                  )),
              bottom: 0,
              left: 0,
              right: 0,
            ),
            Positioned(
              child: Column(
                children: [
                  Expanded(
                      child: Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                add,
                                maxLines: 1,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 32, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w300),
                              ),
                              Text(
                                item.name.toUpperCase(),
                                maxLines: 3,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 38, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                              )
                            ],
                          ))),
                  Expanded(
                      child: Container(
                    padding: EdgeInsets.all(10),
                    child: Center(
                      child: item.itemImage == "" ? Image.network(restaurantLogo) : Image.network(item.itemImage),
                    ),
                  )),
                ],
              ),
              top: 0,
              right: 0,
              left: 0,
              bottom: ScreenSize.height(context) / 8.5,
            )
          ],
        ),
      ),
    );
  }

  Widget suggestionImageCenter(ItemsList item) {
    return Container(
      margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
      width: ScreenSize.width(context) / 3.1,
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(25),
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context),
                child: Image.asset(
                  "assets/images/suggestions_bg.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Container(
                    height: ScreenSize.height(context) / 8.5,
                    width: ScreenSize.width(context),
                    child: Image.asset(
                      "assets/images/default_top.png",
                      fit: BoxFit.fill,
                    ),
                  )),
              top: 0,
              left: 0,
              right: 0,
            ),
            Positioned(
              child: Column(
                children: [
                  Expanded(
                      child: Container(
                    padding: EdgeInsets.all(10),
                    child: Center(
                      child: item.itemImage == "" ? Image.network(restaurantLogo) : Image.network(item.itemImage),
                    ),
                  )),
                  Expanded(
                      child: Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                add,
                                maxLines: 1,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 32, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w300),
                              ),
                              Text(
                                item.name.toUpperCase(),
                                maxLines: 3,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 38, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                              )
                            ],
                          ))),
                ],
              ),
              top: ScreenSize.height(context) / 8.5,
              right: 0,
              left: 0,
              bottom: 0,
            )
          ],
        ),
      ),
    );
  }

  Widget suggestion3() {
    return Container(
      margin: EdgeInsets.all(10),
      height: ScreenSize.height(context),
      child: Card(
        elevation: 5,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(25),
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context),
                child: Image.asset(
                  "assets/images/suggestions_bg.png",
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Positioned(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Container(
                    height: ScreenSize.height(context) / 8.5,
                    width: ScreenSize.width(context),
                    child: Image.asset(
                      "assets/images/default_bottom.png",
                      fit: BoxFit.fill,
                    ),
                  )),
              bottom: 0,
              left: 0,
              right: 0,
            ),
            Positioned(
              child: Column(
                children: [
                  Expanded(
                      child: Container(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                add,
                                maxLines: 1,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 32, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w300),
                              ),
                              Text(
                                defItemName.toUpperCase(),
                                maxLines: 3,
                                textAlign: TextAlign.center,
                                style: TextStyle(fontSize: 38, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                              )
                            ],
                          ))),
                  Expanded(
                      child: Container(
                    padding: EdgeInsets.all(10),
                    child: Center(
                      child: Image.asset(
                        "assets/images/restaurant_logo.png",
                        fit: BoxFit.fill,
                      ),
                    ),
                  )),
                ],
              ),
              top: 0,
              right: 0,
              left: 0,
              bottom: ScreenSize.height(context) / 8.5,
            )
          ],
        ),
      ),
    );
  }

  Widget _yourOrderText() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
        Expanded(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.centerLeft,
              child: RichText(
                textScaleFactor: 1.5,
                text: TextSpan(
                  text: '',
                  children: <TextSpan>[
                    TextSpan(
                      text: addThese,
                      style: TextStyle(fontSize: 26, color: Colors.white, fontFamily: "Palace", fontWeight: FontWeight.w700, letterSpacing: 0.93),
                    ),
                    TextSpan(
                      text: toYourOrder.toUpperCase(),
                      style: TextStyle(fontSize: 26, color: Colors.white, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
            ),
            Image(
              image: AssetImage(
                "assets/images/suggestions_arrow.png",
              ),
              height: 50,
              color: Colors.white,
              fit: BoxFit.fill,
            ),
          ],
        )),
        noThanksButton(),
      ]),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }

  Widget noThanksButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                child: Text(
                  noThanks.toUpperCase(),
                  style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                ),
              ),
            )));
  }

  Widget suggestedItems(SuggestionsViewModel viewModel) {
    suggestionModel = SuggestionModel(restaurantId: restaurantId, menuGroupId: mMenuGroupId, itemIdsList: itemIdsList);
    print("suggestionModel ${suggestionModel.restaurantId}${suggestionModel.menuGroupId}${suggestionModel.itemIdsList!.length}");
    return FutureBuilder(
        future: viewModel.getSuggestionsApi(suggestionModel),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print("connectionState ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loaderWhite());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as MenuItemsModel;
              if (data.responseStatus == 1) {
                if (data.itemsList!.isNotEmpty) {
                  return ListView.separated(
                      separatorBuilder: (BuildContext context, int index) {
                        return VerticalDivider(
                          width: 0.5,
                          color: Colors.transparent,
                        );
                      },
                      controller: PageController(viewportFraction: 1 / 3),
                      physics: BouncingScrollPhysics(),
                      scrollDirection: Axis.horizontal,
                      itemCount: data.itemsList!.length,
                      itemBuilder: (BuildContext context, int i) {
                        if (i % 2 == 0) {
                          return InkWell(
                            child: suggestionImageBottom(data.itemsList![i]),
                            onTap: () {
                              double price = 0.00;
                              if (data.itemsList![i].pricingStrategy == 4) {
                                bool timeBased = false;
                                data.itemsList![i].timePriceList.forEach((timePriceElement) {
                                  timePriceElement.days.forEach((daysElement) {
                                    if (currentDay == daysElement) {
                                      time = DateFormat('HH:mm').parse(currentTime);
                                      print(time);
                                      var startTime = DateFormat('HH:mm').parse(timePriceElement.timeFrom);
                                      var endTime = DateFormat('HH:mm').parse(timePriceElement.timeTo);
                                      if (time.isAfter(startTime) && time.isBefore(endTime)) {
                                        price = timePriceElement.price;
                                        timeBased = true;
                                      }
                                    }
                                  });
                                });
                                if (!timeBased) {
                                  price = data.itemsList![i].basePrice;
                                }
                              } else {
                                price = data.itemsList![i].basePrice;
                              }
                              print("asdasdad== $price ${data.itemsList![i].basePrice}");
                              if (data.itemsList![i].sizeList.isNotEmpty) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SizeBasedItemsScreen(
                                          item: data.itemsList![i],
                                        )));
                              } else if (data.itemsList![i].suggestibleItems.isNotEmpty) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SidesMenuScreen(
                                          item: data.itemsList![i],
                                        )));
                              } else if (data.itemsList![i].modifiersList.isNotEmpty) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => ModifiersMenuScreen(
                                          item: data.itemsList![i],
                                        )));
                              } else {
                                BotToast.showText(
                                  text: "${data.itemsList![i].name} $addedSuccessfully",
                                  align: Alignment(0, 0),
                                );
                                double itemTax = 0.0;
                                if (data.itemsList![i].taxIncludeOption) {
                                  if (data.itemsList![i].taxrates.isNotEmpty) {
                                    itemTax = getTaxes(data.itemsList![i].taxrates, data.itemsList![i].basePrice, 1);
                                  } else {
                                    itemTax = 0.00;
                                  }
                                }
                                print("tax==$itemTax");
                                OrderItems item = OrderItems(
                                    menuId: data.itemsList![i].menuId,
                                    menuGroupId: data.itemsList![i].menuGroupId,
                                    itemId: data.itemsList![i].id,
                                    quantity: 1,
                                    itemName: data.itemsList![i].name,
                                    itemDescription: "",
                                    itemImage: data.itemsList![i].itemImage,
                                    unitPrice: data.itemsList![i].basePrice,
                                    totalPrice: data.itemsList![i].basePrice,
                                    itemTax: 0,
                                    discountAmount: 0,
                                    taxIncludeOption: data.itemsList![i].taxIncludeOption,
                                    taxrates: data.itemsList![i].taxrates,
                                    modifiersList: []);
                                Provider.of<CartViewModel>(context, listen: false).addItemToCart(item);
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SuggestionsScreen(
                                          item: data.itemsList![i],
                                        )));
                              }
                            },
                          );
                        } else {
                          return InkWell(
                            child: suggestionImageCenter(data.itemsList![i]),
                            onTap: () {
                              double price = 0.00;
                              if (data.itemsList![i].pricingStrategy == 4) {
                                bool timeBased = false;
                                data.itemsList![i].timePriceList.forEach((timePriceElement) {
                                  timePriceElement.days.forEach((daysElement) {
                                    if (currentDay == daysElement) {
                                      time = DateFormat('HH:mm').parse(currentTime);
                                      print(time);
                                      var startTime = DateFormat('HH:mm').parse(timePriceElement.timeFrom);
                                      var endTime = DateFormat('HH:mm').parse(timePriceElement.timeTo);
                                      if (time.isAfter(startTime) && time.isBefore(endTime)) {
                                        price = timePriceElement.price;
                                        timeBased = true;
                                      }
                                    }
                                  });
                                });
                                if (!timeBased) {
                                  price = data.itemsList![i].basePrice;
                                }
                              } else {
                                price = data.itemsList![i].basePrice;
                              }
                              print("asdasdad== $price ${data.itemsList![i].basePrice}");
                              if (data.itemsList![i].sizeList.isNotEmpty) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SizeBasedItemsScreen(
                                          item: data.itemsList![i],
                                        )));
                              } else if (data.itemsList![i].suggestibleItems.isNotEmpty) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SidesMenuScreen(
                                          item: data.itemsList![i],
                                        )));
                              } else if (data.itemsList![i].modifiersList.isNotEmpty) {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => ModifiersMenuScreen(
                                          item: data.itemsList![i],
                                        )));
                              } else {
                                BotToast.showText(
                                  text: "${data.itemsList![i].name} $addedSuccessfully",
                                  align: Alignment(0, 0),
                                );
                                double itemTax = 0.0;
                                if (data.itemsList![i].taxIncludeOption) {
                                  if (data.itemsList![i].taxrates.isNotEmpty) {
                                    itemTax = getTaxes(data.itemsList![i].taxrates, data.itemsList![i].basePrice, 1);
                                  } else {
                                    itemTax = 0.00;
                                  }
                                }
                                print("tax==$itemTax");
                                OrderItems item = OrderItems(
                                    menuId: data.itemsList![i].menuId,
                                    menuGroupId: data.itemsList![i].menuGroupId,
                                    itemId: data.itemsList![i].id,
                                    quantity: 1,
                                    itemName: data.itemsList![i].name,
                                    itemDescription: "",
                                    itemImage: data.itemsList![i].itemImage,
                                    unitPrice: data.itemsList![i].basePrice,
                                    totalPrice: data.itemsList![i].basePrice,
                                    itemTax: 0,
                                    discountAmount: 0,
                                    taxIncludeOption: data.itemsList![i].taxIncludeOption,
                                    taxrates: data.itemsList![i].taxrates,
                                    modifiersList: []);
                                Provider.of<CartViewModel>(context, listen: false).addItemToCart(item);
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SuggestionsScreen(
                                          item: data.itemsList![i],
                                        )));
                              }
                            },
                          );
                        }
                      });
                } else {
                  Future.delayed(Duration(milliseconds: 50), () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
                  });
                  return Center(
                    child: SizedBox(),
                  );
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                  ),
                );
              }
            } else {
              return Center(
                child: Text(
                  pleaseTryAgain,
                  style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            }
          } else {
            return Center(child: loaderWhite());
          }
        });
  }
}
