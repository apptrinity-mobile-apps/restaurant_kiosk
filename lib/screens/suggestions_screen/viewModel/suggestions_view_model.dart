import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/models/suggestion_model.dart';
import 'package:kiosk/services/repositories.dart';

class SuggestionsViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  MenuItemsModel? _menuItemsModel;

  Future<MenuItemsModel?> getSuggestionsApi(SuggestionModel suggestionModel) async {
    _isFetching = true;
    _isHavingData = false;
    _menuItemsModel = MenuItemsModel(result: '', responseStatus: 0, itemsList: []);
    notifyListeners();
    try {
      dynamic response = await Repository().getSuggestedItems(suggestionModel);
      if (response != null) {
        _menuItemsModel = MenuItemsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("menuItemsModel $_isFetching $_isHavingData $_menuItemsModel");
    notifyListeners();
    return _menuItemsModel;
  }
}
