import 'package:flutter/material.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/services/repositories.dart';

class PopularItemsViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  MenuItemsModel? _itemsViewModel;

  MenuItemsModel? get popularItemsViewModel {
    return _itemsViewModel;
  }

  Future<MenuItemsModel?> popularItemsApi(String restaurantId) async {
    _isFetching = true;
    _isHavingData = false;
    _itemsViewModel = MenuItemsModel(result: '', responseStatus: 0, itemsList: []);
    notifyListeners();
    try {
      dynamic response = await Repository().getPopularItemList(restaurantId);
      if (response != null) {
        _itemsViewModel = MenuItemsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("newlyAddedItemsModel $_isFetching $_isHavingData $_itemsViewModel");
    notifyListeners();
    return _itemsViewModel;
  }
}
