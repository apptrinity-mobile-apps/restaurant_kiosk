import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/widgets/title_header.dart';

class OffersBannerScreen extends StatefulWidget {
  const OffersBannerScreen({Key? key}) : super(key: key);

  @override
  _OffersBannerScreenState createState() => _OffersBannerScreenState();
}

class _OffersBannerScreenState extends State<OffersBannerScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () {
          // Navigator.of(context).push(MaterialPageRoute(builder: (context) => MainMenuScreen()));
        },
        child: Container(
          color: Colors.black12,
          child: Stack(
            children: [
              Container(
                width: ScreenSize.width(context),
                height: ScreenSize.height(context),
                child: Image.asset("assets/images/default_bg.png", fit: BoxFit.fill),
              ),
              Positioned(
                child: titleHeaderWidget(""),
                top: 20,
                left: 20,
                right: 20,
              ),
              Positioned(
                child: Container(
                  margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                  height: ScreenSize.height(context),
                  width: ScreenSize.width(context),
                  child: CarouselSlider.builder(
                    itemCount: 3,
                    itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) => Card(
                      color: def_color_red,
                      elevation: 15,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                      child: Container(
                          width: ScreenSize.width(context),
                          height: ScreenSize.height(context),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: Stack(
                              children: [
                                Positioned(
                                  child: Container(
                                    alignment: Alignment.centerRight,
                                    width: ScreenSize.width(context),
                                    height: ScreenSize.width(context) / 8,
                                    child: Image.asset(
                                      "assets/images/default_bottom_popular_items.png",
                                    ),
                                  ),
                                  bottom: 0,
                                  right: 0,
                                ),
                                Positioned(
                                  child: Container(
                                    margin: EdgeInsets.only(top: 10, bottom: 10),
                                    width: ScreenSize.width(context) / 2,
                                    height: ScreenSize.width(context),
                                    child: Image.asset(
                                      "assets/images/offers_item.png",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  bottom: 0,
                                  top: 0,
                                ),
                                Positioned(
                                  child: Container(
                                    margin: EdgeInsets.all(15),
                                    padding: EdgeInsets.all(15),
                                    width: ScreenSize.width(context) / 2,
                                    height: ScreenSize.width(context),
                                    child: Align(
                                      alignment: Alignment.topCenter,
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(50, 50, 10, 0),
                                            child: Text(
                                              "4pc Cajun Tender Meal Deal",
                                              maxLines: 3,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(fontSize: 38, color: Colors.white, fontFamily: "Poppins", fontWeight: FontWeight.w700),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.fromLTRB(50, 50, 10, 10),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                  "\u0024",
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      fontSize: 30, color: Colors.white, fontFamily: "Poppins", fontWeight: FontWeight.w700),
                                                ),
                                                Text(
                                                  "9",
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      fontSize: 48, color: Colors.white, fontFamily: "Poppins", fontWeight: FontWeight.w800),
                                                ),
                                                Text(
                                                  "99",
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      fontSize: 30, color: Colors.white, fontFamily: "Poppins", fontWeight: FontWeight.w800),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  right: 5,
                                  bottom: ScreenSize.width(context) / 8,
                                  top: 0,
                                ),
                              ],
                            ),
                          )),
                    ),
                    options: CarouselOptions(height: ScreenSize.height(context), viewportFraction: 1.0, autoPlay: true, enlargeCenterPage: true),
                  ),
                ),
                top: 130,
                bottom: 30,
                left: 30,
                right: 30,
              )
            ],
          ),
        ),
      ),
    );
  }
}
