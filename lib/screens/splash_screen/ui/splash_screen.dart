import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/screens/landing_banner_screen/ui/landing_banners_screen.dart';
import 'package:kiosk/screens/restaurant_login_screen/ui/restaurant_login_screen.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen() : super();

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    sessionManager.isLoggedIn().then((value) {
      setState(() {
        value!
            ? Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LandingBannerScreen()))
            : Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => RestaurantLoginScreen()));
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: Colors.black12,
            child: Stack(children: [
              Container(
                width: ScreenSize.width(context),
                height: ScreenSize.height(context),
                child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
              ),
            ])));
  }
}
