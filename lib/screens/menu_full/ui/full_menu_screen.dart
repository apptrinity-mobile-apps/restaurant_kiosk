import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/menu_groups_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/screens/menu_full/viewModel/full_menu_view_model.dart';
import 'package:kiosk/screens/menu_item_detailed_screen/ui/menu_item_detailed_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading_white.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:provider/provider.dart';

class FullMenuScreen extends StatefulWidget {
  final String menuId;

  const FullMenuScreen({Key? key, required this.menuId}) : super(key: key);

  @override
  _FullMenuScreenState createState() => _FullMenuScreenState();
}

class _FullMenuScreenState extends State<FullMenuScreen> {
  String mMenuId = "", restaurantLogo = "";
  late FullMenuViewModel viewModel;
  List<OrderItems> cartList = [];
  double total = 0.00;
  int totalItems = 0;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    mMenuId = widget.menuId;
    viewModel = Provider.of<FullMenuViewModel>(context, listen: false);
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantLogo = value.restaurantLogo;
      });
    });
    cartList = Provider.of<CartViewModel>(context, listen: false).getCartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(child: menuGroupsList(viewModel), top: 20, right: 20, bottom: ScreenSize.height(context) / 8, left: 20),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget menuGroupsList(FullMenuViewModel viewModel) {
    print("id--- $mMenuId");
    return FutureBuilder(
        future: viewModel.menuGroupsApi(mMenuId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print("connectionState ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loaderWhite());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 25, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as MenuGroupsModel;
              if (data.responseStatus == 1) {
                if (data.menuGroupsList!.isNotEmpty) {
                  return GridView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: data.menuGroupsList!.length,
                      gridDelegate:
                          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4, childAspectRatio: 1, mainAxisSpacing: 8, crossAxisSpacing: 8),
                      itemBuilder: (BuildContext context, int index) {
                        return InkWell(
                          onTap: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) => MenuItemDetailedScreen(menuGroup: data.menuGroupsList![index])));
                          },
                          child: Container(
                            height: ScreenSize.height(context),
                            width: ScreenSize.width(context),
                            child: Card(
                              elevation: 3,
                              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                              color: Colors.white,
                              child: menuItem(data.menuGroupsList![index]),
                            ),
                          ),
                        );
                      });
                } else {
                  return Center(
                    child: Text(
                      noDataAvailable,
                      style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                    ),
                  );
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                  ),
                );
              }
            } else {
              return Center(
                child: Text(
                  pleaseTryAgain,
                  style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.white, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            }
          } else {
            return Center(child: loaderWhite());
          }
        });
  }

  Widget menuItem(MenuGroupsList listItem) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
          child: Container(
            padding: EdgeInsets.all(0),
            height: ScreenSize.height(context),
            width: ScreenSize.width(context),
            child: Center(
              child: Image.asset("assets/images/item_bg.png"),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(25), bottomLeft: Radius.circular(25)),
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              width: ScreenSize.width(context),
              child: Center(
                child: Text(
                  listItem.name,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16, color: def_color_red, letterSpacing: 0, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 40,
          left: 0,
          right: 0,
          top: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
            child: Container(
              padding: EdgeInsets.all(20),
              width: ScreenSize.width(context),
              child: Center(
                child: Container(
                  child: listItem.groupImage == ""
                      ? Image.network(
                          restaurantLogo,
                          fit: BoxFit.fill,
                        )
                      : Image.network(
                          listItem.groupImage,
                          fit: BoxFit.fill,
                        ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }
}
