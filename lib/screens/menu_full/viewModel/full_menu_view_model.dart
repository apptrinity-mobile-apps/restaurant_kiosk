import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/menu_groups_model.dart';
import 'package:kiosk/services/repositories.dart';

class FullMenuViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  MenuGroupsModel? _menuGroupsModel;

  Future<MenuGroupsModel?> menuGroupsApi(String menuId) async {
    _isFetching = true;
    _isHavingData = false;
    _menuGroupsModel = MenuGroupsModel(result: '', responseStatus: 0, menuGroupsList: []);
    notifyListeners();
    try {
      dynamic response = await Repository().menuGroups(menuId);
      if (response != null) {
        _menuGroupsModel = MenuGroupsModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_menuGroupsModel $_isFetching $_isHavingData $_menuGroupsModel");
    notifyListeners();
    return _menuGroupsModel;
  }
}
