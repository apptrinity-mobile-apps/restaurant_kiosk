import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/screens/restaurant_device_name_screen/ui/restaurant_device_name_screen.dart';
import 'package:kiosk/screens/restaurant_login_screen/viewModel/restaurant_login_view_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/models/restaurant_login_model.dart';
import 'package:kiosk/utils/snack_bar.dart';
import 'package:kiosk/widgets/bottom_sheet_keyboard.dart';
import 'package:provider/provider.dart';

class RestaurantOtpScreen extends StatefulWidget {
  RestaurantOtpScreen({Key? key}) : super(key: key);

  @override
  _RestaurantOtpScreenState createState() => _RestaurantOtpScreenState();
}

class _RestaurantOtpScreenState extends State<RestaurantOtpScreen> {
  RestaurantLoginModel? _restaurantLoginModel;
  TextEditingController otpTextController = TextEditingController();
  late RestaurantLoginViewModel viewModel;

  @override
  void initState() {
    viewModel = Provider.of<RestaurantLoginViewModel>(context, listen: false);
    _restaurantLoginModel = viewModel.restaurantLoginModel;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.black12,
      child: Stack(children: [
        Container(
          width: ScreenSize.width(context),
          height: ScreenSize.height(context),
          child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
        ),
        Positioned(
          child: Container(
            width: ScreenSize.width(context),
            height: ScreenSize.height(context),
            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                enterOtp,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 24,
                  fontFamily: "Swis721",
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
                  child: TextFormField(
                    onTap: () {
                      setState(() {
                        keyboard(context, otpTextController, 'num');
                      });
                    },
                    controller: otpTextController,
                    cursorColor: def_color_red,
                    decoration: InputDecoration(border: InputBorder.none, hintText: enterOtp, hintStyle: TextStyle(color: Colors.grey)),
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                      width: 120,
                      margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.white),
                      child: TextButton(
                        onPressed: () async {
                          Navigator.pop(context);
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: Text(
                            back.toUpperCase(),
                            style: TextStyle(
                              fontSize: 21,
                              fontFamily: "Swis721",
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.53,
                              color: def_color_red,
                            ),
                          ),
                        ),
                      )),
                  Container(
                      width: 120,
                      margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.white),
                      child: TextButton(
                        onPressed: () async {
                          if (otpTextController.text.isNotEmpty) {
                            if (otpTextController.text.trim() == _restaurantLoginModel!.OTP) {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => RestaurantDeviceScreen()));
                            } else {
                              showSnackBar(context, invalidOtp);
                            }
                          } else {
                            showSnackBar(context, "Enter OTP");
                          }
                        },
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          child: Text(
                            next.toUpperCase(),
                            style: TextStyle(
                              fontSize: 21,
                              fontFamily: "Swis721",
                              fontWeight: FontWeight.w700,
                              letterSpacing: 0.53,
                              color: def_color_red,
                            ),
                          ),
                        ),
                      )),
                ],
              )
            ]),
          ),
          top: ScreenSize.height(context) / 5,
          bottom: ScreenSize.height(context) / 5,
          right: ScreenSize.width(context) / 5,
          left: ScreenSize.width(context) / 5,
        )
      ]),
    ));
  }
}
