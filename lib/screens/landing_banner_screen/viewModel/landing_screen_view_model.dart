import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/home_banner_model.dart';
import 'package:kiosk/services/repositories.dart';

class LandingScreenViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  HomeBanner? _homeBanner;

  HomeBanner? get homeBanner {
    return _homeBanner;
  }

  Future<HomeBanner?> homeBannerApi(String restaurantCode) async {
    _isFetching = true;
    _isHavingData = false;
    _homeBanner = HomeBanner(responseStatus: 0, result: "", homeBannersList: null);
    notifyListeners();
    try {
      dynamic response = await Repository().homeBanner(restaurantCode);
      if (response != null) {
        _homeBanner = HomeBanner.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("fetch $_isFetching $_isHavingData $_homeBanner");
    notifyListeners();
    return _homeBanner;
  }
}
