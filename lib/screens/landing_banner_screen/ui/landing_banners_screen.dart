import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/home_banner_model.dart';
import 'package:kiosk/screens/dashboard_screen/ui/dashboard_screen.dart';
import 'package:kiosk/screens/landing_banner_screen/viewModel/landing_screen_view_model.dart';
import 'package:kiosk/screens/login_screen/ui/default_login_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/widgets/title_header.dart';
import 'package:provider/provider.dart';

class LandingBannerScreen extends StatefulWidget {
  const LandingBannerScreen({Key? key}) : super(key: key);

  @override
  _LandingBannerScreenState createState() => _LandingBannerScreenState();
}

class _LandingBannerScreenState extends State<LandingBannerScreen> {
  late LandingScreenViewModel viewModel;
  String restaurantLogo = "", restaurantName = "", restaurantId = "";
  bool isEmployeeLogin = false;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    viewModel = Provider.of<LandingScreenViewModel>(context, listen: false);
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantName = value.restaurantName;
        restaurantId = value.restaurantId;
        restaurantLogo = value.restaurantLogo;
        print(restaurantId);
      });
    });
    sessionManager.isEmployeeLoggedIn().then((value) {
      setState(() {
        isEmployeeLogin = value!;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InkWell(
        onTap: () {
          if (isEmployeeLogin) {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => DashboardScreen()));
          } else {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => DefaultLoginScreen()));
          }
        },
        child: Container(
          color: Colors.black12,
          child: Stack(
            children: [
              Container(
                width: ScreenSize.width(context),
                height: ScreenSize.height(context),
                child: Image.asset("assets/images/welcome_bg.png", fit: BoxFit.fill),
              ),
              Positioned(
                child: titleHeaderWidget(restaurantLogo),
                top: 20,
                left: 20,
                right: 20,
              ),
              Positioned(
                child: Container(
                  margin: EdgeInsets.fromLTRB(20, 5, 20, 5),
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
                  height: ScreenSize.height(context),
                  width: ScreenSize.width(context),
                  child: banners(),
                ),
                top: 130,
                bottom: 30,
                left: 90,
                right: 90,
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget banners() {
    return FutureBuilder(
        future: viewModel.homeBannerApi(restaurantId),
        builder: (context, AsyncSnapshot<dynamic> snapshot) {
          print("connectionState ${snapshot.connectionState}");
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(child: loader());
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasError) {
              return Center(
                child: Text(
                  '${snapshot.error} occurred',
                  style: TextStyle(fontSize: 25, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            } else if (snapshot.hasData) {
              var data = snapshot.data as HomeBanner;
              if (data.responseStatus == 1) {
                if (data.homeBannersList!.isNotEmpty) {
                  return CarouselSlider.builder(
                    itemCount: data.homeBannersList!.length,
                    itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) => Card(
                      elevation: 2,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(65)),
                      child: Container(
                          width: ScreenSize.width(context),
                          height: ScreenSize.height(context),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: data.homeBannersList![itemIndex].image == ""
                                ? Image.network(restaurantLogo, fit: BoxFit.fill)
                                : Image.network(data.homeBannersList![itemIndex].image, fit: BoxFit.fill),
                          )),
                    ),
                    options: CarouselOptions(height: ScreenSize.height(context), viewportFraction: 1.0, autoPlay: true, enlargeCenterPage: true),
                  );
                } else {
                  return Center(
                    child: Text(
                      noDataAvailable,
                      style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                    ),
                  );
                }
              } else {
                return Center(
                  child: Text(
                    data.result,
                    style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                  ),
                );
              }
            } else {
              return Center(
                child: Text(
                  pleaseTryAgain,
                  style: TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                ),
              );
            }
          } else {
            return Center(child: loader());
          }
        });
  }
}
