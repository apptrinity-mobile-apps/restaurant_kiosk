import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';

import 'package:kiosk/widgets/cart_count_button_white_bg.dart';
import 'package:kiosk/widgets/menu_item_single.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:kiosk/widgets/reset_cart_dialog.dart';

class SaucesMenuScreen extends StatefulWidget {
  const SaucesMenuScreen({Key? key}) : super(key: key);

  @override
  _SaucesMenuScreenState createState() => _SaucesMenuScreenState();
}

class _SaucesMenuScreenState extends State<SaucesMenuScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 1.7,
                child: Container(
                  width: ScreenSize.width(context),
                  height: ScreenSize.height(context),
                  margin: EdgeInsets.only(right: 20),
                  child: Column(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        width: ScreenSize.width(context),
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: RichText(
                          textScaleFactor: 1.5,
                          text: TextSpan(
                            text: '',
                            children: <TextSpan>[
                              TextSpan(
                                text: dontForget,
                                style: TextStyle(
                                    fontSize: 28, color: Colors.white, fontFamily: "Palace", letterSpacing: 2.25, fontWeight: FontWeight.w500),
                              ),
                              TextSpan(
                                text: dippingSauce.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 28, color: Colors.white, letterSpacing: 1.89, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          width: ScreenSize.width(context),
                          child: ListView(
                            children: [
                              GridView.builder(
                                  itemCount: 10,
                                  shrinkWrap: true,
                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 3,
                                    crossAxisSpacing: 8,
                                    mainAxisSpacing: 8,
                                    childAspectRatio: (1),
                                  ),
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                      onTap: () {},
                                      child: Container(
                                        height: ScreenSize.height(context),
                                        width: ScreenSize.width(context),
                                        child: Card(
                                          elevation: 3,
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                          color: Colors.white,
                                          child: menuItem(context),
                                        ),
                                      ),
                                    );
                                  }),
                            ],
                          ),
                        ),
                        flex: 1,
                      ),
                      Container(
                        width: ScreenSize.width(context),
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(25),
                                    child: TextButton(
                                        onPressed: () {},
                                        child: Padding(
                                          padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                                          child: Text(
                                            noThanks.toUpperCase(),
                                            style: TextStyle(
                                                fontSize: 20,
                                                color: Colors.white,
                                                letterSpacing: 0.9,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          ),
                                        )))),
                            Container(
                              height: 150,
                              child: Image.asset(
                                "assets/images/default_chicken_center_white.png",
                                fit: BoxFit.fill,
                              ),
                            ),
                            Container(
                                margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(25),
                                  gradient: button_red_gradient,
                                ),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(25),
                                    child: TextButton(
                                      onPressed: () {},
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                                        child: Text(
                                          continue_str.toUpperCase(),
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.white,
                                              letterSpacing: 0.9,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    )))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              top: 40,
              left: 30,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 2.8,
                decoration:
                    BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                child: Column(
                  children: [
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                          gradient: button_red_gradient,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                      child: Center(
                        child: Text(
                          yourItems.toUpperCase(),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, color: Colors.white, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    Expanded(
                        child: Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: ListView.builder(
                          itemCount: 5,
                          itemBuilder: (context, index) {
                            return Container(
                              width: ScreenSize.width(context),
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                      child: Text(
                                        defItemName.toUpperCase(),
                                        maxLines: 2,
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: def_color_red,
                                            letterSpacing: 0.3,
                                            fontFamily: "Swis721-Blk",
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                  Expanded(
                                      child: Padding(
                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        Text(
                                          "\u0024",
                                          maxLines: 1,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                        Text(
                                          "0.00",
                                          maxLines: 1,
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        )
                                      ],
                                    ),
                                  )),
                                ],
                              ),
                            );
                          }),
                    )),
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                        gradient: button_red_gradient,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Text(
                                itemTotal.toUpperCase(),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                              ),
                            ),
                            flex: 2,
                          ),
                          Expanded(
                              child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "\u0024",
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  "0.00",
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              right: 20,
              top: 40,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
          },
          child: cartCountButton(),
        ),
      ],
    );
  }
}
