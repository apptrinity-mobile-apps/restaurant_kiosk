import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/dinning_options_model.dart';
import 'package:kiosk/models/order_model.dart';
import 'package:kiosk/screens/order_success_screen/ui/order_success_screen.dart';
import 'package:kiosk/screens/payment_options_screen/viewModel/place_order_view_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/utils/snack_bar.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:provider/provider.dart';

class PaymentOptionsScreen extends StatefulWidget {
  const PaymentOptionsScreen({Key? key}) : super(key: key);

  @override
  _PaymentOptionsScreenState createState() => _PaymentOptionsScreenState();
}

class _PaymentOptionsScreenState extends State<PaymentOptionsScreen> {
  List<OrderItems> cartList = [];
  double total = 0.00, subTotal = 0.00, taxAmount = 0.00;
  int totalItems = 0;
  late CartModel _cart;
  String restaurantId = "", dineInOptionId = "", dineInOptionName = "", dineInBehaviour = "", empId = "", deviceId = "";
  late CustomerInfo customerInfo;
  late DineInList mDineOption;
  late PlaceOrderViewModel viewModel;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    viewModel = Provider.of<PlaceOrderViewModel>(context, listen: false);
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantId = value.restaurantId;
      });
    });
    sessionManager.getDineOptions().then((value) {
      setState(() {
        print("value $value");
        if (value == "") {
          mDineOption = DineInList(behavior: "", id: "", optionName: "");
        } else {
          mDineOption = DineInList.fromJson(jsonDecode(value));
        }
        dineInOptionId = mDineOption.id;
        dineInOptionName = mDineOption.optionName;
        dineInBehaviour = mDineOption.behavior;
      });
    });
    sessionManager.getCustomerInfo().then((value) {
      setState(() {
        if (value == "") {
          customerInfo = CustomerInfo(name: "", phoneNumber: "", email: "");
        } else {
          customerInfo = CustomerInfo.fromJson(jsonDecode(value));
        }
      });
    });
    sessionManager.getEmployeeDetails().then((value) {
      if (value.userDetails != null) {
        empId = value.userDetails!.id;
      } else {
        empId = "";
      }
    });
    sessionManager.getDeviceId().then((value) {
      setState(() {
        deviceId = value;
      });
    });
    cartList = Provider.of<CartViewModel>(context, listen: false).cartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    subTotal = Provider.of<CartViewModel>(context, listen: true).getSubTotal;
    taxAmount = Provider.of<CartViewModel>(context, listen: true).getTotalTaxAmount;
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 1.7,
                child: Container(
                  width: ScreenSize.width(context),
                  height: ScreenSize.height(context),
                  margin: EdgeInsets.fromLTRB(0, 40, 20, 40),
                  child: Column(
                    children: [
                      Expanded(
                          child: Container(
                        margin: EdgeInsets.fromLTRB(10, 40, 10, 20),
                        child: Container(
                          margin: EdgeInsets.only(top: 40),
                          child: Column(
                            children: [
                              Container(
                                margin: EdgeInsets.all(5),
                                child: Card(
                                  elevation: 3,
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                  child: InkWell(
                                    onTap: () {
                                      createCartModel();
                                      /*Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => PaymentScreen(
                                              paymentType: "gift_card",
                                            )));*/
                                      showSnackBar(context, "Coming Soon..");
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                            child: CircleAvatar(
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.all(8),
                                                  child: Image.asset("assets/images/gift_card.png"),
                                                ),
                                              ),
                                              backgroundColor: def_color_red,
                                            ),
                                          ),
                                          Text(
                                            payWithGiftCard.toUpperCase(),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24,
                                                color: def_color_red,
                                                letterSpacing: 0.63,
                                                fontFamily: "Swis721",
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                child: Card(
                                  elevation: 3,
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                  child: InkWell(
                                    onTap: () {
                                      createCartModel();
                                      /*Navigator.of(context).push(MaterialPageRoute(
                                        builder: (context) => PaymentScreen(
                                              paymentType: "card",
                                            )));*/
                                      showSnackBar(context, "Coming Soon..");
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                            child: CircleAvatar(
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.all(8),
                                                  child: Image.asset("assets/images/credit_card.png"),
                                                ),
                                              ),
                                              backgroundColor: def_color_red,
                                            ),
                                          ),
                                          Text(
                                            payWithCard.toUpperCase(),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24,
                                                color: def_color_red,
                                                letterSpacing: 0.63,
                                                fontFamily: "Swis721",
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                margin: EdgeInsets.all(5),
                                child: Card(
                                  elevation: 3,
                                  color: Colors.white,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                  child: InkWell(
                                    onTap: () async {
                                      createCartModel();
                                      showDialog(
                                          context: context,
                                          barrierDismissible: false,
                                          builder: (BuildContext context) {
                                            return loader();
                                          });
                                      await viewModel.addOrderApi(_cart).then((value) {
                                        OrderModel? _orderModel;
                                        Navigator.of(context).pop();
                                        if (viewModel.orderModel != null) {
                                          setState(() {
                                            _orderModel = viewModel.orderModel;
                                          });
                                          if (_orderModel!.responseStatus == 1) {
                                            Navigator.of(context).pushAndRemoveUntil(
                                                MaterialPageRoute(
                                                    builder: (context) => OrderSuccessScreen(
                                                          orderUniqueId: _orderModel!.orderUniqueId,
                                                          orderId: _orderModel!.orderId,
                                                        )),
                                                (Route<dynamic> route) => false);
                                          } else {
                                            showSnackBar(context, _orderModel!.result);
                                          }
                                        } else {
                                          showSnackBar(context, pleaseTryAgain);
                                        }
                                      });
                                    },
                                    child: Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        children: [
                                          Container(
                                            margin: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                            child: CircleAvatar(
                                              child: Center(
                                                child: Padding(
                                                  padding: EdgeInsets.all(8),
                                                  child: Image.asset("assets/images/credit_card.png"),
                                                ),
                                              ),
                                              backgroundColor: def_color_red,
                                            ),
                                          ),
                                          Text(
                                            payWithCash.toUpperCase(),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 24,
                                                color: def_color_red,
                                                letterSpacing: 0.63,
                                                fontFamily: "Swis721",
                                                fontWeight: FontWeight.w700),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )),
                    ],
                  ),
                ),
              ),
              top: 40,
              left: 30,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: Container(
                height: ScreenSize.height(context),
                width: ScreenSize.width(context) / 2.8,
                decoration:
                    BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                child: Column(
                  children: [
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                          gradient: button_red_gradient,
                          borderRadius: BorderRadius.only(topLeft: Radius.circular(25), topRight: Radius.circular(25))),
                      child: Center(
                        child: Text(
                          yourItems.toUpperCase(),
                          maxLines: 1,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 16, color: Colors.white, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                    Expanded(
                        child: Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      child: ListView.builder(
                          itemCount: cartList.length,
                          itemBuilder: (context, index) {
                            return Container(
                              width: ScreenSize.width(context),
                              padding: EdgeInsets.fromLTRB(0, 5, 0, 5),
                              child: Column(children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                        child: Text(
                                          cartList[index].itemName.toUpperCase(),
                                          maxLines: 2,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      flex: 2,
                                    ),
                                    Center(
                                      child: Container(
                                        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                        child: Text(
                                          cartList[index].quantity.toString(),
                                          maxLines: 2,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: def_color_red,
                                              letterSpacing: 0.3,
                                              fontFamily: "Swis721-Blk",
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                        child: Padding(
                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            "\u0024",
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          ),
                                          Text(
                                            (cartList[index].unitPrice * cartList[index].quantity).toStringAsFixed(2),
                                            maxLines: 1,
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 14,
                                                color: def_color_red,
                                                letterSpacing: 0.3,
                                                fontFamily: "Swis721-Blk",
                                                fontWeight: FontWeight.w500),
                                          )
                                        ],
                                      ),
                                    )),
                                  ],
                                ),
                                cartList[index].modifiersList.isEmpty
                                    ? SizedBox()
                                    : ListView.builder(
                                        shrinkWrap: true,
                                        itemCount: cartList[index].modifiersList.length,
                                        itemBuilder: (context, i) {
                                          return Container(
                                              width: ScreenSize.width(context),
                                              padding: EdgeInsets.fromLTRB(0, 1, 0, 1),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Padding(
                                                      padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                      child: Text(
                                                        cartList[index].modifiersList[i].optionName.toUpperCase(),
                                                        maxLines: 2,
                                                        textAlign: TextAlign.start,
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            color: Colors.black38,
                                                            letterSpacing: 0.3,
                                                            fontFamily: "Swis721-Blk",
                                                            fontWeight: FontWeight.w500),
                                                      ),
                                                    ),
                                                    flex: 2,
                                                  ),
                                                  Expanded(
                                                      child: Padding(
                                                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      children: [
                                                        Text(
                                                          "\u0024",
                                                          maxLines: 1,
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        ),
                                                        Text(
                                                          cartList[index].modifiersList[i].price.toString(),
                                                          maxLines: 1,
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.black38,
                                                              letterSpacing: 0.3,
                                                              fontFamily: "Swis721-Blk",
                                                              fontWeight: FontWeight.w500),
                                                        )
                                                      ],
                                                    ),
                                                  )),
                                                ],
                                              ));
                                        })
                              ]),
                            );
                          }),
                    )),
                    Container(
                      width: ScreenSize.width(context),
                      padding: EdgeInsets.fromLTRB(10, 20, 10, 20),
                      decoration: BoxDecoration(
                        gradient: button_red_gradient,
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                              child: Text(
                                itemTotal.toUpperCase(),
                                maxLines: 1,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                              ),
                            ),
                            flex: 2,
                          ),
                          Expanded(
                              child: Padding(
                            padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "\u0024",
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                ),
                                Text(
                                  total.toStringAsFixed(2),
                                  maxLines: 1,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontSize: 14, color: Colors.white, letterSpacing: 0.6, fontFamily: "Swis721", fontWeight: FontWeight.w700),
                                )
                              ],
                            ),
                          )),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              right: 20,
              top: 40,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {},
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }

  void createCartModel() {
    print("$subTotal $total");
    _cart = CartModel(
        restaurantId: restaurantId,
        employeeId: empId,
        dineInOptionId: dineInOptionId,
        dineInOptionName: dineInOptionName,
        dineInBehaviour: dineInBehaviour,
        orderItems: cartList,
        paymentDetails: [],
        subTotal: subTotal,
        taxAmount: taxAmount,
        totalAmount: total,
        customerInfo: customerInfo,
        tipAmount: 0,
        creditsUsed: 0,
        discountAmount: 0,
        serviceChargesAmount: 0,
        deviceId: deviceId);
    print(jsonEncode(_cart));
  }
}
