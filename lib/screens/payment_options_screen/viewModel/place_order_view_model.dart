import 'package:flutter/material.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/order_model.dart';
import 'package:kiosk/services/repositories.dart';

class PlaceOrderViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;

  OrderModel? _orderModel;

  OrderModel? get orderModel {
    return _orderModel;
  }

  Future<bool?> addOrderApi(CartModel cartModel) async {
    _isFetching = true;
    _isHavingData = false;
    notifyListeners();
    try {
      dynamic response = await Repository().sendOrder(cartModel);
      if (response != null) {
        _orderModel = OrderModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_orderModel $_isFetching $_isHavingData $_orderModel");
    notifyListeners();
    return _isHavingData;
  }
}
