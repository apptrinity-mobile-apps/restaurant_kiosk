import 'dart:convert';
import 'package:accordion/accordion.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/employee_login_model.dart';
import 'package:kiosk/screens/dashboard_screen/ui/dashboard_screen.dart';
import 'package:kiosk/screens/login_screen/viewModel/default_login_view_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading_white.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/widgets/bottom_sheet_keyboard.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:provider/provider.dart';

class DefaultLoginScreen extends StatefulWidget {
  const DefaultLoginScreen({Key? key}) : super(key: key);

  @override
  _DefaultLoginScreenState createState() => _DefaultLoginScreenState();
}

class _DefaultLoginScreenState extends State<DefaultLoginScreen> {
  String restaurantLogo = "", restaurantName = "", restaurantId = "";
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passcodeController = TextEditingController();
  late DefaultLoginViewModel viewModel;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    viewModel = Provider.of<DefaultLoginViewModel>(context, listen: false);
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantId = value.restaurantId;
        restaurantLogo = value.restaurantLogo;
        restaurantName = value.restaurantName;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                height: ScreenSize.height(context),
                width: ScreenSize.width(context),
                child: Card(
                  color: Colors.white,
                  elevation: 3,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                  child: Stack(
                    children: [
                      Positioned(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 70),
                              child: Image.asset(
                                "assets/images/default_left_bottom_corner.png",
                                width: 100,
                                height: 100,
                              ),
                            ),
                            Flexible(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Center(
                                    child: Image.asset(
                                      "assets/images/default_chicken_center_red.png",
                                      width: 100,
                                      height: 100,
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                                    child: Text(
                                      restaurantName,
                                      style: TextStyle(fontSize: 18, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w500),
                                      overflow: TextOverflow.clip,
                                      maxLines: 3,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 70),
                              child: Image.asset(
                                "assets/images/default_right_bottom_corner.png",
                                width: 100,
                                height: 100,
                              ),
                            )
                          ],
                        ),
                        bottom: 0,
                        left: 0,
                        right: 0,
                      ),
                      Positioned(
                        child: Container(
                          height: ScreenSize.height(context),
                          width: ScreenSize.width(context),
                          child: Row(children: [
                            Expanded(
                                child: Container(
                              margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                              width: ScreenSize.width(context) / 2.4,
                              child: Card(
                                elevation: 3,
                                shadowColor: def_color_red,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                                child: InkWell(
                                  onTap: () {
                                    showEmployeeLoginDialog();
                                  },
                                  child: loginTypeWidget(loginAsEmployee, "assets/images/default_waiter.png"),
                                ),
                              ),
                            )),
                            Expanded(
                                child: Container(
                              margin: EdgeInsets.fromLTRB(5, 10, 5, 10),
                              width: ScreenSize.width(context) / 2.4,
                              child: Card(
                                elevation: 3,
                                shadowColor: def_color_red,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                                child: InkWell(
                                  onTap: () {
                                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => DashboardScreen()));
                                  },
                                  child: loginTypeWidget(loginAsCustomer, "assets/images/default_customer.png"),
                                ),
                              ),
                            )),
                          ]),
                        ),
                        top: 50,
                        right: 50,
                        left: 50,
                        bottom: ScreenSize.height(context) / 5,
                      ),
                    ],
                  ),
                ),
              ),
              top: 30,
              bottom: 30,
              left: 30,
              right: 30,
            )
          ],
        ),
      ),
    );
  }

  Widget loginTypeWidget(String text, String image) {
    return Padding(
      padding: EdgeInsets.all(5),
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: EdgeInsets.all(15),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: Container(
                    height: ScreenSize.height(context) / 3,
                    width: ScreenSize.width(context) / 2.3,
                    child: Image.asset(
                      image,
                      height: ScreenSize.height(context) / 6,
                      width: ScreenSize.width(context) / 4.6,
                      // fit: BoxFit.fill,
                    )),
              ),
            ),
            flex: 2,
          ),
          Padding(
            padding: EdgeInsets.all(15),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: Container(
                  width: ScreenSize.width(context) / 2.3,
                  padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                  decoration: BoxDecoration(gradient: button_red_gradient),
                  child: Center(
                    child: Text(
                      text,
                      maxLines: 1,
                      style: TextStyle(fontSize: 24, color: Colors.white, letterSpacing: 0.5, fontFamily: "Swis721", fontWeight: FontWeight.w800),
                    ),
                  )),
            ),
          ),
        ],
      ),
    );
  }

  void showEmployeeLoginDialog() {
    passcodeController = TextEditingController();
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        enableDrag: false,
        isScrollControlled: true,
        builder: (BuildContext _context) {
          return Container(
              height: ScreenSize.height(context),
              margin: EdgeInsets.fromLTRB(10, 10, 20, 0),
              child: Accordion(maxOpenSections: 1, children: [
                AccordionSection(
                  isOpen: true,
                  headerBackgroundColor: def_color_red,
                  contentBorderColor: def_color_red,
                  headerPadding: EdgeInsets.all(15),
                  header: Text(
                    loginWithEmailPassword,
                    style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                  ),
                  content: emailAndPassword(),
                  contentHorizontalPadding: 20,
                  contentBorderWidth: 1,
                ),
                AccordionSection(
                  isOpen: false,
                  headerBackgroundColor: def_color_red,
                  contentBorderColor: def_color_red,
                  headerPadding: EdgeInsets.all(15),
                  header: Text(
                    loginWithPasscode,
                    style: TextStyle(fontSize: 16, color: Colors.white, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                  ),
                  content: passcode(),
                  contentHorizontalPadding: 20,
                  contentBorderWidth: 1,
                ),
              ]));
        });
  }

  Widget emailAndPassword() {
    return Container(
        width: ScreenSize.width(context) / 2,
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.all(10),
                child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(
                    enterEmail,
                    style: TextStyle(
                      fontFamily: 'Swis721-Blk',
                      fontSize: 16,
                      color: def_color_red,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), border: Border.all(color: Colors.black)),
                    child: TextFormField(
                      onTap: () {
                        keyboard(context, emailController, 'text');
                      },
                      controller: emailController,
                      cursorColor: def_color_red,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          fillColor: Colors.white,
                          hintText: enterEmail,
                          hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                    ),
                  )
                ]),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(
                    enterPassword,
                    style: TextStyle(
                      fontFamily: 'Swis721-Blk',
                      fontSize: 16,
                      color: def_color_red,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(30), border: Border.all(color: Colors.black)),
                    child: TextFormField(
                      onTap: () {
                        keyboard(context, passwordController, 'text');
                      },
                      controller: passwordController,
                      cursorColor: def_color_red,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          fillColor: Colors.white,
                          hintText: enterPassword,
                          hintStyle: TextStyle(fontSize: 16, color: Colors.grey)),
                    ),
                  )
                ]),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: def_color_red,
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: TextButton(
                              onPressed: () {
                                if (emailController.text != "" && passwordController.text != "") {
                                  showDialog<void>(
                                      context: context,
                                      barrierDismissible: false,
                                      builder: (BuildContext context) {
                                        return loaderWhite();
                                      });
                                  viewModel.employeeLoginUsingEmailApi(emailController.text.trim(), passwordController.text.trim()).then((value) {
                                    EmployeeLoginModel? employeeLoginModel;
                                    Navigator.of(context).pop();
                                    if (viewModel.getEmployeeDetails != null) {
                                      employeeLoginModel = Provider.of<DefaultLoginViewModel>(context, listen: false).getEmployeeDetails;
                                      if (employeeLoginModel!.responseStatus == 1) {
                                        sessionManager.employeeLogin(true);
                                        sessionManager.saveEmployeeDetails(jsonEncode(employeeLoginModel));
                                        Navigator.of(context).pop();
                                        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => DashboardScreen()));
                                      } else {
                                        BotToast.showText(
                                          text: employeeLoginModel.result,
                                          align: Alignment(0, 0),
                                        );
                                      }
                                    } else {
                                      BotToast.showText(
                                        text: pleaseTryAgain,
                                        align: Alignment(0, 0),
                                      );
                                    }
                                  });
                                } else {
                                  BotToast.showText(
                                    text: "All fields are mandatory!",
                                    align: Alignment(0, 0),
                                  );
                                }
                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                child: Text(
                                  submit.toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ))),
                    Container(
                        margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.black,
                          ),
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: TextButton(
                              onPressed: () {
                                emailController.clear();
                                passwordController.clear();
                                passcodeController.clear();
                                //passcodeController.dispose();
                                Navigator.of(context).pop();
                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                child: Text(
                                  cancel.toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 16, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                                ),
                              ),
                            )))
                  ],
                ),
              ),
            ]));
  }

  Widget passcode() {
    return Container(
        width: ScreenSize.width(context) / 2,
        padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
        child: Container(
          child: Center(
            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                enterPasscode,
                style: TextStyle(
                  fontFamily: 'Swis721-Blk',
                  fontSize: 16,
                  color: def_color_red,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                margin: EdgeInsets.fromLTRB(0, 5, 0, 5),
                child: PinCodeTextField(
                  autoFocus: false,
                  controller: passcodeController,
                  appContext: context,
                  pastedTextStyle: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                  ),
                  length: 6,
                  obscureText: false,
                  pinTheme: PinTheme(
                      fieldHeight: 50,
                      fieldWidth: 50,
                      selectedColor: Colors.black,
                      activeColor: Colors.black,
                      inactiveColor: Colors.grey,
                      selectedFillColor: Colors.white,
                      activeFillColor: Colors.white,
                      inactiveFillColor: Colors.grey,
                      shape: PinCodeFieldShape.box),
                  cursorColor: Colors.black,
                  keyboardType: TextInputType.number,
                  onChanged: (otp) {},
                  onCompleted: (otp) {
                    //Navigator.of(context).pop();
                  },
                  onTap: () {
                    keyboard(context, passcodeController, 'number');
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                        margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: def_color_red,
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: TextButton(
                              onPressed: () {
                                if (passcodeController.text != "") {
                                  if (passcodeController.text.length == 6) {
                                    showDialog<void>(
                                        context: context,
                                        barrierDismissible: false,
                                        builder: (BuildContext context) {
                                          return loaderWhite();
                                        });
                                    viewModel.employeeLoginUsingPasscodeApi(restaurantId, passcodeController.text.trim()).then((value) {
                                      EmployeeLoginModel? employeeLoginModel;
                                      Navigator.of(context).pop();
                                      if (viewModel.getEmployeeDetails != null) {
                                        employeeLoginModel = Provider.of<DefaultLoginViewModel>(context, listen: false).getEmployeeDetails;
                                        if (employeeLoginModel!.responseStatus == 1) {
                                          sessionManager.employeeLogin(true);
                                          sessionManager.saveEmployeeDetails(jsonEncode(employeeLoginModel));
                                          Navigator.of(context).pop();
                                          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => DashboardScreen()));
                                        } else {
                                          BotToast.showText(
                                            text: employeeLoginModel.result,
                                            align: Alignment(0, 0),
                                          );
                                        }
                                      } else {
                                        BotToast.showText(
                                          text: pleaseTryAgain,
                                          align: Alignment(0, 0),
                                        );
                                      }
                                    });
                                  } else {
                                    BotToast.showText(
                                      text: "Passcode must be minimum of 6 characters!",
                                      align: Alignment(0, 0),
                                    );
                                  }
                                } else {
                                  BotToast.showText(
                                    text: "Enter a valid Passcode!",
                                    align: Alignment(0, 0),
                                  );
                                }
                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                child: Text(
                                  submit.toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                                ),
                              ),
                            ))),
                    Container(
                        margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                        width: 180,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: Colors.white,
                          border: Border.all(
                            color: Colors.black,
                          ),
                        ),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(50),
                            child: TextButton(
                              onPressed: () {
                                emailController.clear();
                                passwordController.clear();
                                passcodeController.clear();
                                //passcodeController.dispose();
                                Navigator.of(context).pop();
                              },
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(20, 15, 20, 15),
                                child: Text(
                                  cancel.toUpperCase(),
                                  style: TextStyle(
                                      fontSize: 16, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                                ),
                              ),
                            )))
                  ],
                ),
              )
            ]),
          ),
        ));
  }
}
