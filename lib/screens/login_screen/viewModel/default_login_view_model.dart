import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/basic_response.dart';
import 'package:kiosk/models/employee_login_model.dart';
import 'package:kiosk/services/repositories.dart';

class DefaultLoginViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  EmployeeLoginModel? _employeeLoginModel;
  BasicResponse? _basicResponse;

  EmployeeLoginModel? get getEmployeeDetails {
    return _employeeLoginModel;
  }

  BasicResponse? get getBasicResponse {
    return _basicResponse;
  }

  Future<EmployeeLoginModel?> employeeLoginUsingEmailApi(String email, String password) async {
    _isFetching = true;
    _isHavingData = false;
    _employeeLoginModel = EmployeeLoginModel(result: '', responseStatus: 0, userDetails: null);
    notifyListeners();
    try {
      dynamic response = await Repository().employeeLoginUsingEmail(email, password);
      if (response != null) {
        _employeeLoginModel = EmployeeLoginModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_employeeLoginModel $_isFetching $_isHavingData $_employeeLoginModel");
    notifyListeners();
    return _employeeLoginModel;
  }

  Future<EmployeeLoginModel?> employeeLoginUsingPasscodeApi(String restaurantId, String passcode) async {
    _isFetching = true;
    _isHavingData = false;
    _employeeLoginModel = EmployeeLoginModel(result: '', responseStatus: 0, userDetails: null);
    notifyListeners();
    try {
      dynamic response = await Repository().employeeLoginUsingPasscode(restaurantId, passcode);
      if (response != null) {
        _employeeLoginModel = EmployeeLoginModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_employeeLoginModel $_isFetching $_isHavingData $_employeeLoginModel");
    notifyListeners();
    return _employeeLoginModel;
  }

  Future<BasicResponse?> employeeLogoutApi(String restaurantId, String empId) async {
    _isFetching = true;
    _isHavingData = false;
    _basicResponse = BasicResponse(result: '', responseStatus: 0);
    notifyListeners();
    try {
      dynamic response = await Repository().employeeLogout(restaurantId, empId);
      if (response != null) {
        _basicResponse = BasicResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_basicResponse $_isFetching $_isHavingData ${_basicResponse!.responseStatus}");
    notifyListeners();
    return _basicResponse;
  }
}
