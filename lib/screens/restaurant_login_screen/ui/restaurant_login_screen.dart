import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/screens/restaurant_login_screen/viewModel/restaurant_login_view_model.dart';
import 'package:kiosk/screens/restaurant_otp_screen/ui/restaurant_otp_screen.dart';
import 'package:kiosk/models/restaurant_login_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/snack_bar.dart';
import 'package:kiosk/widgets/bottom_sheet_keyboard.dart';
import 'package:provider/provider.dart';

class RestaurantLoginScreen extends StatefulWidget {
  const RestaurantLoginScreen({Key? key}) : super(key: key);

  @override
  _RestaurantLoginScreenState createState() => _RestaurantLoginScreenState();
}

class _RestaurantLoginScreenState extends State<RestaurantLoginScreen> {
  TextEditingController codeTextController = new TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final RestaurantLoginViewModel viewModel = Provider.of<RestaurantLoginViewModel>(context, listen: false);

    return Scaffold(
        body: Container(
      color: Colors.black12,
      child: Stack(children: [
        Container(
          width: ScreenSize.width(context),
          height: ScreenSize.height(context),
          child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
        ),
        Positioned(
          child: Container(
            width: ScreenSize.width(context),
            height: ScreenSize.height(context),
            child: Column(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                enterRestaurantCode,
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontSize: 24,
                  fontFamily: "Swis721",
                  fontWeight: FontWeight.w700,
                  color: Colors.white,
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                  padding: EdgeInsets.fromLTRB(20, 5, 20, 5),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
                  child: TextFormField(
                    onTap: () {
                      keyboard(context, codeTextController, 'text');
                    },
                    enableInteractiveSelection: false,
                    controller: codeTextController,
                    cursorColor: def_color_red,
                    decoration: InputDecoration(border: InputBorder.none, hintText: enterRestaurantCode, hintStyle: TextStyle(color: Colors.grey)),
                  )),
              Center(
                child: Container(
                    margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                    width: 120,
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(25), color: Colors.white),
                    child: TextButton(
                      onPressed: () async {
                        if (codeTextController.text.isNotEmpty) {
                          showDialog(
                              context: context,
                              barrierDismissible: false,
                              builder: (BuildContext context) {
                                return loader();
                              });
                          await viewModel.restaurantLoginApi(codeTextController.text).then((value) {
                            RestaurantLoginModel? restaurantLoginModel;
                            Navigator.of(context).pop();
                            if (viewModel.restaurantLoginModel != null) {
                              setState(() {
                                restaurantLoginModel = viewModel.restaurantLoginModel;
                              });
                            }
                            if (restaurantLoginModel!.responseStatus == 1) {
                              print("OTP: " + restaurantLoginModel!.OTP + "-----" + restaurantLoginModel!.restaurantLogo);
                              showSnackBar(context, restaurantLoginModel!.OTP);
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => RestaurantOtpScreen()));
                            } else {
                              showSnackBar(context, restaurantLoginModel!.result);
                            }
                          });
                        } else {
                          showSnackBar(context, "Enter restaurant code.");
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Text(
                          submit.toUpperCase(),
                          style: TextStyle(
                            fontSize: 21,
                            fontFamily: "Swis721",
                            fontWeight: FontWeight.w700,
                            letterSpacing: 0.53,
                            color: def_color_red,
                          ),
                        ),
                      ),
                    )),
              )
            ]),
          ),
          top: ScreenSize.height(context) / 5,
          bottom: ScreenSize.height(context) / 5,
          right: ScreenSize.width(context) / 5,
          left: ScreenSize.width(context) / 5,
        )
      ]),
    ));
  }
}
