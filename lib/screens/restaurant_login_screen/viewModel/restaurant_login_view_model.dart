import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/services/repositories.dart';
import 'package:kiosk/models/restaurant_login_model.dart';

class RestaurantLoginViewModel with ChangeNotifier {
  bool _isFetching = false, _isHavingData = false;
  RestaurantLoginModel? _restaurantLoginModel;

  RestaurantLoginModel? get restaurantLoginModel {
    return _restaurantLoginModel;
  }

  Future<RestaurantLoginModel?> restaurantLoginApi(String restaurantCode) async {
    _isFetching = true;
    _isHavingData = false;
    _restaurantLoginModel = RestaurantLoginModel(result: "", responseStatus: 0, OTP: "", restaurantId: "", restaurantLogo: "", restaurantName: "");
    notifyListeners();
    try {
      dynamic response = await Repository().restaurantLogin(restaurantCode);
      if (response != null) {
        _restaurantLoginModel = RestaurantLoginModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("fetch $_isFetching $_isHavingData $_restaurantLoginModel");
    notifyListeners();
    return _restaurantLoginModel;
  }
}
