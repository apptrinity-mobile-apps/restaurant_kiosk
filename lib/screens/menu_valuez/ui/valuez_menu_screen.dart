import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/menu_item_single.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:provider/provider.dart';

class ValuezItemsMenuScreen extends StatefulWidget {
  const ValuezItemsMenuScreen({Key? key}) : super(key: key);

  @override
  _ValuezItemsMenuScreenState createState() => _ValuezItemsMenuScreenState();
}

class _ValuezItemsMenuScreenState extends State<ValuezItemsMenuScreen> {
  List<OrderItems> cartList = [];
  double total = 0.00;
  int totalItems = 0;

  @override
  void initState() {
    cartList = Provider.of<CartViewModel>(context, listen: false).getCartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                child: Text(
                  valuez,
                  maxLines: 2,
                  textScaleFactor: 1.5,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 32, color: Colors.white, letterSpacing: 2.79, fontFamily: "Palace", fontWeight: FontWeight.w500),
                ),
              ),
              top: 20,
              left: 20,
              right: 20,
            ),
            Positioned(child: _items(), top: 80, right: 20, bottom: ScreenSize.height(context) / 8, left: 20),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        cartCountButton(),
      ],
    );
  }

  Widget _items() {
    return GridView.builder(
        scrollDirection: Axis.vertical,
        itemCount: 16,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4, childAspectRatio: 1, mainAxisSpacing: 8, crossAxisSpacing: 8),
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              // Navigator.of(context).push(MaterialPageRoute(builder: (context) => SizeBasedItemsScreen()));
            },
            child: Container(
              height: ScreenSize.height(context),
              width: ScreenSize.width(context),
              child: Card(
                elevation: 3,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                color: Colors.white,
                child: menuItem(context),
              ),
            ),
          );
        });
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
              },
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }
}
