import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';
import 'package:kiosk/screens/cart_screen/ui/cart_screen.dart';
import 'package:kiosk/screens/modifiers_menu_screen/ui/modifiers_menu_screen.dart';
import 'package:kiosk/screens/sides_menu_screen/ui/sides_menu_screen.dart';
import 'package:kiosk/screens/suggestions_screen/ui/suggestions_screen.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/math_calculations.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/widgets/nutrition_button_white_bg.dart';
import 'package:kiosk/widgets/reset_button_white_bg.dart';
import 'package:provider/provider.dart';

class SizeBasedItemsScreen extends StatefulWidget {
  final ItemsList item;

  const SizeBasedItemsScreen({Key? key, required this.item}) : super(key: key);

  @override
  _SizeBasedItemsScreenState createState() => _SizeBasedItemsScreenState();
}

class _SizeBasedItemsScreenState extends State<SizeBasedItemsScreen> {
  List<String> bottomImagesList = [];
  late ItemsList mMenuItem;
  String restaurantLogo = "";
  List<OrderItems> cartList = [];
  double total = 0.00;
  int totalItems = 0;
  late SessionManager sessionManager;

  @override
  void initState() {
    sessionManager = SessionManager();
    mMenuItem = widget.item;
    bottomImagesList.add("assets/images/default_chicken_left_white.png");
    bottomImagesList.add("assets/images/default_chicken_center_white.png");
    bottomImagesList.add("assets/images/default_chicken_right_white.png");
    sessionManager.getRestaurantDetails().then((value) {
      setState(() {
        restaurantLogo = value.restaurantLogo;
      });
    });
    cartList = Provider.of<CartViewModel>(context, listen: false).getCartList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    total = Provider.of<CartViewModel>(context, listen: true).getFinalTotal;
    totalItems = Provider.of<CartViewModel>(context, listen: true).getQuantities;
    return Scaffold(
      body: Container(
        color: Colors.black12,
        child: Stack(
          children: [
            Container(
              width: ScreenSize.width(context),
              height: ScreenSize.height(context),
              child: Image.asset("assets/images/default_menu_bg.png", fit: BoxFit.fill),
            ),
            Positioned(
              child: Container(
                alignment: Alignment.topLeft,
                width: ScreenSize.width(context) / 7,
                height: ScreenSize.height(context) / 3,
                child: Image.asset(
                  "assets/images/default_menu_left_top.png",
                ),
              ),
              top: 0,
              left: 0,
            ),
            Positioned(
              child: Container(
                width: ScreenSize.width(context),
                height: ScreenSize.height(context),
                child: Column(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      width: ScreenSize.width(context),
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                      child: Text(
                        mMenuItem.name,
                        style:
                            TextStyle(fontSize: 28, color: Colors.white, letterSpacing: 1.89, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                        overflow: TextOverflow.clip,
                        maxLines: 2,
                      ),
                    ),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.fromLTRB(20, 0, 20, 20),
                        width: ScreenSize.width(context),
                        child: ListView(
                          children: [
                            GridView.builder(
                                itemCount: mMenuItem.sizeList.length,
                                shrinkWrap: true,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  crossAxisSpacing: 8,
                                  mainAxisSpacing: 8,
                                  childAspectRatio: (1),
                                ),
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      print("mMenuItem.basePrice ${mMenuItem.basePrice}");
                                      setState(() {
                                        mMenuItem.name = mMenuItem.name + " " + mMenuItem.sizeList[index].sizeName;
                                        mMenuItem.basePrice = mMenuItem.sizeList[index].price;
                                      });
                                      print("mMenuItem.basePrice ------- ${mMenuItem.basePrice}");
                                      if (mMenuItem.suggestibleItems.isNotEmpty) {
                                        Navigator.of(context).push(MaterialPageRoute(
                                            builder: (context) => SidesMenuScreen(
                                                  item: mMenuItem,
                                                )));
                                      } else if (mMenuItem.modifiersList.isNotEmpty) {
                                        Navigator.of(context).push(MaterialPageRoute(
                                            builder: (context) => ModifiersMenuScreen(
                                                  item: mMenuItem,
                                                )));
                                      } else {
                                        BotToast.showText(
                                          text: "${mMenuItem.name} $addedSuccessfully",
                                          align: Alignment(0, 0),
                                        );
                                        double itemTax = 0.0;
                                        if (mMenuItem.taxIncludeOption) {
                                          if (mMenuItem.taxrates.isNotEmpty) {
                                            itemTax = getTaxes(mMenuItem.taxrates, mMenuItem.basePrice, 1);
                                          } else {
                                            itemTax = 0.00;
                                          }
                                        }
                                        print("tax---$itemTax");
                                        OrderItems item = OrderItems(
                                            menuId: mMenuItem.menuId,
                                            menuGroupId: mMenuItem.menuGroupId,
                                            itemId: mMenuItem.id,
                                            quantity: 1,
                                            itemName: mMenuItem.name,
                                            itemDescription: "",
                                            itemImage: mMenuItem.itemImage,
                                            unitPrice: mMenuItem.basePrice,
                                            totalPrice: mMenuItem.basePrice,
                                            itemTax: 0,
                                            discountAmount: 0,
                                            taxIncludeOption: mMenuItem.taxIncludeOption,
                                            taxrates: mMenuItem.taxrates,
                                            modifiersList: []);
                                        Provider.of<CartViewModel>(context, listen: false).addItemToCart(item);
                                        Navigator.of(context).push(MaterialPageRoute(
                                            builder: (context) => SuggestionsScreen(
                                                  item: mMenuItem,
                                                )));
                                      }
                                    },
                                    child: Container(
                                      margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                                      height: ScreenSize.height(context),
                                      width: ScreenSize.width(context),
                                      child: Card(
                                        elevation: 3,
                                        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
                                        color: Colors.white,
                                        child: menuItem(mMenuItem.sizeList[index]),
                                      ),
                                    ),
                                  );
                                }),
                            GridView.builder(
                                itemCount: bottomImagesList.length,
                                shrinkWrap: true,
                                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount: 3,
                                  crossAxisSpacing: 8,
                                  mainAxisSpacing: 8,
                                  childAspectRatio: (1),
                                ),
                                itemBuilder: (context, index) {
                                  return Container(
                                    margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                                    height: ScreenSize.height(context),
                                    width: ScreenSize.width(context),
                                    child: Image.asset(bottomImagesList[index]),
                                  );
                                }),
                          ],
                        ),
                      ),
                      flex: 1,
                    ),
                  ],
                ),
              ),
              top: 0,
              left: 30,
              right: 30,
              bottom: ScreenSize.height(context) / 8,
            ),
            Positioned(
              child: dashboardBottomWidgets(),
              bottom: 20,
              right: 20,
              left: 20,
            )
          ],
        ),
      ),
    );
  }

  Widget dashboardBottomWidgets() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
            margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
            width: 180,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Colors.white,
            ),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
                    child: Text(
                      back.toUpperCase(),
                      style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
                    ),
                  ),
                ))),
        resetButton(context),
        Spacer(),
        nutritionInfoButton(),
        InkWell(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => CartScreen()));
          },
          child: cartCountButton(),
        ),
      ],
    );
  }

  Widget menuItem(SizeList listItem) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
          child: Container(
            padding: EdgeInsets.all(0),
            height: ScreenSize.height(context),
            width: ScreenSize.width(context),
            child: Center(
              child: Image.asset("assets/images/item_bg.png"),
            ),
          ),
        ),
        Positioned(
          bottom: 0,
          right: 0,
          left: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(bottomRight: Radius.circular(25), bottomLeft: Radius.circular(25)),
            child: Container(
              color: Colors.white,
              padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
              width: ScreenSize.width(context),
              child: Center(
                child: Text(
                  listItem.sizeName,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16, color: def_color_red, letterSpacing: 0, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          bottom: 40,
          left: 0,
          right: 0,
          top: 0,
          child: ClipRRect(
            borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
            child: Container(
              padding: EdgeInsets.all(20),
              width: ScreenSize.width(context),
              child: Center(
                child: Container(
                  child: mMenuItem.itemImage == "" ? Image.network(restaurantLogo) : Image.network(mMenuItem.itemImage),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget cartCountButton() {
    return Container(
        margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
        width: 180,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          color: Colors.white,
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: TextButton(
              onPressed: () {},
              child: Padding(
                  padding: EdgeInsets.fromLTRB(10, 6, 10, 6),
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 10),
                        child: CircleAvatar(
                          child: Center(
                            child: Text(
                              totalItems.toString(),
                              style: TextStyle(
                                  fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                          ),
                          backgroundColor: def_color_red,
                        ),
                      ),
                      Expanded(
                          child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "\u0024",
                              style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                            ),
                            Text(
                              total.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                            )
                          ],
                        ),
                      )),
                    ],
                  )),
            )));
  }
}
