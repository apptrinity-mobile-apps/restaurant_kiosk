import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

Widget loaderWhite() {
  return SpinKitCircle(
    color: Colors.white,
    size: 100.0,
  );
}
