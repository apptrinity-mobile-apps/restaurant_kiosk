import 'dart:convert';

import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/employee_login_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:kiosk/models/restaurant_login_model.dart';

class SessionManager {
  static const String restaurantLoggedIn = "login";
  static const String restaurantLoginDetails = "restaurant_login_details";
  static const String employeeLoggedIn = "employee_login";
  static const String employeeLoginDetails = "employee_login_details";
  static const String dineOptionsDetails = "dine_options_details";
  static const String cartDetails = "cart_details";
  static const String tempCartDetails = "temp_cart_details";
  static const String customerDetails = "customer_details";
  static const String deviceNameId = "device_name_id";
  static const String revenueCenterId = "revenue_center_id";
  static const String serviceAreaId = "service_area_id";

  Future<bool?> isLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(restaurantLoggedIn) == null ? false : prefs.getBool(restaurantLoggedIn);
  }

  void login(bool isLogin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(restaurantLoggedIn, isLogin);
  }

  Future<bool?> isEmployeeLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(employeeLoggedIn) == null ? false : prefs.getBool(employeeLoggedIn);
  }

  void employeeLogin(bool isLogin) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(employeeLoggedIn, isLogin);
  }

  void saveEmployeeDetails(userDetails) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(employeeLoginDetails, userDetails);
  }

  Future<EmployeeLoginModel> getEmployeeDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(employeeLoginDetails) ?? "";
    if (_details == "") {
      return EmployeeLoginModel(responseStatus: 0, result: "", userDetails: null);
    } else {
      return EmployeeLoginModel.fromJson(jsonDecode(_details));
    }
  }

  void clearEmployeeDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(employeeLoginDetails, "");
  }

  void saveRestaurantDetails(restaurantDetails) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(restaurantLoginDetails, restaurantDetails);
  }

  Future<RestaurantLoginModel> getRestaurantDetails() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(restaurantLoginDetails) ?? "";
    RestaurantLoginModel _loginResponse = RestaurantLoginModel.fromJson(jsonDecode(_details));
    return _loginResponse;
  }

  void saveDineOptions(dineOptions) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(dineOptionsDetails, dineOptions);
  }

  Future<String> getDineOptions() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(dineOptionsDetails) ?? "";
    return _details;
  }

  void clearDineOptions() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(dineOptionsDetails, "");
  }

  void saveCartDetails(cartItems) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(cartDetails, cartItems);
  }

  Future<List<OrderItems>> getCartItems() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(cartDetails) ?? "[]";
    if (_details == "") {
      return [];
    } else {
      Iterable list = json.decode(_details);
      return list.map((model) => OrderItems.fromJson(model)).toList();
    }
  }

  void saveCustomerInfo(customerInfo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(customerDetails, customerInfo);
  }

  Future<String> getCustomerInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(customerDetails) ?? "";
    return _details;
  }

  void clearCustomerInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(customerDetails, "");
  }

  void clearCartItems() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(cartDetails, "[]");
  }

  void saveTempCartItems(cartItems) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(tempCartDetails, cartItems);
  }

  Future<String> getTempCartItems() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(tempCartDetails) ?? "";
    return _details;
  }

  void clearTempCartItems() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(tempCartDetails, "");
  }

  void saveDeviceId(String deviceId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(deviceNameId, deviceId);
  }

  void saveServiceAreaId(String areaId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(serviceAreaId, areaId);
  }

  Future<String> getDeviceId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(deviceNameId) ?? "";
    return _details;
  }

  Future<String> getServiceAreaId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final _details = prefs.getString(serviceAreaId) ?? "";
    return _details;
  }
}
