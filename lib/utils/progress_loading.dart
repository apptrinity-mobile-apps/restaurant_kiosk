import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kiosk/utils/all_constants.dart';

Widget loader() {
  return SpinKitCircle(
    color: def_color_red,
    size: 100.0,
  );
}
