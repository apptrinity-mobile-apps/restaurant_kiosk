import 'dart:math';

import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';

double alwaysDown(double value, int precision) {
  final isNegative = value.isNegative;
  final mod = pow(10.0, precision);
  final roundDown = (((value.abs() * mod).floor()) / mod);
  return isNegative ? -roundDown : roundDown;
}

double alwaysUp(double value) {
  final isNegative = value.isNegative;
  double offSet = ((value.sign) / 100);
  final roundUp = value + offSet;
  return isNegative ? -roundUp : roundUp;
}

double halfUp(double val, int places) {
  final mod = pow(10.0, places);
  return ((val * mod).round().toDouble() / mod);
}

double halfEven(double val, int places) {
  String y = val.toString().split('.')[1];
  List<String> result = y.split('');
  print("res--- $y $result");
  if (y == "0") {
    return getNumber(0.00);
  } else if (int.parse(result[1]).isEven && int.parse(result[2]) > 5) {
    double result = val + 0.0;
    return getNumber(result);
  } else if (int.parse(result[1]).isEven && int.parse(result[2]) <= 5) {
    double result = val + 0.0;
    return getNumber(result);
  } else if (int.parse(result[1]).isOdd && int.parse(result[2]) < 5) {
    double result = val + 0.0;
    return getNumber(result);
  } else if (int.parse(result[1]).isOdd && int.parse(result[2]) >= 5) {
    double result = val + 0.010;
    return getNumber(result);
  } else {
    double result = val + 0.0;
    return getNumber(result);
  }
}

double getNumber(double input, {int precision = 2}) {
  return input == 0.00 ? 0 : double.parse('$input'.substring(0, '$input'.indexOf('.') + precision + 1));
}

double getTaxes(List<Taxrates> taxRates, double basePrice, int itemQty) {
  double itemTax = 0.00;
  taxRates.forEach((tax) {
    if (tax.taxType == 0) {
      // No tax applicable
      itemTax = 0.00;
    } else if (tax.taxType == 1) {
      // Tax percentage applicable
      var _tax = basePrice * (tax.taxRate / 100);
      if (tax.roundingOptions == 1) {
        // Half Even Rounding
        _tax = halfEven(_tax, 2);
      } else if (tax.roundingOptions == 2) {
        // Half Up Rounding
        _tax = halfUp(_tax, 2);
      } else if (tax.roundingOptions == 3) {
        // Round Down Rounding
        _tax = alwaysDown(_tax, 2);
      } else if (tax.roundingOptions == 4) {
        // Round Up Rounding
        _tax = getNumber(alwaysUp(_tax), precision: 2);
      }
      itemTax = _tax;
    } else if (tax.taxType == 2) {
      // Fixed tax applicable
      itemTax = itemQty * double.parse(tax.taxRate.toString());
    } else if (tax.taxType == 3) {
      // Tax table applicable
      if (tax.taxTable.isNotEmpty) {
        tax.taxTable.forEach((elementInTable) {
          if (basePrice >= double.parse(elementInTable.from) && basePrice <= double.parse(elementInTable.to)) {
            itemTax += double.parse(elementInTable.taxApplied);
          }
        });
      } else {
        itemTax = 0.00;
      }
    }
  });
  return itemTax;
}
