import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

void showSnackBar(BuildContext context, String message) {
  final snackBar = SnackBar(content: Text(message), behavior: SnackBarBehavior.floating);
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
