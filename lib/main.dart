import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kiosk/screens/dashboard_screen/viewModel/dashboard_view_model.dart';
import 'package:kiosk/screens/landing_banner_screen/viewModel/landing_screen_view_model.dart';
import 'package:kiosk/screens/login_screen/viewModel/default_login_view_model.dart';
import 'package:kiosk/screens/main_menu_screen/viewModel/main_menu_view_model.dart';
import 'package:kiosk/screens/menu_full/viewModel/full_menu_view_model.dart';
import 'package:kiosk/screens/menu_item_detailed_screen/viewModel/menu_items_detailed_view_model.dart';
import 'package:kiosk/screens/menu_new_items/viewModel/newly_added_view_model.dart';
import 'package:kiosk/screens/menu_top_picks/viewModel/popular_items_view_model.dart';
import 'package:kiosk/screens/payment_options_screen/viewModel/place_order_view_model.dart';
import 'package:kiosk/screens/payment_screen/viewModel/payment_view_model.dart';
import 'package:kiosk/screens/restaurant_login_screen/viewModel/restaurant_login_view_model.dart';
import 'package:kiosk/screens/splash_screen/ui/splash_screen.dart';
import 'package:kiosk/screens/suggestions_screen/viewModel/suggestions_view_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:kiosk/viewModel/device_setup_view_model.dart';
import 'package:provider/provider.dart';
import 'package:bot_toast/bot_toast.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => RestaurantLoginViewModel()),
        ChangeNotifierProvider(create: (context) => DeviceSetupViewModel()),
        ChangeNotifierProvider(create: (context) => LandingScreenViewModel()),
        ChangeNotifierProvider(create: (context) => DashBoardViewModel()),
        ChangeNotifierProvider(create: (context) => FullMenuViewModel()),
        ChangeNotifierProvider(create: (context) => MainMenuViewModel()),
        ChangeNotifierProvider(create: (context) => MenuGroupItemsViewModel()),
        ChangeNotifierProvider(create: (context) => NewlyAddedItemsViewModel()),
        ChangeNotifierProvider(create: (context) => PopularItemsViewModel()),
        ChangeNotifierProvider(create: (context) => SuggestionsViewModel()),
        ChangeNotifierProvider(create: (context) => PaymentViewModel()),
        ChangeNotifierProvider(create: (context) => PlaceOrderViewModel()),
        ChangeNotifierProvider(create: (context) => CartViewModel()),
        ChangeNotifierProvider(create: (context) => DefaultLoginViewModel()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: app_name,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        builder: BotToastInit(),
        navigatorObservers: [BotToastNavigatorObserver()],
        home: SplashScreen(),
      ),
    );
  }
}
