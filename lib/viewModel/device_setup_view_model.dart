import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kiosk/models/add_kiosk_model.dart';
import 'package:kiosk/models/basic_response.dart';
import 'package:kiosk/models/service_areas_model.dart';
import 'package:kiosk/services/repositories.dart';

class DeviceSetupViewModel with ChangeNotifier {
  String deviceName = "", serviceAreaId = "";
  bool _isFetching = false, _isHavingData = false;
  BasicResponse? _deviceNameResponse;
  ServiceAreasModel? _serviceAreasModel;
  AddActiveKioskModel? _activeKioskModel;

  String? get getDeviceName {
    return deviceName;
  }

  String? get getServiceAreaId {
    return serviceAreaId;
  }

  BasicResponse? get deviceNameResponse {
    return _deviceNameResponse;
  }

  ServiceAreasModel? get serviceAreasModel {
    return _serviceAreasModel;
  }

  AddActiveKioskModel? get activeKioskModel {
    return _activeKioskModel;
  }

  void saveDeviceName(String name) {
    deviceName = name;
    notifyListeners();
  }

  void saveServiceArea(String areaId) {
    serviceAreaId = areaId;
    notifyListeners();
  }

  Future<BasicResponse?> checkDeviceNameApi(String restaurantId, String deviceName) async {
    _isFetching = true;
    _isHavingData = false;
    _deviceNameResponse = BasicResponse(result: '', responseStatus: 0);
    notifyListeners();
    try {
      dynamic response = await Repository().checkDeviceName(restaurantId, deviceName);
      if (response != null) {
        _deviceNameResponse = BasicResponse.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("deviceNameResponse $_isFetching $_isHavingData ${_deviceNameResponse!.responseStatus}");
    notifyListeners();
    return _deviceNameResponse;
  }

  Future<ServiceAreasModel?> getServiceAreasApi(String restaurantId) async {
    _isFetching = true;
    _isHavingData = false;
    _serviceAreasModel = ServiceAreasModel(result: '', responseStatus: 0, serviceAreasList: null);
    notifyListeners();
    try {
      dynamic response = await Repository().getServiceAreas(restaurantId);
      if (response != null) {
        _serviceAreasModel = ServiceAreasModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_serviceAreasModel $_isFetching $_isHavingData ${_serviceAreasModel!.responseStatus}");
    notifyListeners();
    return _serviceAreasModel;
  }

  Future<AddActiveKioskModel?> addKioskDevice(
    String restaurantId,
    String deviceName,
    String deviceToken,
    String serviceAreaId,
  ) async {
    _isFetching = true;
    _isHavingData = false;
    _activeKioskModel = AddActiveKioskModel(result: '', responseStatus: 0, deviceId: '');
    notifyListeners();
    try {
      print("pppp $restaurantId, $deviceName, $deviceToken, $serviceAreaId");
      dynamic response = await Repository().addKioskDevice(restaurantId, deviceName, deviceToken, serviceAreaId);
      if (response != null) {
        _activeKioskModel = AddActiveKioskModel.fromJson(response);
        _isHavingData = true;
      } else {
        _isHavingData = false;
      }
    } catch (e) {
      _isHavingData = false;
      print(e);
    }
    _isFetching = false;
    print("_activeKioskModel $_isFetching $_isHavingData ${_activeKioskModel!.responseStatus}");
    notifyListeners();
    return _activeKioskModel;
  }
}
