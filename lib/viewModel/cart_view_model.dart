import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/cart_model.dart';

class CartViewModel with ChangeNotifier {
  List<OrderItems> cartList = [];

  void addItemToCart(OrderItems item) {
    cartList.add(item);
    notifyListeners();
  }

  void clearCart() {
    cartList = [];
    notifyListeners();
  }

  void updateCartItem(int position, OrderItems item) {
    cartList[position] = item;
    notifyListeners();
  }

  void removeItem(int position) {
    cartList.removeAt(position);
    notifyListeners();
  }

  void clearAndAddCart(List<OrderItems> newCartList) {
    cartList = newCartList;
    notifyListeners();
  }

  List<OrderItems> get getCartList {
    return cartList;
  }

  int get getQuantities {
    int qty = 0;
    cartList.forEach((element) {
      qty += element.quantity;
    });
    return qty;
  }

  double get getSubTotal {
    double subTotal = 0.00;
    cartList.forEach((element) {
      if (element.modifiersList.isNotEmpty) {
        subTotal += (element.totalPrice * element.quantity);
        element.modifiersList.forEach((modifier) {
          subTotal += double.parse(modifier.price);
        });
      } else {
        subTotal += (element.totalPrice * element.quantity);
      }
    });
    return subTotal;
  }

  double get getTotalTaxAmount {
    double taxAmount = 0.00;
    cartList.forEach((element) {
      taxAmount += element.itemTax;
    });
    return taxAmount;
  }

  double get getFinalTotal {
    double finalTotal = 0.00;
    finalTotal = getSubTotal + getTotalTaxAmount;
    return finalTotal;
  }
}
