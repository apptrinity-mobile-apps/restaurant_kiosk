class SuggestionModel {
  late String restaurantId;
  late String menuGroupId;
  late List<String>? itemIdsList;

  SuggestionModel({required this.restaurantId, required this.menuGroupId, required this.itemIdsList});

  SuggestionModel.fromJson(Map<String, dynamic> json) {
    restaurantId = json['restaurantId'];
    menuGroupId = json['menuGroupId'];
    itemIdsList = json['itemIdsList'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['restaurantId'] = this.restaurantId;
    data['menuGroupId'] = this.menuGroupId;
    data['itemIdsList'] = this.itemIdsList;
    return data;
  }
}
