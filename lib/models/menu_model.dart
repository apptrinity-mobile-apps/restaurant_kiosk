class MenuModel {
  late List<MenusList>? menusList;
  late int responseStatus;
  late String result;

  MenuModel({required this.menusList, required this.responseStatus, required this.result});

  MenuModel.fromJson(Map<String, dynamic> json) {
    if (json['menus_list'] != null) {
      menusList = [];
      json['menus_list'].forEach((v) {
        menusList!.add(new MenusList.fromJson(v));
      });
    } else {
      menusList = [];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['menus_list'] = this.menusList!.map((v) => v.toJson()).toList();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class MenusList {
  late String id;
  late String menuImage;
  late String menuName;
  late String posName;
  late bool taxInclude;

  MenusList({required this.id, required this.menuImage, required this.menuName, required this.posName, required this.taxInclude});

  MenusList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    menuImage = json['menuImage'] == null ? "" : json['menuImage'];
    menuName = json['menuName'] == null ? "" : json['menuName'];
    posName = json['posName'] == null ? "" : json['posName'];
    taxInclude = json['taxInclude'] == null ? "" : json['taxInclude'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['menuImage'] = this.menuImage;
    data['menuName'] = this.menuName;
    data['posName'] = this.posName;
    data['taxInclude'] = this.taxInclude;
    return data;
  }
}
