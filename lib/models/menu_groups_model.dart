class MenuGroupsModel {
  late List<MenuGroupsList>? menuGroupsList;
  late int responseStatus;
  late String result;

  MenuGroupsModel({required this.menuGroupsList, required this.responseStatus, required this.result});

  MenuGroupsModel.fromJson(Map<String, dynamic> json) {
    if (json['menuGroupsList'] != null) {
      menuGroupsList = [];
      json['menuGroupsList'].forEach((v) {
        menuGroupsList!.add(new MenuGroupsList.fromJson(v));
      });
    } else {
      menuGroupsList = [];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['menuGroupsList'] = this.menuGroupsList!.map((v) => v.toJson()).toList();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class MenuGroupsList {
  late String basePrice;
  late String createdBy;
  late String createdById;
  late String createdOn;
  late String id;
  late String name;
  late String groupImage;
  late int orderValue;
  late int pricingStrategy;
  late String restaurantId;
  late List<SizeList> sizeList;
  late int status;
  late List<TimePriceList> timePriceList;
  late String uniqueNumber;

  MenuGroupsList(
      {required this.basePrice,
      required this.createdBy,
      required this.createdById,
      required this.createdOn,
      required this.id,
      required this.name,
      required this.orderValue,
      required this.pricingStrategy,
      required this.restaurantId,
      required this.sizeList,
      required this.status,
      required this.timePriceList,
      required this.uniqueNumber});

  MenuGroupsList.fromJson(Map<String, dynamic> json) {
    basePrice = json['basePrice'] == null ? "" : json['basePrice'];
    createdBy = json['createdBy'] == null ? "" : json['createdBy'];
    createdById = json['createdById'] == null ? "" : json['createdById'];
    createdOn = json['createdOn'] == null ? "" : json['createdOn'];
    id = json['id'];
    name = json['name'];
    groupImage = json['groupImage'] == null ? "" : json['groupImage'];
    orderValue = json['orderValue'] == null ? 0 : json['orderValue'];
    pricingStrategy = json['pricingStrategy'] == null ? 0 : json['pricingStrategy'];
    restaurantId = json['restaurantId'];
    if (json['sizeList'] != null) {
      sizeList = [];
      json['sizeList'].forEach((v) {
        sizeList.add(new SizeList.fromJson(v));
      });
    } else {
      sizeList = [];
    }
    status = json['status'];
    if (json['timePriceList'] != null) {
      timePriceList = [];
      json['timePriceList'].forEach((v) {
        timePriceList.add(new TimePriceList.fromJson(v));
      });
    } else {
      timePriceList = [];
    }
    uniqueNumber = json['uniqueNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basePrice'] = this.basePrice;
    data['createdBy'] = this.createdBy;
    data['createdById'] = this.createdById;
    data['createdOn'] = this.createdOn;
    data['id'] = this.id;
    data['name'] = this.name;
    data['groupImage'] = this.groupImage;
    data['orderValue'] = this.orderValue;
    data['pricingStrategy'] = this.pricingStrategy;
    data['restaurantId'] = this.restaurantId;
    data['sizeList'] = this.sizeList.map((v) => v.toJson()).toList();
    data['status'] = this.status;
    data['timePriceList'] = this.timePriceList.map((v) => v.toJson()).toList();
    data['uniqueNumber'] = this.uniqueNumber;
    return data;
  }
}

class SizeList {
  late double price;
  late String sizeName;

  SizeList({required this.price, required this.sizeName});

  SizeList.fromJson(Map<String, dynamic> json) {
    price = json['price'] == null ? 0.00 : json['price'];
    sizeName = json['sizeName'] == null ? "" : json['sizeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['sizeName'] = this.sizeName;
    return data;
  }
}

class TimePriceList {
  late List<String> days;
  late String price;
  late String timeFrom;
  late String timeTo;

  TimePriceList({required this.days, required this.price, required this.timeFrom, required this.timeTo});

  TimePriceList.fromJson(Map<String, dynamic> json) {
    days = json['days'].cast<String>();
    price = json['price'] == null ? "" : json['price'];
    timeFrom = json['timeFrom'] == null ? "" : json['timeFrom'];
    timeTo = json['timeTo'] == null ? "" : json['timeTo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['days'] = this.days;
    data['price'] = this.price;
    data['timeFrom'] = this.timeFrom;
    data['timeTo'] = this.timeTo;
    return data;
  }
}
