class AddActiveKioskModel {
  String? deviceId;
  int? responseStatus;
  String? result;

  AddActiveKioskModel({this.deviceId, this.responseStatus, this.result});

  AddActiveKioskModel.fromJson(Map<String, dynamic> json) {
    deviceId = json['deviceId'];
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deviceId'] = this.deviceId;
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}
