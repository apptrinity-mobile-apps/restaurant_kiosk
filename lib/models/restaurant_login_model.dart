class RestaurantLoginModel {
  late String OTP;
  late int responseStatus;
  late String restaurantId;
  late String restaurantName;
  late String restaurantLogo;
  late String result;

  RestaurantLoginModel(
      {required this.OTP,
      required this.responseStatus,
      required this.restaurantId,
      required this.restaurantName,
      required this.restaurantLogo,
      required this.result});

  RestaurantLoginModel.fromJson(Map<String, dynamic> json) {
    OTP = json['OTP'] == null ? "" : json['OTP'];
    responseStatus = json['responseStatus'];
    restaurantId = json['restaurantId'] == null ? "" : json['restaurantId'];
    restaurantName = json['restaurantName'] == null ? "" : json['restaurantName'];
    restaurantLogo = json['restaurantLogo'] == null ? "" : json['restaurantLogo'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['OTP'] = this.OTP;
    data['responseStatus'] = this.responseStatus;
    data['restaurantId'] = this.restaurantId;
    data['restaurantName'] = this.restaurantName;
    data['restaurantLogo'] = this.restaurantLogo;
    data['result'] = this.result;
    return data;
  }
}
