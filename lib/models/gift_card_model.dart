class GiftCardModel {
  late GiftCard? giftCard;
  late int responseStatus;
  late String result;

  GiftCardModel({required this.giftCard, required this.responseStatus, required this.result});

  GiftCardModel.fromJson(Map<String, dynamic> json) {
    giftCard = json['giftCard'] != null ? new GiftCard.fromJson(json['giftCard']) : null;
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['giftCard'] = this.giftCard!.toJson();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class GiftCard {
  late String amount;
  late String cardType;
  late String createdOn;
  late String email;
  late String name;

  GiftCard({required this.amount, required this.cardType, required this.createdOn, required this.email, required this.name});

  GiftCard.fromJson(Map<String, dynamic> json) {
    amount = json['amount'] == null ? "" : json['amount'];
    cardType = json['cardType'] == null ? "" : json['cardType'];
    createdOn = json['createdOn'] == null ? "" : json['createdOn'];
    email = json['email'] == null ? "" : json['email'];
    name = json['name'] == null ? "" : json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['amount'] = this.amount;
    data['cardType'] = this.cardType;
    data['createdOn'] = this.createdOn;
    data['email'] = this.email;
    data['name'] = this.name;
    return data;
  }
}
