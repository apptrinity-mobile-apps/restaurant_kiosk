class HomeBanner {
  late List<HomeBannersList>? homeBannersList;
  late int responseStatus;
  late String result;

  HomeBanner({required this.homeBannersList, required this.responseStatus, required this.result});

  HomeBanner.fromJson(Map<String, dynamic> json) {
    if (json['homeBannersList'] != null) {
      homeBannersList = [];
      json['homeBannersList'].forEach((v) {
        homeBannersList!.add(new HomeBannersList.fromJson(v));
      });
    } else {
      homeBannersList = [];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['homeBannersList'] = this.homeBannersList!.map((v) => v.toJson()).toList();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class HomeBannersList {
  late String createdOn;
  late String id;
  late String image;
  late String restaurantId;
  late int status;
  late String title;

  HomeBannersList(
      {required this.createdOn, required this.id, required this.image, required this.restaurantId, required this.status, required this.title});

  HomeBannersList.fromJson(Map<String, dynamic> json) {
    createdOn = json['createdOn'];
    id = json['id'];
    image = json['image'] == null ? "" : json['image'];
    restaurantId = json['restaurantId'];
    status = json['status'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['createdOn'] = this.createdOn;
    data['id'] = this.id;
    data['image'] = this.image;
    data['restaurantId'] = this.restaurantId;
    data['status'] = this.status;
    data['title'] = this.title;
    return data;
  }
}
