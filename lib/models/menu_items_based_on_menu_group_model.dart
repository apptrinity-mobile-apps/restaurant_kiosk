class MenuItemsModel {
  late List<ItemsList>? itemsList;
  late int responseStatus;
  late String result;

  MenuItemsModel({required this.itemsList, required this.responseStatus, required this.result});

  MenuItemsModel.fromJson(Map<String, dynamic> json) {
    if (json['items_list'] != null) {
      itemsList = [];
      json['items_list'].forEach((v) {
        itemsList!.add(new ItemsList.fromJson(v));
      });
    } else {
      itemsList = [];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['items_list'] = this.itemsList!.map((v) => v.toJson()).toList();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class ItemsList {
  late double basePrice;
  late int defaultQty;
  late String id;
  late String menuId;
  late String menuGroupId;
  late bool inheritDiningOptionTax;
  late bool inheritTaxInclude;
  late bool inheritTaxRate;
  late String itemImage;
  late int maxLimit;
  late List<MenuPriceList> menuPriceList;
  late List<ModifiersList> modifiersList;
  late String name;
  late String posName;
  late int pricingStrategy;
  late List<SizeList> sizeList;
  late List<SuggestibleItems> suggestibleItems;
  late bool taxIncludeOption;
  late List<Taxrates> taxrates;
  late List<TimePriceList> timePriceList;

  ItemsList(
      {required this.basePrice,
      required this.defaultQty,
      required this.id,
      required this.inheritDiningOptionTax,
      required this.inheritTaxInclude,
      required this.inheritTaxRate,
      required this.itemImage,
      required this.maxLimit,
      required this.menuPriceList,
      required this.modifiersList,
      required this.name,
      required this.posName,
      required this.pricingStrategy,
      required this.sizeList,
      required this.suggestibleItems,
      required this.taxIncludeOption,
      required this.taxrates,
      required this.timePriceList});

  ItemsList.fromJson(Map<String, dynamic> json) {
    basePrice = json['basePrice'] == null ? 0.00 : json['basePrice'];
    defaultQty = json['defaultQty'] == null ? 0 : json['defaultQty'];
    id = json['id'] == null ? "" : json['id'];
    menuId = json['menuId'] == null ? "" : json['menuId'];
    menuGroupId = json['menuGroupId'] == null ? "" : json['menuGroupId'];
    inheritDiningOptionTax = json['inheritDiningOptionTax'] == null ? false : json['inheritDiningOptionTax'];
    inheritTaxInclude = json['inheritTaxInclude'] == null ? false : json['inheritTaxInclude'];
    inheritTaxRate = json['inheritTaxRate'] == null ? false : json['inheritTaxRate'];
    itemImage = json['itemImage'] == null ? "" : json['itemImage'];
    maxLimit = json['maxLimit'] == null ? 0 : json['maxLimit'];
    if (json['menuPriceList'] != null) {
      menuPriceList = [];
      json['menuPriceList'].forEach((v) {
        menuPriceList.add(new MenuPriceList.fromJson(v));
      });
    } else {
      menuPriceList = [];
    }
    if (json['modifiersList'] != null) {
      modifiersList = [];
      json['modifiersList'].forEach((v) {
        modifiersList.add(new ModifiersList.fromJson(v));
      });
    } else {
      modifiersList = [];
    }
    name = json['name'] == null ? "" : json['name'];
    posName = json['posName'] == null ? "" : json['posName'];
    pricingStrategy = json['pricingStrategy'] == null ? 0 : json['pricingStrategy'];
    if (json['sizeList'] != null) {
      sizeList = [];
      json['sizeList'].forEach((v) {
        sizeList.add(new SizeList.fromJson(v));
      });
    } else {
      sizeList = [];
    }
    if (json['suggestibleItems'] != null) {
      suggestibleItems = [];
      json['suggestibleItems'].forEach((v) {
        suggestibleItems.add(new SuggestibleItems.fromJson(v));
      });
    } else {
      suggestibleItems = [];
    }
    taxIncludeOption = json['taxIncludeOption'] == null ? false : json['taxIncludeOption'];
    if (json['taxrates'] != null) {
      taxrates = [];
      json['taxrates'].forEach((v) {
        taxrates.add(new Taxrates.fromJson(v));
      });
    } else {
      taxrates = [];
    }
    if (json['timePriceList'] != null) {
      timePriceList = [];
      json['timePriceList'].forEach((v) {
        timePriceList.add(new TimePriceList.fromJson(v));
      });
    } else {
      timePriceList = [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['basePrice'] = this.basePrice;
    data['defaultQty'] = this.defaultQty;
    data['id'] = this.id;
    data['menuId'] = this.menuId;
    data['menuGroupId'] = this.menuGroupId;
    data['inheritDiningOptionTax'] = this.inheritDiningOptionTax;
    data['inheritTaxInclude'] = this.inheritTaxInclude;
    data['inheritTaxRate'] = this.inheritTaxRate;
    data['itemImage'] = this.itemImage;
    data['maxLimit'] = this.maxLimit;
    data['menuPriceList'] = this.menuPriceList.map((v) => v.toJson()).toList();
    data['modifiersList'] = this.modifiersList.map((v) => v.toJson()).toList();
    data['name'] = this.name;
    data['posName'] = this.posName;
    data['pricingStrategy'] = this.pricingStrategy;
    data['sizeList'] = this.sizeList.map((v) => v.toJson()).toList();
    data['suggestibleItems'] = this.suggestibleItems.map((v) => v.toJson()).toList();
    data['taxIncludeOption'] = this.taxIncludeOption;
    data['taxrates'] = this.taxrates.map((v) => v.toJson()).toList();
    data['timePriceList'] = this.timePriceList.map((v) => v.toJson()).toList();
    return data;
  }
}

class MenuPriceList {
  late String menuName;
  late double price;

  MenuPriceList({required this.menuName, required this.price});

  MenuPriceList.fromJson(Map<String, dynamic> json) {
    menuName = json['menuName'] == null ? "" : json['menuName'];
    price = json['price'] == null ? 0.00 : json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['menuName'] = this.menuName;
    data['price'] = this.price;
    return data;
  }
}

class ModifiersList {
  late bool existingMenuGroup;
  late String id;
  late int isRequired;
  late int maxSelections;
  late int minSelections;
  late bool multipleSelect;
  late String name;
  late List<OptionsList> optionsList;
  late String posName;
  late int pricing;
  late String sortOrder;

  ModifiersList(
      {required this.existingMenuGroup,
      required this.id,
      required this.isRequired,
      required this.maxSelections,
      required this.minSelections,
      required this.multipleSelect,
      required this.name,
      required this.optionsList,
      required this.posName,
      required this.pricing,
      required this.sortOrder});

  ModifiersList.fromJson(Map<String, dynamic> json) {
    existingMenuGroup = json['existingMenuGroup'] == null ? false : json['existingMenuGroup'];
    id = json['id'] == null ? "" : json['id'];
    isRequired = json['isRequired'] == null ? 0 : json['isRequired'];
    maxSelections = json['maxSelections'] == null ? -1 : json['maxSelections'];
    minSelections = json['minSelections'] == null ? -1 : json['minSelections'];
    multipleSelect = json['multipleSelect'] == null ? false : json['multipleSelect'];
    name = json['name'] == null ? "" : json['name'];
    if (json['optionsList'] != null) {
      optionsList = [];
      json['optionsList'].forEach((v) {
        optionsList.add(new OptionsList.fromJson(v));
      });
    } else {
      optionsList = [];
    }
    posName = json['posName'] == null ? "" : json['posName'];
    pricing = json['pricing'] == null ? 0 : json['pricing'];
    sortOrder = json['sortOrder'] == null ? "" : json['sortOrder'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['existingMenuGroup'] = this.existingMenuGroup;
    data['id'] = this.id;
    data['isRequired'] = this.isRequired;
    data['maxSelections'] = this.maxSelections;
    data['minSelections'] = this.minSelections;
    data['multipleSelect'] = this.multipleSelect;
    data['name'] = this.name;
    data['optionsList'] = this.optionsList.map((v) => v.toJson()).toList();
    data['posName'] = this.posName;
    data['pricing'] = this.pricing;
    data['sortOrder'] = this.sortOrder;
    return data;
  }
}

class OptionsList {
  late String id;
  late String modifierImage;
  late String optionName;
  late String price;

  OptionsList({required this.id, required this.modifierImage, required this.optionName, required this.price});

  OptionsList.fromJson(Map<String, dynamic> json) {
    id = json['id'] == null ? "" : json['id'];
    modifierImage = json['modifierImage'] == null ? "" : json['modifierImage'];
    optionName = json['optionName'] == null ? "" : json['optionName'];
    price = json['price'] == null ? "" : json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['optionName'] = this.optionName;
    data['modifierImage'] = this.modifierImage;
    data['price'] = this.price;
    return data;
  }
}

class SizeList {
  late double price;
  late String sizeName;

  SizeList({required this.price, required this.sizeName});

  SizeList.fromJson(Map<String, dynamic> json) {
    price = json['price'] == null ? 0.00 : json['price'];
    sizeName = json['sizeName'] == null ? "" : json['sizeName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this.price;
    data['sizeName'] = this.sizeName;
    return data;
  }
}

class SuggestibleItems {
  late String menuGroupDescription;
  late String menuGroupId;
  late String menuGroupImage;
  late String menuGroupName;
  late List<ItemsList> suggestibleItems;

  SuggestibleItems(
      {required this.menuGroupDescription,
      required this.menuGroupId,
      required this.menuGroupImage,
      required this.menuGroupName,
      required this.suggestibleItems});

  SuggestibleItems.fromJson(Map<String, dynamic> json) {
    menuGroupDescription = json['menuGroupDescription'] == null ? "" : json['menuGroupDescription'];
    menuGroupId = json['menuGroupId'] == null ? "" : json['menuGroupId'];
    menuGroupImage = json['menuGroupImage'] == null ? "" : json['menuGroupImage'];
    menuGroupName = json['menuGroupName'] == null ? "" : json['menuGroupName'];
    if (json['suggestible_items'] != null) {
      suggestibleItems = [];
      json['suggestible_items'].forEach((v) {
        suggestibleItems.add(new ItemsList.fromJson(v));
      });
    } else {
      suggestibleItems = [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['menuGroupDescription'] = this.menuGroupDescription;
    data['menuGroupId'] = this.menuGroupId;
    data['menuGroupImage'] = this.menuGroupImage;
    data['menuGroupName'] = this.menuGroupName;
    data['suggestible_items'] = this.suggestibleItems.map((v) => v.toJson()).toList();
    return data;
  }
}

class Taxrates {
  late String sId;
  late String createdBy;
  late bool mDefault;
  late bool enableTakeOutRate;
  late String importId;
  late double orderValue;
  late String restaurantId;
  late int roundingOptions;
  late int status;
  late double takeOutTaxRate;
  late String taxName;
  late double taxRate;
  late List<TaxTable> taxTable;
  late int taxType;
  late int uniqueNumber;
  late String updatedBy;

  Taxrates(
      {required this.sId,
      required this.createdBy,
      required this.mDefault,
      required this.enableTakeOutRate,
      required this.importId,
      required this.orderValue,
      required this.restaurantId,
      required this.roundingOptions,
      required this.status,
      required this.takeOutTaxRate,
      required this.taxName,
      required this.taxRate,
      required this.taxTable,
      required this.taxType,
      required this.uniqueNumber,
      required this.updatedBy});

  Taxrates.fromJson(Map<String, dynamic> json) {
    sId = json['_id'] == null ? "" : json['_id'];
    createdBy = json['createdBy'];
    mDefault = json['default'] == null ? false : json['default'];
    enableTakeOutRate = json['enableTakeOutRate'] == null ? false : json['enableTakeOutRate'];
    importId = json['importId'] == null ? "" : json['importId'];
    orderValue = json['orderValue'] == null ? 0.00 : double.parse(json['orderValue'].toString());
    restaurantId = json['restaurantId'];
    roundingOptions = json['roundingOptions'] == null ? 0 : json['roundingOptions'];
    status = json['status'];
    takeOutTaxRate = json['takeOutTaxRate'] == null ? 0.00 : double.parse(json['takeOutTaxRate'].toString());
    taxName = json['taxName'] == null ? "" : json['taxName'];
    taxRate = json['taxRate'] == null ? 0.00 : double.parse(json['taxRate'].toString());
    if (json['taxTable'] != null) {
      taxTable = [];
      json['taxTable'].forEach((v) {
        taxTable.add(new TaxTable.fromJson(v));
      });
    } else {
      taxTable = [];
    }
    taxType = json['taxType'] == null ? 0 : json['taxType'];
    uniqueNumber = json['uniqueNumber'] == null ? 0 : json['uniqueNumber'];
    updatedBy = json['updatedBy'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['createdBy'] = this.createdBy;
    data['default'] = this.mDefault;
    data['enableTakeOutRate'] = this.enableTakeOutRate;
    data['importId'] = this.importId;
    data['orderValue'] = this.orderValue;
    data['restaurantId'] = this.restaurantId;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['takeOutTaxRate'] = this.takeOutTaxRate;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    data['taxType'] = this.taxType;
    data['uniqueNumber'] = this.uniqueNumber;
    data['updatedBy'] = this.updatedBy;
    return data;
  }
}

class TaxTable {
  late String from;
  late double priceDifference;
  late bool repeat;
  late String taxApplied;
  late String to;

  TaxTable({required this.from, required this.priceDifference, required this.repeat, required this.taxApplied, required this.to});

  TaxTable.fromJson(Map<String, dynamic> json) {
    from = json['from'] == null ? "" : json['from'];
    priceDifference = json['priceDifference'] == null ? 0.00 : double.parse(json['priceDifference'].toString());
    repeat = json['repeat'] == null ? false : json['repeat'];
    taxApplied = json['taxApplied'] == null ? "" : json['taxApplied'];
    to = json['to'] == null ? "" : json['to'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['from'] = this.from;
    data['priceDifference'] = this.priceDifference;
    data['repeat'] = this.repeat;
    data['taxApplied'] = this.taxApplied;
    data['to'] = this.to;
    return data;
  }
}

class TimePriceList {
  late List<String> days;
  late double price;
  late String timeFrom;
  late String timeTo;

  TimePriceList({required this.days, required this.price, required this.timeFrom, required this.timeTo});

  TimePriceList.fromJson(Map<String, dynamic> json) {
    days = json['days'].cast<String>();
    price = json['price'] == null ? 0.00 : json['price'];
    timeFrom = json['timeFrom'] == null ? "" : json['timeFrom'];
    timeTo = json['timeTo'] == null ? "" : json['timeTo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['days'] = this.days;
    data['price'] = this.price;
    data['timeFrom'] = this.timeFrom;
    data['timeTo'] = this.timeTo;
    return data;
  }
}
