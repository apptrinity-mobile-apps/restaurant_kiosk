class DinningOptionsModel {
  late List<DineInList>? dineInList;
  late int responseStatus;
  late String result;

  DinningOptionsModel({required this.dineInList, required this.responseStatus, required this.result});

  DinningOptionsModel.fromJson(Map<String, dynamic> json) {
    if (json['dineInList'] != null) {
      dineInList = [];
      json['dineInList'].forEach((v) {
        dineInList!.add(new DineInList.fromJson(v));
      });
    } else {
      dineInList = [];
    }
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dineInList'] = this.dineInList!.map((v) => v.toJson()).toList();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}

class DineInList {
  late String behavior;
  late String id;
  late String optionName;

  DineInList({required this.behavior, required this.id, required this.optionName});

  DineInList.fromJson(Map<String, dynamic> json) {
    behavior = json['behavior'];
    id = json['id'];
    optionName = json['optionName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['behavior'] = this.behavior;
    data['id'] = this.id;
    data['optionName'] = this.optionName;
    return data;
  }
}
