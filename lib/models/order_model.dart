class OrderModel {
  late String orderId;
  late String orderUniqueId;
  late int responseStatus;
  late String result;

  OrderModel({required this.orderId, required this.orderUniqueId, required this.responseStatus, required this.result});

  OrderModel.fromJson(Map<String, dynamic> json) {
    orderId = json['orderId'] == null ? "" : json['orderId'];
    orderUniqueId = json['orderUniqueId'] == null ? "" : json['orderUniqueId'];
    responseStatus = json['responseStatus'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderId'] = this.orderId;
    data['orderUniqueId'] = this.orderUniqueId;
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    return data;
  }
}
