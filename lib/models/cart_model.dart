import 'package:kiosk/models/menu_items_based_on_menu_group_model.dart';

class CartModel {
  late String restaurantId;
  late String employeeId;
  late String dineInOptionId;
  late String dineInOptionName;
  late String dineInBehaviour;
  late List<OrderItems>? orderItems;
  late List<PaymentDetails>? paymentDetails;
  late double subTotal;
  late double taxAmount;
  late double totalAmount;
  late CustomerInfo customerInfo;
  late int tipAmount;
  late int creditsUsed;
  late double discountAmount;
  late double serviceChargesAmount;
  late String deviceId;

  CartModel(
      {required this.restaurantId,
      required this.employeeId,
      required this.dineInOptionId,
      required this.dineInOptionName,
      required this.dineInBehaviour,
      required this.orderItems,
      required this.paymentDetails,
      required this.subTotal,
      required this.taxAmount,
      required this.totalAmount,
      required this.customerInfo,
      required this.tipAmount,
      required this.creditsUsed,
      required this.discountAmount,
      required this.serviceChargesAmount,
      required this.deviceId});

  CartModel.fromJson(Map<String, dynamic> json) {
    restaurantId = json['restaurantId'];
    employeeId = json['employeeId'];
    dineInOptionId = json['dineInOptionId'];
    dineInOptionName = json['dineInOptionName'];
    dineInBehaviour = json['dineInBehaviour'];
    if (json['orderItems'] != null) {
      orderItems = [];
      json['orderItems'].forEach((v) {
        orderItems!.add(new OrderItems.fromJson(v));
      });
    } else {
      orderItems = [];
    }
    if (json['paymentDetails'] != null) {
      paymentDetails = [];
      json['paymentDetails'].forEach((v) {
        paymentDetails!.add(new PaymentDetails.fromJson(v));
      });
    } else {
      paymentDetails = [];
    }
    subTotal = json['subTotal'];
    taxAmount = json['taxAmount'];
    totalAmount = json['totalAmount'];
    customerInfo = (json['customerInfo'] != null ? new CustomerInfo.fromJson(json['customerInfo']) : null)!;
    tipAmount = json['tipAmount'];
    creditsUsed = json['creditsUsed'];
    discountAmount = json['discountAmount'];
    serviceChargesAmount = json['serviceChargesAmount'];
    deviceId = json['deviceId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['restaurantId'] = this.restaurantId;
    data['employeeId'] = this.employeeId;
    data['dineInOptionId'] = this.dineInOptionId;
    data['dineInOptionName'] = this.dineInOptionName;
    data['dineInBehaviour'] = this.dineInBehaviour;
    data['orderItems'] = this.orderItems!.map((v) => v.toJson()).toList();
    data['paymentDetails'] = this.paymentDetails!.map((v) => v.toJson()).toList();
    data['subTotal'] = this.subTotal;
    data['taxAmount'] = this.taxAmount;
    data['totalAmount'] = this.totalAmount;
    data['customerInfo'] = this.customerInfo.toJson();
    data['tipAmount'] = this.tipAmount;
    data['creditsUsed'] = this.creditsUsed;
    data['discountAmount'] = this.discountAmount;
    data['serviceChargesAmount'] = this.serviceChargesAmount;
    data['deviceId'] = this.deviceId;
    return data;
  }
}

class OrderItems {
  late String itemId;
  late String itemDescription;
  late String itemImage;
  late int quantity;
  late double totalPrice;
  late double unitPrice;
  late double itemTax;
  late String itemName;
  late int discountAmount;
  late String menuGroupId;
  late String menuId;
  late bool taxIncludeOption;
  late List<Taxrates> taxrates;
  late List<ModifierList> modifiersList;

  OrderItems(
      {required this.itemId,
      required this.itemDescription,
      required this.itemImage,
      required this.quantity,
      required this.totalPrice,
      required this.unitPrice,
      required this.itemTax,
      required this.itemName,
      required this.discountAmount,
      required this.menuGroupId,
      required this.menuId,
      required this.taxIncludeOption,
      required this.taxrates,
      required this.modifiersList});

  OrderItems.fromJson(Map<String, dynamic> json) {
    itemId = json['itemId'];
    itemDescription = json['itemDescription'];
    itemImage = json['itemImage'];
    quantity = json['quantity'];
    totalPrice = json['totalPrice'];
    unitPrice = json['unitPrice'];
    itemName = json['itemName'];
    itemTax = json['itemTax'] == null ? 0.0 : json['itemTax'];
    discountAmount = json['discountAmount'];
    menuGroupId = json['menuGroupId'];
    menuId = json['menuId'];
    if (json['modifiersList'] != null) {
      modifiersList = [];
      json['modifiersList'].forEach((v) {
        modifiersList.add(new ModifierList.fromJson(v));
      });
    } else {
      modifiersList = [];
    }
    taxIncludeOption = json['taxIncludeOption'] == null ? false : json['taxIncludeOption'];
    if (json['taxrates'] != null) {
      taxrates = [];
      json['taxrates'].forEach((v) {
        taxrates.add(new Taxrates.fromJson(v));
      });
    } else {
      taxrates = [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['itemId'] = this.itemId;
    data['itemDescription'] = this.itemDescription;
    data['itemImage'] = this.itemImage;
    data['quantity'] = this.quantity;
    data['totalPrice'] = this.totalPrice;
    data['unitPrice'] = this.unitPrice;
    data['itemName'] = this.itemName;
    data['discountAmount'] = this.discountAmount;
    data['menuGroupId'] = this.menuGroupId;
    data['menuId'] = this.menuId;
    data['modifiersList'] = this.modifiersList.map((v) => v.toJson()).toList();
    return data;
  }
}

class ModifierList {
  late String id;
  late String optionName;
  late String price;

  ModifierList({required this.id, required this.optionName, required this.price});

  ModifierList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    optionName = json['optionName'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['optionName'] = this.optionName;
    data['price'] = this.price;
    return data;
  }
}

class PaymentDetails {
  late int paymentType;
  late double amount;
  late String cardToken;
  late String expiry;
  late String sourceLast4;
  late String giftCardNumber;

  PaymentDetails(
      {required this.paymentType,
      required this.amount,
      required this.cardToken,
      required this.expiry,
      required this.sourceLast4,
      required this.giftCardNumber});

  PaymentDetails.fromJson(Map<String, dynamic> json) {
    paymentType = json['paymentType'];
    amount = json['amount'];
    cardToken = json['cardToken'];
    expiry = json['expiry'];
    sourceLast4 = json['sourceLast4'];
    giftCardNumber = json['giftCardNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['paymentType'] = this.paymentType;
    data['amount'] = this.amount;
    data['cardToken'] = this.cardToken;
    data['expiry'] = this.expiry;
    data['sourceLast4'] = this.sourceLast4;
    data['giftCardNumber'] = this.giftCardNumber;
    return data;
  }
}

class CustomerInfo {
  late String name;
  late String phoneNumber;
  late String email;

  CustomerInfo({required this.name, required this.phoneNumber, required this.email});

  CustomerInfo.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phoneNumber = json['phoneNumber'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phoneNumber'] = this.phoneNumber;
    data['email'] = this.email;
    return data;
  }
}

/*class Taxrates {
  late String sId;
  late String createdBy;
  late bool mDefault;
  late bool enableTakeOutRate;
  late String importId;
  late String orderValue;
  late String restaurantId;
  late int roundingOptions;
  late int status;
  late int takeOutTaxRate;
  late String taxName;
  late int taxRate;
  late List<TaxTable> taxTable;
  late int taxType;
  late int uniqueNumber;
  late String updatedBy;

  Taxrates(
      {required this.sId,
        required this.createdBy,
        required this.mDefault,
        required this.enableTakeOutRate,
        required this.importId,
        required this.orderValue,
        required this.restaurantId,
        required this.roundingOptions,
        required this.status,
        required this.takeOutTaxRate,
        required this.taxName,
        required this.taxRate,
        required this.taxTable,
        required this.taxType,
        required this.uniqueNumber,
        required this.updatedBy});

  Taxrates.fromJson(Map<String, dynamic> json) {
    sId = json['_id'] == null ? "" : json['name'];
    createdBy = json['createdBy'];
    mDefault = json['default'] == null ? false : json['default'];
    enableTakeOutRate = json['enableTakeOutRate'] == null ? false : json['enableTakeOutRate'];
    importId = json['importId'] == null ? "" : json['importId'];
    orderValue = json['orderValue'] == null ? "" : json['orderValue'];
    restaurantId = json['restaurantId'];
    roundingOptions = json['roundingOptions'] == null ? 0 : json['roundingOptions'];
    status = json['status'];
    takeOutTaxRate = json['takeOutTaxRate'] == null ? "" : json['takeOutTaxRate'];
    taxName = json['taxName'] == null ? "" : json['taxName'];
    taxRate = json['taxRate'] == null ? 0 : json['taxRate'];
    if (json['taxTable'] != null) {
      taxTable = [];
      json['taxTable'].forEach((v) {
        taxTable.add(new TaxTable.fromJson(v));
      });
    } else {
      taxTable = [];
    }
    taxType = json['taxType'] == null ? 0 : json['taxType'];
    uniqueNumber = json['uniqueNumber'];
    updatedBy = json['updatedBy'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.sId;
    data['createdBy'] = this.createdBy;
    data['default'] = this.mDefault;
    data['enableTakeOutRate'] = this.enableTakeOutRate;
    data['importId'] = this.importId;
    data['orderValue'] = this.orderValue;
    data['restaurantId'] = this.restaurantId;
    data['roundingOptions'] = this.roundingOptions;
    data['status'] = this.status;
    data['takeOutTaxRate'] = this.takeOutTaxRate;
    data['taxName'] = this.taxName;
    data['taxRate'] = this.taxRate;
    data['taxTable'] = this.taxTable.map((v) => v.toJson()).toList();
    data['taxType'] = this.taxType;
    data['uniqueNumber'] = this.uniqueNumber;
    data['updatedBy'] = this.updatedBy;
    return data;
  }
}

class TaxTable {
  late String from;
  late double priceDifference;
  late bool repeat;
  late String taxApplied;
  late String to;

  TaxTable({required this.from, required this.priceDifference, required this.repeat, required this.taxApplied, required this.to});

  TaxTable.fromJson(Map<String, dynamic> json) {
    from = json['from'] == null ? "" : json['from'];
    priceDifference = json['priceDifference'] == null ? 0.00 : double.parse(json['priceDifference'].toString());
    repeat = json['repeat'] == null ? false : json['repeat'];
    taxApplied = json['taxApplied'] == null ? "" : json['taxApplied'];
    to = json['to'] == null ? "" : json['to'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['from'] = this.from;
    data['priceDifference'] = this.priceDifference;
    data['repeat'] = this.repeat;
    data['taxApplied'] = this.taxApplied;
    data['to'] = this.to;
    return data;
  }
}*/
