class ServiceAreasModel {
  int? responseStatus;
  String? result;
  List<ServiceAreasList>? serviceAreasList;

  ServiceAreasModel({this.responseStatus, this.result, this.serviceAreasList});

  ServiceAreasModel.fromJson(Map<String, dynamic> json) {
    responseStatus = json['responseStatus'];
    result = json['result'];
    if (json['serviceAreasList'] != null) {
      serviceAreasList = [];
      json['serviceAreasList'].forEach((v) {
        serviceAreasList!.add(new ServiceAreasList.fromJson(v));
      });
    } else {
      serviceAreasList = [];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['responseStatus'] = this.responseStatus;
    data['result'] = this.result;
    if (this.serviceAreasList != null) {
      data['serviceAreasList'] = this.serviceAreasList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ServiceAreasList {
  String? id;
  String? serviceName;

  ServiceAreasList({this.id, this.serviceName});

  ServiceAreasList.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    serviceName = json['serviceName'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['serviceName'] = this.serviceName;
    return data;
  }
}
