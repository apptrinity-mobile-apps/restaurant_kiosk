import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';

Widget menuItem(BuildContext context) {
  return Stack(
    children: [
      ClipRRect(
        borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
        child: Container(
          padding: EdgeInsets.all(0),
          height: ScreenSize.height(context),
          width: ScreenSize.width(context),
          child: Center(
            child: Image.asset("assets/images/item_bg.png"),
          ),
        ),
      ),
      Positioned(
        bottom: 0,
        right: 0,
        left: 0,
        child: ClipRRect(
          borderRadius: BorderRadius.only(bottomRight: Radius.circular(25), bottomLeft: Radius.circular(25)),
          child: Container(
            color: Colors.white,
            padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
            width: ScreenSize.width(context),
            child: Center(
              child: Text(
                "Chicken to share",
                maxLines: 2,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 16, color: def_color_red, letterSpacing: 0, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
              ),
            ),
          ),
        ),
      ),
      Positioned(
        bottom: 40,
        left: 0,
        right: 0,
        top: 0,
        child: ClipRRect(
          borderRadius: BorderRadius.only(topRight: Radius.circular(25), topLeft: Radius.circular(25)),
          child: Container(
            padding: EdgeInsets.all(20),
            width: ScreenSize.width(context),
            child: Center(
              child: Container(
                child: Image.asset("assets/images/restaurant_logo.png"),
              ),
            ),
          ),
        ),
      )
    ],
  );
}
