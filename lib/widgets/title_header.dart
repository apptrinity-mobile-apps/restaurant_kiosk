import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:kiosk/utils/all_constants.dart';

Widget titleHeaderWidget(String logo) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Container(
        margin: EdgeInsets.fromLTRB(10, 0, 20, 0),
        child: Image.network(
          logo,
          width: 100,
          height: 100,
        ),
      ),
      Container(
        child: RichText(
          // textAlign: TextAlign.center,
          textScaleFactor: 1.5,
          text: TextSpan(
            text: '',
            children: <TextSpan>[
              TextSpan(
                text: touchScreenTo,
                style: TextStyle(fontSize: 28, color: def_color_red, fontFamily: "Palace", fontWeight: FontWeight.w700),
              ),
              TextSpan(
                text: order,
                style: TextStyle(fontSize: 26, color: def_color_red, fontFamily: "Swis721", fontWeight: FontWeight.w700),
              ),
            ],
          ),
        ),
      ),
      Container(
          margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
          child: Image.asset(
            'assets/images/touch_pointer.png',
            width: 100,
            height: 75,
          ))
    ],
  );
}
