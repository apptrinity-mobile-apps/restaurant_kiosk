import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/utils/all_constants.dart';

Widget cartCountButton() {
  return Container(
    height: 50,
    width: 180,
    child: ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child: Container(
          padding: EdgeInsets.fromLTRB(5, 5, 15, 5),
          decoration: BoxDecoration(color: Colors.white),
          child: Row(
            children: [
              Container(
                margin: EdgeInsets.only(right: 10),
                child: CircleAvatar(
                  child: Center(
                    child: Text(
                      "0",
                      style: TextStyle(fontSize: 20, letterSpacing: 0.8, color: Colors.white, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                    ),
                  ),
                  backgroundColor: def_color_red,
                ),
              ),
              Expanded(
                  child: Center(
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "\u0024",
                      style: TextStyle(fontSize: 16, color: def_color_red, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                    ),
                    Text(
                      "0.00",
                      style:
                          TextStyle(fontSize: 20, color: def_color_red, letterSpacing: 0.9, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w800),
                    )
                  ],
                ),
              )),
            ],
          )),
    ),
    margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
  );
}
