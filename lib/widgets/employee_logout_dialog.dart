import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/models/basic_response.dart';
import 'package:kiosk/screens/dashboard_screen/viewModel/dashboard_view_model.dart';
import 'package:kiosk/screens/landing_banner_screen/ui/landing_banners_screen.dart';
import 'package:kiosk/screens/login_screen/viewModel/default_login_view_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/progress_loading_white.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:provider/provider.dart';

void logoutDialog(BuildContext context) {
  String restaurantId = "", empId = "";
  SessionManager sessionManager = SessionManager();
  sessionManager.getRestaurantDetails().then((value) {
    restaurantId = value.restaurantId;
  });
  sessionManager.getEmployeeDetails().then((value) {
    if (value.userDetails != null) {
      empId = value.userDetails!.id;
    } else {
      empId = "";
    }
  });
  showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new WillPopScope(
            onWillPop: () async => false,
            child: SimpleDialog(
                backgroundColor: Colors.white,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                children: <Widget>[
                  Container(
                      width: ScreenSize.width(context) / 2,
                      height: ScreenSize.height(context) / 3,
                      padding: EdgeInsets.all(10),
                      child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, crossAxisAlignment: CrossAxisAlignment.center, children: [
                        Text(
                          sureEmployeeLogout,
                          style:
                              TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                                width: 180,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: def_color_red,
                                ),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(50),
                                    child: TextButton(
                                      onPressed: () {
                                        showDialog<void>(
                                            context: context,
                                            barrierDismissible: false,
                                            builder: (BuildContext context) {
                                              return loaderWhite();
                                            });
                                        print("ids--- $restaurantId $empId");
                                        Provider.of<DefaultLoginViewModel>(context, listen: false)
                                            .employeeLogoutApi(restaurantId, empId)
                                            .then((value) {
                                          BasicResponse? _basicResponse;
                                          Navigator.of(context).pop();
                                          if (Provider.of<DefaultLoginViewModel>(context, listen: false).getBasicResponse != null) {
                                            _basicResponse = Provider.of<DefaultLoginViewModel>(context, listen: false).getBasicResponse;
                                            if (_basicResponse!.responseStatus == 1) {
                                              BotToast.showText(
                                                text: _basicResponse.result,
                                                align: Alignment(0, 0),
                                              );
                                              Provider.of<CartViewModel>(context, listen: false).clearCart();
                                              Provider.of<DashBoardViewModel>(context, listen: false).setSelected(-1);
                                              sessionManager.clearCartItems();
                                              sessionManager.clearCustomerInfo();
                                              sessionManager.clearTempCartItems();
                                              sessionManager.clearDineOptions();
                                              sessionManager.clearEmployeeDetails();
                                              sessionManager.employeeLogin(false);
                                              Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => LandingBannerScreen()),
                                                  (Route<dynamic> route) => false);
                                            } else {
                                              BotToast.showText(
                                                text: _basicResponse.result,
                                                align: Alignment(0, 0),
                                              );
                                            }
                                          } else {
                                            BotToast.showText(
                                              text: pleaseTryAgain,
                                              align: Alignment(0, 0),
                                            );
                                          }
                                        });
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                        child: Text(
                                          yes,
                                          style: TextStyle(
                                            fontSize: 25,
                                            fontFamily: "Swis721",
                                            letterSpacing: 0.53,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ))),
                            Container(
                                margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                                width: 180,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.white,
                                  border: Border.all(
                                    color: Colors.black,
                                  ),
                                ),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(50),
                                    child: TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                        child: Text(
                                          no,
                                          style: TextStyle(
                                            fontSize: 25,
                                            fontFamily: "Swis721",
                                            letterSpacing: 0.53,
                                            fontWeight: FontWeight.w700,
                                            color: def_color_red,
                                          ),
                                        ),
                                      ),
                                    )))
                          ],
                        )
                      ]))
                ]));
      });
}
