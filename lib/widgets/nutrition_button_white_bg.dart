import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/utils/all_constants.dart';

Widget nutritionInfoButton() {
  return Container(
      margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
      width: 180,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.white,
      ),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: TextButton(
              onPressed: () {},
              child: Padding(
                padding: EdgeInsets.fromLTRB(10, 6, 6, 6),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Image.asset(
                        "assets/images/nutrition_info.png",
                        width: 20,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          nutritionInfo.toUpperCase(),
                          style: TextStyle(
                              fontSize: 16, color: def_color_red, letterSpacing: 0.5, fontFamily: "Swis721-Blk", fontWeight: FontWeight.w500),
                        ),
                        Text(
                          turnOn.toUpperCase(),
                          style:
                              TextStyle(fontSize: 14, color: def_color_red, letterSpacing: 0.4, fontFamily: "Swis721", fontWeight: FontWeight.w800),
                        )
                      ],
                    )
                  ],
                ),
              ))));
}
