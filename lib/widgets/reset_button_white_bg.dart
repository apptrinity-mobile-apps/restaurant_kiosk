import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/widgets/reset_cart_dialog.dart';

Widget resetButton(BuildContext context) {
  return Container(
      margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
      width: 180,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.white,
      ),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: TextButton(
            onPressed: () {
              clearCartDialog(context);
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(20, 12, 20, 12),
              child: Text(
                reset.toUpperCase(),
                style: TextStyle(fontSize: 20, color: def_color_red, fontFamily: "Swis721", letterSpacing: 0.6, fontWeight: FontWeight.w800),
              ),
            ),
          )));
}
