import 'package:flutter/material.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:virtual_keyboard_2/virtual_keyboard_2.dart';

void keyboard(BuildContext context, TextEditingController controller, String type) {
  showModalBottomSheet(
      context: context,
      useRootNavigator: true,
      barrierColor: Colors.black26.withOpacity(0.2),
      builder: (builder) {
        return Container(
          color: Colors.white,
          child: VirtualKeyboard(
              height: ScreenSize.height(context) / 4,
              textColor: Colors.red,
              type: type == 'text' ? VirtualKeyboardType.Alphanumeric : VirtualKeyboardType.Numeric,
              textController: controller),
        );
      });
}
