import 'package:flutter/material.dart';
import 'package:kiosk/screens/dashboard_screen/ui/dashboard_screen.dart';
import 'package:kiosk/screens/dashboard_screen/viewModel/dashboard_view_model.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/utils/screen_size.dart';
import 'package:kiosk/utils/shared_preferences.dart';
import 'package:kiosk/viewModel/cart_view_model.dart';
import 'package:provider/provider.dart';

void clearCartDialog(BuildContext context) {
  SessionManager sessionManager = SessionManager();
  showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return new WillPopScope(
            onWillPop: () async => false,
            child: SimpleDialog(
                backgroundColor: Colors.white,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                children: <Widget>[
                  Container(
                      width: ScreenSize.width(context) / 2,
                      height: ScreenSize.height(context) / 3,
                      padding: EdgeInsets.all(10),
                      child: Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, crossAxisAlignment: CrossAxisAlignment.center, children: [
                        Text(
                          sureClearCart,
                          style:
                              TextStyle(fontSize: 35, fontFamily: "Swis721", color: Colors.black, fontWeight: FontWeight.w300, letterSpacing: 0.63),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                                width: 180,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: def_color_red,
                                ),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(50),
                                    child: TextButton(
                                      onPressed: () {
                                        Provider.of<CartViewModel>(context, listen: false).clearCart();
                                        Provider.of<DashBoardViewModel>(context, listen: false).setSelected(-1);
                                        sessionManager.clearCartItems();
                                        sessionManager.clearCustomerInfo();
                                        sessionManager.clearTempCartItems();
                                        sessionManager.clearDineOptions();
                                        Navigator.pushAndRemoveUntil(
                                            context, MaterialPageRoute(builder: (context) => DashboardScreen()), (Route<dynamic> route) => false);
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                        child: Text(
                                          yes,
                                          style: TextStyle(
                                            fontSize: 25,
                                            fontFamily: "Swis721",
                                            letterSpacing: 0.53,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ))),
                            Container(
                                margin: EdgeInsets.fromLTRB(5, 20, 5, 20),
                                width: 180,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  color: Colors.white,
                                  border: Border.all(
                                    color: Colors.black,
                                  ),
                                ),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(50),
                                    child: TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: Padding(
                                        padding: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                        child: Text(
                                          no,
                                          style: TextStyle(
                                            fontSize: 25,
                                            fontFamily: "Swis721",
                                            letterSpacing: 0.53,
                                            fontWeight: FontWeight.w700,
                                            color: def_color_red,
                                          ),
                                        ),
                                      ),
                                    )))
                          ],
                        )
                      ]))
                ]));
      });
}
