import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/utils/all_constants.dart';

Widget userSignInButton() {
  return ClipRRect(
    borderRadius: BorderRadius.circular(50),
    child: Container(
      height: 50,
      padding: EdgeInsets.fromLTRB(5, 0, 20, 0),
      decoration: BoxDecoration(gradient: button_red_gradient),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(right: 20),
            child: CircleAvatar(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: Image.asset(
                  "assets/images/default_user.png",
                  height: 30,
                  width: 30,
                ),
              ),
              backgroundColor: Colors.white,
            ),
          ),
          Text(
            signIn.toUpperCase(),
            style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: "Swis721", fontWeight: FontWeight.w500),
          )
        ],
      ),
    ),
  );
}
