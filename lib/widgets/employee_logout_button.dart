import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kiosk/utils/all_constants.dart';
import 'package:kiosk/widgets/employee_logout_dialog.dart';

Widget employeeLogoutButton(BuildContext context) {
  return Container(
      margin: EdgeInsets.fromLTRB(5, 5, 5, 5),
      width: 180,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        gradient: button_red_gradient,
      ),
      child: ClipRRect(
          borderRadius: BorderRadius.circular(50),
          child: TextButton(
            onPressed: () {
              logoutDialog(context);
            },
            child: Padding(
              padding: EdgeInsets.fromLTRB(10, 15, 10, 15),
              child: Text(
                logout.toUpperCase(),
                style: TextStyle(fontSize: 20, color: Colors.white, fontFamily: "Swis721", fontWeight: FontWeight.w500),
              ),
            ),
          )));
}
