import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/suggestion_model.dart';
import 'package:kiosk/services/api_services.dart';
import 'package:kiosk/services/base_services.dart';

class Repository {
  BaseService _service = ApiServices();

  Future<dynamic> restaurantLogin(String restaurantCode) async {
    dynamic response = await _service.restaurantLogin(restaurantCode);
    return response;
  }

  Future<dynamic> dinningOptions(String restaurantId) async {
    dynamic response = await _service.dinningOptions(restaurantId);
    return response;
  }

  Future<dynamic> menus(String restaurantId) async {
    dynamic response = await _service.menus(restaurantId);
    return response;
  }

  Future<dynamic> menuGroups(String menuId) async {
    dynamic response = await _service.menuGroups(menuId);
    return response;
  }

  Future<dynamic> menuItemsBasedOnGroups(String menuGroupId, String restaurantId) async {
    dynamic response = await _service.menuItemsBasedOnGroups(menuGroupId, restaurantId);
    return response;
  }

  Future<dynamic> getLatestMenuItems(String restaurantId) async {
    dynamic response = await _service.getLatestMenuItems(restaurantId);
    return response;
  }

  Future<dynamic> getPopularItemList(String restaurantId) async {
    dynamic response = await _service.getPopularItemList(restaurantId);
    return response;
  }

  Future<dynamic> homeBanner(String restaurantId) async {
    dynamic response = await _service.homeBanner(restaurantId);
    return response;
  }

  Future<dynamic> sendOrder(CartModel model) async {
    dynamic response = await _service.sendOrder(model);
    return response;
  }

  Future<dynamic> getSuggestedItems(SuggestionModel model) async {
    dynamic response = await _service.suggestedItems(model);
    return response;
  }

  Future<dynamic> getGiftCardBalance(String restaurantId, String cardNo) async {
    dynamic response = await _service.giftCardBalance(restaurantId, cardNo);
    return response;
  }

  Future<dynamic> getPopularMenuItemsFromMenuGroup(String restaurantId, String menuGroupId) async {
    dynamic response = await _service.popularMenuItemsFromMenuGroup(restaurantId, menuGroupId);
    return response;
  }

  Future<dynamic> getPopularMenuItems(String restaurantId, String menuId) async {
    dynamic response = await _service.popularMenuItems(restaurantId, menuId);
    return response;
  }

  Future<dynamic> getOrderDetails(String restaurantId, String orderId) async {
    dynamic response = await _service.viewOrder(restaurantId, orderId);
    return response;
  }

  Future<dynamic> getReceiptData(String restaurantId) async {
    dynamic response = await _service.receiptData(restaurantId);
    return response;
  }

  Future<dynamic> employeeLoginUsingPasscode(String restaurantId, String passcode) async {
    dynamic response = await _service.employeeLoginPasscode(restaurantId, passcode);
    return response;
  }

  Future<dynamic> employeeLoginUsingEmail(String email, String password) async {
    dynamic response = await _service.employeeLoginEmail(email, password);
    return response;
  }

  Future<dynamic> employeeLogout(String restaurantId, String empId) async {
    dynamic response = await _service.employeeLogout(restaurantId, empId);
    return response;
  }

  Future<dynamic> checkDeviceName(String restaurantId, String deviceName) async {
    dynamic response = await _service.checkDeviceName(restaurantId, deviceName);
    return response;
  }

  Future<dynamic> getServiceAreas(String restaurantId) async {
    dynamic response = await _service.getServiceAreas(restaurantId);
    return response;
  }

  Future<dynamic> addKioskDevice(String restaurantId, String deviceName, String deviceToken, String serviceAreaId) async {
    dynamic response = await _service.addKioskDevice(restaurantId, deviceName, deviceToken, serviceAreaId);
    return response;
  }
}
