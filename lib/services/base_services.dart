import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/suggestion_model.dart';

abstract class BaseService {
  // staging
  final String baseUrl = "http://3.14.187.24/api/kiosk/";

  // final String baseUrl = "http://3.138.149.52/api/kiosk/";

  // live
  // final String baseUrl = "http://3.14.187.24/api/kiosk/";

  Future<dynamic> restaurantLogin(String restaurantCode);

  Future<dynamic> dinningOptions(String restaurantId);

  Future<dynamic> menus(String restaurantId);

  Future<dynamic> menuGroups(String menuId);

  Future<dynamic> menuItemsBasedOnGroups(String menuGroupId, String restaurantId);

  Future<dynamic> getPopularItemList(String restaurantId);

  Future<dynamic> getLatestMenuItems(String restaurantId);

  Future<dynamic> homeBanner(String restaurantId);

  Future<dynamic> giftCardBalance(String restaurantId, String cardNo);

  Future<dynamic> sendOrder(CartModel model);

  Future<dynamic> suggestedItems(SuggestionModel model);

  Future<dynamic> popularMenuItems(String restaurantId, String menuId);

  Future<dynamic> popularMenuItemsFromMenuGroup(String restaurantId, String menuGroupId);

  Future<dynamic> viewOrder(String restaurantId, String orderId);

  Future<dynamic> receiptData(String restaurantId);

  Future<dynamic> employeeLoginPasscode(String restaurantId, String passcode);

  Future<dynamic> employeeLoginEmail(String email, String password);

  Future<dynamic> employeeLogout(String restaurantId, String empId);

  Future<dynamic> checkDeviceName(String restaurantId, String deviceName);

  Future<dynamic> getServiceAreas(String restaurantId);

  Future<dynamic> addKioskDevice(String restaurantId, String deviceName, String deviceToken, String serviceAreaId);
}
