import 'dart:convert';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:kiosk/models/cart_model.dart';
import 'package:kiosk/models/suggestion_model.dart';
import 'package:kiosk/services/base_services.dart';
import 'package:kiosk/services/app_exceptions.dart';

class ApiServices extends BaseService {
  @visibleForTesting
  dynamic returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        dynamic responseJson = jsonDecode(response.body);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw UnauthorisedException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
        throw InternalServerException(response.body.toString());
      default:
        throw FetchDataException('Error occurred while communication with server with status code : ${response.statusCode}');
    }
  }

  @override
  Future restaurantLogin(String restaurantCode) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/generate_restaurant_otp");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantCode": restaurantCode}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future dinningOptions(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_dinein_options");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future menus(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_menus");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future menuGroups(String menuId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_menu_groups_list");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"menuId": menuId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future menuItemsBasedOnGroups(String menuGroupId, String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_menu_items_for_menugroups");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"menuGroupId": menuGroupId, "restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future homeBanner(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_all_home_banner");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getLatestMenuItems(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_latest_menu_items");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getPopularItemList(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_top_picked_menu_items");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future sendOrder(CartModel model) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_kiosk_order");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode(model));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future suggestedItems(SuggestionModel model) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_suggested_items_for_menugroups");
    try {
      final response = await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode(model));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future giftCardBalance(String restaurantId, String cardNo) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/giftcard_balance_enquiry");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId, "giftCardNo": cardNo}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future popularMenuItems(String restaurantId, String menuId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/menu_based_popular_items");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId, "menuId": menuId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future popularMenuItemsFromMenuGroup(String restaurantId, String menuGroupId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/menugroup_based_popular_items");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"restaurantId": restaurantId, "menuGroupId": menuGroupId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future viewOrder(String restaurantId, String orderId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/view_kiosk_order");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId, "orderId": orderId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future receiptData(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_all_receipt_data");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future employeeLoginPasscode(String restaurantId, String passcode) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/kiosk_passcode_login");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId, "passcode": passcode}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future employeeLoginEmail(String email, String password) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/kiosk_email_login");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"email": email, "password": password}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future employeeLogout(String restaurantId, String empId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/kiosk_employee_logout");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId, "employeeId": empId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future checkDeviceName(String restaurantId, String deviceName) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/check_existing_device_name");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId, "deviceName": deviceName}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future getServiceAreas(String restaurantId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/get_service_areas_list");
    try {
      final response =
          await http.post(url, headers: {"Content-type": "application/json; charset=UTF-8"}, body: json.encode({"restaurantId": restaurantId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }

  @override
  Future addKioskDevice(String restaurantId, String deviceName, String deviceToken, String serviceAreaId) async {
    dynamic responseJson;
    Uri url = Uri.parse(baseUrl + "/add_active_kiosk_device");
    try {
      final response = await http.post(url,
          headers: {"Content-type": "application/json; charset=UTF-8"},
          body: json.encode({"restaurantId": restaurantId, "deviceName": deviceName, "deviceToken": deviceToken, "serviceAreaId": serviceAreaId}));
      responseJson = returnResponse(response);
    } on SocketException {
      throw FetchDataException('Data Exception');
    }
    return responseJson;
  }
}
